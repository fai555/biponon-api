from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from rest_framework_jwt.views import (
    obtain_jwt_token, refresh_jwt_token, verify_jwt_token
)
from rest_framework_swagger.views import get_swagger_view


schema_view = get_swagger_view(title='Biponon API')

urlpatterns = [
    url(r'', admin.site.urls),

    # JWT
    url(r'^api/v1/token/auth/$', obtain_jwt_token),
    url(r'^api/v1/token/refresh/$', refresh_jwt_token),
    url(r'^api/v1/token/verify/$', verify_jwt_token),

    # Dashboard
    url(r'^api/', include('dashboard.urls')),
]

# Django admin site branding
admin.site.site_header = 'Biponon Dashboard Admin Console'
admin.site.site_title = 'Biponon Dashboard Admin Console'

if settings.DEBUG:
    urlpatterns += [
        # Login, logout at docs
        url(r'^auth/', include('rest_framework.urls')),
        # API docs
        url(r'^docs/$', schema_view),
    ]
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT
    )
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
