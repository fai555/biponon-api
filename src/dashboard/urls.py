from django.conf.urls import include, url

from .views_v1 import (
    # get_active_unique_merchandisers,
    get_count_active_unique_merchandisers,##
    get_count_active_unique_supervisers,##
    # get_visited_outlets,
    # get_count_visited_outlets,
    # get_count_closed_visited_outlets,
    # get_table_merchandisers_activity_summary,
    get_table_merchandisers_activity_summary_with_total,##
    # get_table_supervisor_activity_summary,
    get_table_supervisor_activity_summary_with_total,##
    get_table_unique_outlets_visited,##
    get_table_unique_outlets_visited_superviser,##
    get_total_calls_all_day,##
    # get_outlet_details,
    # get_merchandiser_details,
    # get_outlets_by_reporter,
    # get_active_merchandisers,
    # get_count_visited_unique_outlets,
    # get_report_geo_tagging,
    get_a_merchandiser_supervisor,##
    get_an_outlet,##
    # get_outlet_geolocation,
    # get_outlets_geo_location,
    # get_latest_outlet_info,
    # merchandisers_didnt_make_a_call,
    # get_target_vs_achievement,
    # get_area,
    # get_territory,
    # get_distributor_codes,
    # get_region_list,
    # get_merchandisers_by_dh_code,
    get_target_vs_achievement_by_merchandiser,##
    # get_visited_outlets_by_merchandiser,
    # get_phone_and_pic_url,
    # get_outlet_visitors,
    # get_region_wise_target_vs_achievement_on_specific_date,
    # get_count_unique_inactive_outlets_on_specific_month,
    # get_count_unique_not_found_outlets_on_specific_month,
    # get_count_unique_open_outlet_specific_month,
    # get_count_unique_not_found_outlet_specific_month,
    # get_count_unique_inactive_outlet_specific_month,
    # get_count_unique_closed_outlet_specific_month,
    # get_count_unique_open_outlet_specific_date,
    # get_count_unique_not_found_outlet_specific_date,
    # get_count_unique_inactive_outlet_specific_date,
    # get_count_unique_closed_outlet_specific_date,
    # get_count_unique_pic_url_specific_date,
    total_unique_outlets,##
    get_merchandiser_visited_in_specific_outlet,

    test_get_count_unique_outlet,##
    # test_get_count_active_unique_user,
    get_distinct_raw_data_for_merchandiser_csv,##
    get_distinct_raw_data_for_supervisor_csv,##
    # get_merchant_raw_dump_from_21dec_till_date_supervisor_csv,##
    # get_merchant_raw_dump_from_21dec_till_date_merchandiser_csv,##
    get_raw_data_merchandiser_for_merchant_csv,##
    get_raw_data_supervisor_for_merchant_csv,##

    # get_target_vs_achievement_supervisor,
    get_target_vs_achievement_by_supervisor,##
    # get_target_vs_achievement_by_merchandiser_number,
    # get_merchandiser_supervisor_call_start_time,
    # get_all_info_merchandiser_complain_specific_date,
    # get_complain_of_a_specific_merchandiser,
    get_latest_outlet_info_filterwise,
    # get_whitelisting_number_by_distributor_code,
    get_date_wise_merchandiser_complain_csv,##
    get_merchandiser_wise_target_vs_achievement_specific_date,##
    get_visited_outlets_date_range,##
    get_whitelisting_number_by_user,
    # get_dump_specific_outlet,
    FileView,##
    FileViewLatestCall,##
    # get_user_type,
    get_date_wise_merchandiser_maintenance_csv,##
    get_date_wise_merchandiser_new_task_csv,##
    get_selfie_attendance_merchandiser_csv,##
    get_selfie_attendance_supervisor_csv,##
    get_selfie_attendance_on_specific_date,##
    get_user_with_no_selfie_attendance,##
    get_selfie_count,##
    get_user_short_info,##
    # get_filter_user,
    # get_user_lat_lng,
    get_outlet_lat_lng,##
    get_total_user,##
    get_count_region_wise,
    #test_api_link
    RoutePlanUploader,
    AnnotateImage,
    get_count_all_dashboard,##
    get_table_users_activity_summary_with_total,##
    get_outlets_not_visited_csv,##
    PredictRetailLogo,

)


v1_urlpatterns = [
    # url(
    #     r'^active-unique-merchandisers/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_active_unique_merchandisers, name='get_active_unique_merchandisers'
    # ),
    url(
        r'^count-active-unique-merchandisers/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
        get_count_active_unique_merchandisers, name='get_count_active_unique_merchandisers'
    ),

    url(
        r'^count-active-unique-supervisers/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
        get_count_active_unique_supervisers, name='get_count_active_unique_supervisers'
    ),

    # url(
    #     r'^visited-outlets/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_visited_outlets, name='get_visited_outlets'
    # ),
    # url(
    #     r'^count-visited-outlets/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_visited_outlets, name='get_count_visited_outlets'
    # ),
    # url(
    #     r'^count-closed-visited-outlets$',
    #     get_count_closed_visited_outlets, name='get_count_closed_visited_outlets'
    # ),

    # url(
    #     r'^table-merchandisers-activity-summary/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_table_merchandisers_activity_summary, name='get_table_merchandisers_activity_summary'
    # ),


    url(
        r'^table-merchandisers-activity-summary-with-total/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
        get_table_merchandisers_activity_summary_with_total, name='get_table_merchandisers_activity_summary_with_total'
    ),
    
    # url(
    #     r'^table-supervisors-activity-summary/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_table_supervisor_activity_summary, name='get_table_supervisor_activity_summary'
    # ),
    

    url(
        r'^table-supervisors-activity-summary-with-total/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
        get_table_supervisor_activity_summary_with_total, name='get_table_supervisor_activity_summary_with_total'
    ),
    
    url(
        r'^table-total-number-calls-region/(?P<user>[+\w]+)$',
        get_table_unique_outlets_visited, name='get_table_unique_outlets_visited'
    ),

    url(
        r'^table-total-number-calls-region-supervisor$',
        get_table_unique_outlets_visited_superviser, name='get_table_unique_outlets_visited_superviser'
    ),
    

    url(
        r'^today-number-calls-time-period/(?P<user>[+\w]+)$',
        get_total_calls_all_day, name='get_total_calls_all_day'
    ),
    # url(
    #     r'^outlet-details/(?P<outlet_wallet_number>[+\w]+)$',
    #     get_outlet_details, name='get_outlet_details'
    # ),
    # url(
    #     r'^merchandiser-details/(?P<whitelisting_number>[+\w]+)$',
    #     get_merchandiser_details, name='get_merchandiser_details'
    # ),
    # url(
    #     r'^outlets-by-merchandiser/(?P<phone>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_outlets_by_reporter, name='get_outlets_by_reporter'
    # ),
    # url(
    #     r'^active-outlets/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<limit>\d+)/(?P<offset>\d+)$',
    #     get_active_merchandisers, name='get_active_merchandisers'
    # ),
    # url(
    #     r'^count-visited-unique-outlets/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_visited_unique_outlets, name='get_count_visited_unique_outlets'
    # ),
    # url(
    #     r'^report-geo-tagging/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<limit>\d+)/(?P<offset>\d+)$',
    #     get_report_geo_tagging, name='get_report_geo_tagging'
    # ),
    url(
        r'^search-merchandiser-supervisor/(?P<whitelisting_number>[+\w]+)$',
        get_a_merchandiser_supervisor, name='get_a_merchandiser_supervisor'
    ),
    url(
        r'^outlet/(?P<wallet>[+\w]+)$',
        get_an_outlet, name='get_an_outlet'
    ),
    # url(
    #     r'^get-outlet-geolocation/(?P<wallet>[+\w]+)$',
    #     get_outlet_geolocation, name='get_outlet_geolocation'
    # ),
    # url(
    #     r'^get-outlet-geolocation$',
    #     get_outlets_geo_location, name='get_outlets_geo_location'
    # ),
    # url(
    #     r'^get-latest-outlet-info/(?P<limit>\d+)$',
    #     get_latest_outlet_info, name='get_latest_outlet_info'
    # ),
    # url(
    #     r'^merchandisers-didnt-make-a-call-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
    #     merchandisers_didnt_make_a_call, name='merchandisers_didnt_make_a_call'
    # ),
    # url(
    #     r'^target-vs-achievement-merchandiser-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
    #     get_target_vs_achievement, name='get_target_vs_achievement'
    # ),
    # url(
    #     r'^target-vs-achievement-supervisor-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
    #     get_target_vs_achievement_supervisor, name='get_target_vs_achievement_supervisor'
    # ),
    url(
         r'^target-vs-achievement-by-supervisor-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_target_vs_achievement_by_supervisor, name='get_target_vs_achievement_by_supervisor'
    ),
    # url(
    #     r'^get-area/(?P<region>[-\w]+)$',
    #     get_area, name='get_area'
    # ),
    # url(
    #     r'^get-territory/(?P<area>[-\w]+)/(?P<user>[-\w]+)$',
    #     get_territory, name='get_territory'
    # ),
    # url(
    #     r'^get-distributor-codes/(?P<territory>[-\w]+)$',
    #     get_distributor_codes, name='get_distributor_codes'
    # ),
    # url(
    #     r'^get-region/(?P<user>[+\w]+)$',
    #     get_region_list, name='get_region_list'
    # ),
    # url(
    #     r'^get-merchandisers/(?P<code>[-\w]+)$',
    #     get_merchandisers_by_dh_code, name='get_merchandisers_by_dh_code'
    # ),
    url(
        r'^target-vs-achievement-by-merchandiser-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_target_vs_achievement_by_merchandiser, name='get_target_vs_achievement_by_merchandiser'
    ),

    # url(
    #     r'^target-vs-achievement-merchandiser/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_target_vs_achievement_by_merchandiser_number, name='get_target_vs_achievement_by_merchandiser_number'
    # ),




    # url(
    #     r'^get-visited-outlets/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_visited_outlets_by_merchandiser, name='get_visited_outlets_by_merchandiser'
    # ),
    # url(
    #     r'^get-phone-and-pic-url/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<limit>\d+)$',
    #     get_phone_and_pic_url, name='get_phone_and_pic_url'
    # ),

    # url(
    #     r'^get-outlet-visitors/(?P<wallet>[+\w]+)$',
    #     get_outlet_visitors, name='get_outlet_visitors'
    # ),

    # url(
    #     r'^get-region-wise-target-vs-achievement-on-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_region_wise_target_vs_achievement_on_specific_date, name='get_region_wise_target_vs_achievement_on_specific_date'
    # ),


    # url(
    #     r'^get-count-unique-inactive-outlets/(?P<month>\d+)$',
    #     get_count_unique_inactive_outlets_on_specific_month, name='get_count_unique_inactive_outlets_on_specific_month'
    # ),

    # url(
    #     r'^get-count-unique-not-found-outlets/(?P<month>\d+)$',
    #     get_count_unique_not_found_outlets_on_specific_month, name='get_count_unique_not_found_outlets_on_specific_month'
    # ),

    # url(
    #     r'^get-count-unique-open-outlet-specific-month/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_open_outlet_specific_month, name='get_count_unique_open_outlet_specific_month'
    # ),

    # url(
    #     r'^get-count-unique-not-found-outlet-specific-month/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_not_found_outlet_specific_month, name='get_count_unique_not_found_outlet_specific_month'
    # ),

    # url(
    #     r'^get-count-unique-inactive-outlet-specific-month/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_inactive_outlet_specific_month, name='get_count_unique_inactive_outlet_specific_month'
    # ),

    # url(
    #     r'^get-count-unique-closed-outlet-specific-month/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_closed_outlet_specific_month, name='get_count_unique_closed_outlet_specific_month'
    # ),

    # url(
    #     r'^get-count-unique-open-outlet-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_open_outlet_specific_date, name='get_count_unique_open_outlet_specific_date'
    # ),

    # url(
    #     r'^get-count-unique-not-found-outlet-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_not_found_outlet_specific_date, name='get_count_unique_not_found_outlet_specific_date'
    # ),

    # url(
    #     r'^get-count-unique-inactive-outlet-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_inactive_outlet_specific_date, name='get_count_unique_inactive_outlet_specific_date'
    # ),

    # url(
    #     r'^get-count-unique-closed-outlet-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_closed_outlet_specific_date, name='get_count_unique_closed_outlet_specific_date'
    # ),

    # url(
    #     r'^get-count-unique-pic-url-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_count_unique_pic_url_specific_date, name='get_count_unique_pic_url_specific_date'
    # ),

    url(
        r'^total-unique-outlets$',
        total_unique_outlets, name='total_unique_outlets'

    ),

    url(
        r'^get-merchandiser-visited-in-specific-outlet/(?P<wallet_number>[+\w]+)$',
        get_merchandiser_visited_in_specific_outlet, name='get_merchandiser_visited_in_specific_outlet'

    ),

    url(
        r'^get-distinct-raw-data-for-merchandiser-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_distinct_raw_data_for_merchandiser_csv, name='get_distinct_raw_data_for_merchandiser_csv'
    ),

    url(
        r'^get-distinct-raw-data-for-supervisor-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_distinct_raw_data_for_supervisor_csv, name='get_distinct_raw_data_for_supervisor_csv'
    ),

    # url(
    #     r'^get-merchant-raw-dump-from-21dec-till-date-supervisor-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
    #     get_merchant_raw_dump_from_21dec_till_date_supervisor_csv, name='get_merchant_raw_dump_from_21dec_till_date_supervisor_csv'
    # ),
    
    # url(
    #     r'^get-merchant-raw-dump-from-21dec-till-date-merchandiser-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
    #     get_merchant_raw_dump_from_21dec_till_date_merchandiser_csv, name='get_merchant_raw_dump_from_21dec_till_date_merchandiser_csv'
    # ),

    url(
        r'^get-raw-data-merchandiser-for-merchant-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_raw_data_merchandiser_for_merchant_csv, name='get_raw_data_merchandiser_for_merchant_csv'
    ),

    url(
        r'^get-raw-data-supervisor-for-merchant-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_raw_data_supervisor_for_merchant_csv, name='get_raw_data_supervisor_for_merchant_csv'
    ),




    url(
        r'^get-count-unique-outlet/(?P<agent_merchant>[+\w]+)$',
        test_get_count_unique_outlet, name='test_get_count_unique_outlet'
    ),

    # url(
    #     r'^get-merchandiser-supervisor-call-start-time/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
    #     get_merchandiser_supervisor_call_start_time, name='get_merchandiser_supervisor_call_start_time'
    # ),

    # url(
    #     r'^get-all-info-merchandiser-complain-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_all_info_merchandiser_complain_specific_date, name='get_all_info_merchandiser_complain_specific_date'
    # ),

    # url(
    #     r'^get-complain-of-a-specific-merchandiser/(?P<reporter>[+\w]+)$',
    #     get_complain_of_a_specific_merchandiser, name='get_complain_of_a_specific_merchandiser'
    # ),

    url(
        r'^get-latest-outlet-info-filterwise/(?P<region>[-\w]+)/(?P<area>[-\w]+)/(?P<territory>[-\w]+)/(?P<house>.+)/(?P<user>[+\w]+)/(?P<agent_merchant>[+\w]+)/(?P<bmcc>[+\w]+)/(?P<outlet>[+\w]+)/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)/(?P<limit>\d+)/(?P<offset>\d+)$',
        get_latest_outlet_info_filterwise, name='get_latest_outlet_info_filterwise'
    ),

    # url(

    #     r'^get-whitelisting-number/(?P<distributor_code>[+\w]+)$',
    #     get_whitelisting_number_by_distributor_code, name='get_whitelisting_number_by_distributor_code'
    # ),

    url(
        r'^get-outlet-complain-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_date_wise_merchandiser_complain_csv, name='get_date_wise_merchandiser_complain_csv'

    ),

    url(
        r'^get-merchandiser-wise-target-vs-achievement-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<user>[+\w]+)$',
        get_merchandiser_wise_target_vs_achievement_specific_date, name='get_merchandiser_wise_target_vs_achievement_specific_date'
    ),

    url(

        r'^get-visited-outlets-date-range/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_visited_outlets_date_range, name='get_visited_outlets_date_range'
    ),

    url(
        r'^get-whitelisting-number-by-user/(?P<user>[+\w]+)$',
        get_whitelisting_number_by_user, name='get_whitelisting_number_by_user'
    ),

    # url(
    #     r'^get-dump-specific-outlet/(?P<number>.+)$',
    #     get_dump_specific_outlet, name='get_dump_specific_outlet'
    # ),

    url(
        r'^upload/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$', 
        FileView.as_view(), name="file-upload"
    ),

    url(
        r'^upload-outlet-latest-call/(?P<region>[-\w]+)/(?P<area>[-\w]+)/(?P<territory>[-\w]+)/(?P<house>[+\w]+)/(?P<user>[+\w]+)/(?P<agent_merchant>[+\w]+)/(?P<bmcc>[+\w]+)/(?P<outlet>[+\w]+)/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)/(?P<limit>\d+)/(?P<offset>\d+)$', 
        FileViewLatestCall.as_view(), name="file-upload-latest-call"
    ),

    # url(
    #     r'^get-user-type$', 
    #     get_user_type, name="get_user_type"
    # ), 

    url(
        r'^get-outlet-maintenance-task-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_date_wise_merchandiser_maintenance_csv, name='get_date_wise_merchandiser_maintenance_csv'

    ),

    url(
        r'^get-outlet-new-task-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_date_wise_merchandiser_new_task_csv, name='get_date_wise_merchandiser_new_task_csv'

    ),
    
    url(
        r'^get-selfie-attendance-merchandiser-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_selfie_attendance_merchandiser_csv, name='get_selfie_attendance_merchandiser_csv'

    ),

   url(
        r'^get-selfie-attendance-supervisor-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_selfie_attendance_supervisor_csv, name='get_selfie_attendance_supervisor_csv'

    ), 

    url(
        r'^get-selfie-attendance-on-specific-date/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)/(?P<user>[-\w]+)$',
        get_selfie_attendance_on_specific_date, name='get_selfie_attendance_on_specific_date'

    ), 

    url(
        r'^get-user-with-no-selfie-attendance/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)/(?P<user>[-\w]+)$',
        get_user_with_no_selfie_attendance, name='get_user_with_no_selfie_attendance'

    ),

    url(
        r'^get-selfie-count/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$',
        get_selfie_count, name='get_selfie_count'

    ),  
    
    # url(
    #     r'^get-filter-user/(?P<query>.+)$',
    #     get_filter_user, name='get_filter_user'

    # ),

    url(
        r'^get-user-short-info/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
        get_user_short_info, name='get_user_short_info'


     ),

    # url(
    #     r'^get-user-lat-lng/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
    #     get_user_lat_lng, name='get_user_lat_lng'


    #  ),

    url(
        r'^get-outlet-lat-lng/(?P<whitelisting_number>[+\w]+)/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$',
        get_outlet_lat_lng, name='get_outlet_lat_lng'


     ),

    url(
        r'^get-total-user/(?P<user>[+\w]+)$',
        get_total_user, name='get_total_user'

    ), 

    url(
        r'^get-count-region-day-wise/(?P<user>[+\w]+)$',
        get_count_region_wise, name='get_count_region_wise'

    ),  


    url(
        r'^route_plan_upload/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$', 
        RoutePlanUploader.as_view(), name="route_plan_upload"
    ),
    

    url(
        r'^get-annotation/$',
        AnnotateImage.as_view(), name='get-annotation'

    ),
    url(
        r'^get-count-all-dashboard/(?P<user>[+\w]+)$',
        get_count_all_dashboard, name='get_count_all_dashboard'


     ),

    url(
        r'^get-table-users-activity-summary-with-total/(?P<user>[+\w]+)$',
        get_table_users_activity_summary_with_total, name='get_table_users_activity_summary_with_total'


     ),

    url(
        r'^get-outlets-not-visited-csv/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)/(?P<day2>\d+)/(?P<month2>\d+)/(?P<year2>\d+)$', 
       get_outlets_not_visited_csv, name="get_outlets_not_visited_csv"
    ),
    
    url(
        r'^predict-retail-logo/$',
        PredictRetailLogo.as_view(), name='predict-retail-logo'

    ),
     
    




]


app_name = 'dashboard'
urlpatterns = [
    url(r'^v1/', include(v1_urlpatterns)),
]

#Swad Tasnim Testing