from django.contrib import admin

from .models import BipononCenterRegion, BipononCenterArea, BipononCenterTerritory, BipononCenter, UserPermission

admin.site.site_header = 'Bponon Web API Dashboard'

class BipononCenterInLine(admin.ModelAdmin):
    list_display = ('name', 'ma_wallet', 'distribution_house_code')
    list_filter = ('distribution_house_code',)





admin.site.register(BipononCenterRegion)
admin.site.register(BipononCenterArea)
admin.site.register(BipononCenterTerritory)
admin.site.register(BipononCenter, BipononCenterInLine)
admin.site.register(UserPermission)