###############################################################################
# Believe me, I didn't write any of these code, I just ported it from         #
# Flask to Django on a tight schedule, so couldn't refactor.                  #
# My manager promised me to allow the time to refactor in future.             #
#                                      - (See the name from git blame? :p)    #
###############################################################################
REGIONS = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA NORTH',
    'DHAKA SOUTH',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET',
    'TOTAL'
]

REGIONS3 = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA NORTH',
    'DHAKA SOUTH',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET',
    'TOTAL'

]

REGIONS2 = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET',
    'TOTAL'
]


REGIONS4 = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET',
    'TOTAL'
]
REGIONS5 = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA NORTH',
    'DHAKA SOUTH',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET'
]

re2 = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA NORTH',
    'DHAKA SOUTH',
    'DHAKA',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET',
    'TOTAL'
    ]



def table_summary_imputer(data):
    TABLE_SUMMARY_TEMPLATE = {
        region: {
            'VisitedOutlets': 0,
            'ActiveMerchandisers': 0
        } for region in REGIONS
    }
    
    for key in data.keys():
        
        TABLE_SUMMARY_TEMPLATE[key]['VisitedOutlets'] = data[key]['VisitedOutlets']
        TABLE_SUMMARY_TEMPLATE[key]['ActiveMerchandisers'] = data[key]['ActiveMerchandisers']
        
    return TABLE_SUMMARY_TEMPLATE

def table_summary_imputer_2(data):
    TABLE_SUMMARY_TEMPLATE = {
        region: {
            'VisitedOutlets': 0,
            'ActiveSupervisors': 0
        } for region in re2
    }

    for key in data.keys():
        TABLE_SUMMARY_TEMPLATE[key]['VisitedOutlets'] = data[key]['VisitedOutlets']
        TABLE_SUMMARY_TEMPLATE[key]['ActiveSupervisors'] = data[key]['ActiveSupervisors']
        

    return TABLE_SUMMARY_TEMPLATE    


def table_summary_imputer3(data):
    TABLE_SUMMARY_TEMPLATE = {
        region: {
            'VisitedOutlets': 0,
            'ActiveMerchandisers': 0
        } for region in REGIONS3
    }
    
    for key in data.keys():
        
        TABLE_SUMMARY_TEMPLATE[key]['VisitedOutlets'] = data[key]['VisitedOutlets']
        TABLE_SUMMARY_TEMPLATE[key]['ActiveMerchandisers'] = data[key]['ActiveMerchandisers']
        
    return TABLE_SUMMARY_TEMPLATE


def table_summary_imputer4(data):
    TABLE_SUMMARY_TEMPLATE = {
        region: {
            'VisitedOutlets': 0,
            'ActiveSupervisors': 0
        } for region in REGIONS4
    }

    for key in data.keys():
        TABLE_SUMMARY_TEMPLATE[key]['VisitedOutlets'] = data[key]['VisitedOutlets']
        TABLE_SUMMARY_TEMPLATE[key]['ActiveSupervisors'] = data[key]['ActiveSupervisors']
        

    return TABLE_SUMMARY_TEMPLATE



def call_by_day_imputer(data):
    CALL_BY_DAY_TEMPLATE = {
        region: 0 for region in REGIONS
    }

    for key in data.keys():
        CALL_BY_DAY_TEMPLATE[key] = data[key]

    return CALL_BY_DAY_TEMPLATE
