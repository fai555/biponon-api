import json
import numpy as np
from PIL import Image
import requests
from io import BytesIO


STANDARD_COLORS = [
    'AliceBlue', 'Chartreuse', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque',
    'BlanchedAlmond', 'BlueViolet', 'BurlyWood', 'CadetBlue', 'AntiqueWhite',
    'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson', 'Cyan',
    'DarkCyan', 'DarkGoldenRod', 'DarkGrey', 'DarkKhaki', 'DarkOrange',
    'DarkOrchid', 'DarkSalmon', 'DarkSeaGreen', 'DarkTurquoise', 'DarkViolet',
    'DeepPink', 'DeepSkyBlue', 'DodgerBlue', 'FireBrick', 'FloralWhite',
    'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'Gold', 'GoldenRod',
    'Salmon', 'Tan', 'HoneyDew', 'HotPink', 'IndianRed', 'Ivory', 'Khaki',
    'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue',
    'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey',
    'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue',
    'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime',
    'LimeGreen', 'Linen', 'Magenta', 'MediumAquaMarine', 'MediumOrchid',
    'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen',
    'MediumTurquoise', 'MediumVioletRed', 'MintCream', 'MistyRose', 'Moccasin',
    'NavajoWhite', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed',
    'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed',
    'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple',
    'Red', 'RosyBrown', 'RoyalBlue', 'SaddleBrown', 'Green', 'SandyBrown',
    'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
    'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'GreenYellow',
    'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White',
    'WhiteSmoke', 'Yellow', 'YellowGreen'
]

CATEGORY_INDEX = {
    1: {'id': 1, 'name': u'bkash_logo_app'},
    2: {'id': 2, 'name': u'bkash_logo_icon'},
    3: {'id': 3, 'name': u'bkash_logo_text'},
    4: {'id': 4, 'name': u'bkash_logo'},
    5: {'id': 5, 'name': u'rocket_logo'},
    6: {'id': 6, 'name': u'nagad_logo_icon'},
    7: {'id': 7, 'name': u'nagad_logo_text'},
    8: {'id': 8, 'name': u'nagad_logo'},
    9: {'id': 9, 'name': u'grameenphone'},
    10: {'id': 10, 'name': u'robi'},
    11: {'id': 11, 'name': u'banglalink'}
}


def format_mask(detection_masks, detection_boxes, N, image_size):

    (height, width, _) = image_size
    output_masks = np.zeros((N, image_size[0], image_size[1]))
    # Process the masks related to the N objects detected in the image
    for i in range(N):
        normalized_mask = detection_masks[i].astype(np.float32)
        normalized_mask = Image.fromarray(normalized_mask, 'F')

        # Boxes are expressed with 4 scalars - normalized coordinates [y_min, x_min, y_max, x_max]
        [y_min, x_min, y_max, x_max] = detection_boxes[i]

        # Compute absolute boundary of box
        box_size = (int((x_max - x_min) * width),
                    int((y_max - y_min) * height))

        # Resize the mask to the box size using LANCZOS appoximation
        resized_mask = normalized_mask.resize(box_size, Image.LANCZOS)

        # Convert back to array
        resized_mask = np.array(resized_mask).astype(np.float32)

        # Binarize the image by using a fixed threshold
        binary_mask_box = np.zeros(resized_mask.shape)
        thresh = 0.5
        (h, w) = resized_mask.shape

        for k in range(h):
            for j in range(w):
                if resized_mask[k][j] >= thresh:
                    binary_mask_box[k][j] = 1

        binary_mask_box = binary_mask_box.astype(np.uint8)

        # Replace the mask in the context of the original image size
        binary_mask = np.zeros((height, width))

        x_min_at_scale = int(x_min * width)
        y_min_at_scale = int(y_min * height)

        d_x = int((x_max - x_min) * width)
        d_y = int((y_max - y_min) * height)

        for x in range(d_x):
            for y in range(d_y):
                binary_mask[y_min_at_scale +
                            y][x_min_at_scale + x] = binary_mask_box[y][x]

        # Update the masks array
        output_masks[i][:][:] = binary_mask

    # Cast mask as integer
    output_masks = output_masks.astype(np.uint8)
    return output_masks


def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)
        ).astype(np.uint8)


def get_image(image_url):
    response = requests.get(image_url)
    image = Image.open(BytesIO(response.content)).convert("RGB")

    return image


def pre_process(image):

    image_np = load_image_into_numpy_array(image)

    # Expand dims to create  bach of size 1
    image_tensor = np.expand_dims(image_np, 0)
    formatted_json_input = json.dumps(
        {
            "signature_name": "serving_default",
            "instances": image_tensor.tolist()
        }
    )

    return formatted_json_input


def post_process(server_response, image_size):

    response = json.loads(server_response.text)
    output_dict = response['predictions'][0]

    # all outputs are float32 numpy arrays, so convert types as appropriate

    output_dict['num_detections'] = int(output_dict['num_detections'])

    output_dict['detection_classes'] = np.array(
        [int(class_id) for class_id in output_dict['detection_classes']]
    )

    output_dict['detection_boxes'] = np.array(output_dict['detection_boxes'])
    output_dict['detection_scores'] = np.array(output_dict['detection_scores'])

    # Process detection mask
    if 'detection_masks' in output_dict:
        # Determine a threshold above wihc we consider the pixel shall belong to the mask
        # thresh = 0.5
        output_dict['detection_masks'] = np.array(
            output_dict['detection_masks']
        )
        output_dict['detection_masks'] = format_mask(
            output_dict['detection_masks'],
            output_dict['detection_boxes'],
            output_dict['num_detections'],
            image_size
        )

    return output_dict


def get_boxes_and_labels(boxes, classes, scores, im_width, im_height):

    data_list = []

    max_boxes_to_draw = 20
    min_score_thresh = 0.2

    for i in range(0, boxes.shape[0]):
        if scores[i] > min_score_thresh:
            box = tuple(boxes[i].tolist())

            display_str = ''

            if classes[i] in CATEGORY_INDEX.keys():
                class_name = CATEGORY_INDEX[classes[i]]['name']
            else:
                class_name = 'N/A'

            display_str = str(class_name)
            display_str = '{}: {}%'.format(display_str, int(100*scores[i]))

            box_color = STANDARD_COLORS[classes[i] % len(STANDARD_COLORS)]

            ymin, xmin, ymax, xmax = box
            data = {
                'display_string': display_str,
                'bounding_box': {
                    'ymin': float(ymin) * im_height,
                    'xmin': float(xmin) * im_width,
                    'ymax': float(ymax) * im_height,
                    'xmax': float(xmax) * im_width
                },
                'color': box_color
            }

            data_list.append(data)

    return data_list

