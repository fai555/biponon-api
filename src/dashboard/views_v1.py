###############################################################################
# Believe me, I didn't write any of these code, I just ported it from         #
# Flask to Django on a tight schedule, so couldn't refactor.                  #
# My manager promised me to allow the time to refactor in future.             #
#                                      - (See the name from git blame? :p)    #
###############################################################################
import datetime
import time
import csv,json

from django.http import HttpResponseRedirect
from django.shortcuts import render
#from .form import UploadFileForm

from rest_framework.views import APIView

from django.shortcuts import render
import openpyxl
import pandas as pd

from django.http import HttpResponse
from django.conf import settings
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser, JSONParser
from rest_framework import status
from .serializers import FileSerializer
import os
from random import randint

from .bigqueries import (
    bigquery_client, PROJECT_ID,
    # SELECT_ACTIVE_UNIQUE_MERCHANDISERS_ON_SPECIFIC_DATE,
    COUNT_OF_ACTIVE_UNIQUE_MERCHANDISERS_ON_SPECIFIC_DATE,##
    COUNT_OF_ACTIVE_UNIQUE_SUPERVISERS_ON_SPECIFIC_DATE,##
    # COUNT_OF_VISITED_OUTLETS_ON_SPECIFIC_DATE,
    # VISITED_OUTLETS_ON_SPECIFIC_DATE,
    # COUNT_OF_UNIQUE_VISITED_CLOSED_OUTLETS,
    # TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE,
    TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL,##
    # TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE,
    TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL,##
    TABLE_NEW_UNIQUE_OUTLETS_VISITED,##
    TABLE_NEW_UNIQUE_OUTLETS_VISITED_SUPERVISER,##
    TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY,##
    # GET_OUTLET_DETAILS_BY_WALLET_NO,
    # GET_MERCHANDISER_DETAILS_BY_WHITELISTING_NUMBER,
    # GET_OUTLETS_GROUPBY_REPORTER,
    # GET_RAW_DATA_BY_MONTH,
    # GET_RAW_DATA_BY_DAY,
    # GET_ACTIVE_OUTLETS,
    # VISITED_UNIQUE_OUTLET_COUNT_ON_MONTH,
    # GEO_TAGGING_REPORT_PAGINATED,
    # GET_LAST_N_OUTLET_INFORMATION,
    # TARGET_VS_ACHIEVEMENT,
    # TARGET_VS_ACHIEVEMENT_SUPERVISOR,
    # TARGET_VS_ACHIEVEMENT_BY_SUPERVISOR,
    # MERCHANDISER_DIDNT_MAKE_ANY_CALL,
    # GET_AREA_BY_REGION,
    # GET_TERRITORY_BY_AREA,
    # GET_DISTRIBUTOR_CODE_BY_TERRITORY,
    # GET_REGION_LIST,
    # GET_MERCHANDISERS_LIST_BY_DISTRIBUTOR_CODE,
    # TARGET_VS_ACHIEVEMENT_BY_MERCHANDISER,
    # TARGET_VS_ACHIEVEMENT_BY_MERCHANDISER_NUMBER,
    # VISITED_OUTLETS_BY_MERCHANDISER_ON_A_GIVEN_DATE,
    # GET_GEOLOCATION_OF_OUTLETS,
    # GET_GEOLOCATION_BY_OUTLET,
    GET_AN_OUTLET,##
    GET_A_MERCHANDISER_SUPERVISOR,##
    # GET_PHONE_AND_PIC_URL,
    # GET_OUTLET_VISITORS,
    # GET_REGION_WISE_TARGET_VS_ACHIEVEMENT_ON_SPECIFIC_DATE,
    # GET_COUNT_UNIQUE_INACTIVE_OUTLETS_ON_SPECIFIC_MONTH,
    # GET_COUNT_UNIQUE_NOT_FOUND_OUTLETS_ON_SPECIFIC_MONTH,
    # GET_COUNT_UNIQUE_OPEN_OUTLET_SPECIFIC_MONTH,
    # GET_COUNT_UNIQUE_NOT_FOUND_OUTLET_SPECIFIC_MONTH,
    # GET_COUNT_UNIQUE_INACTIVE_OUTLET_SPECIFIC_MONTH,
    # GET_COUNT_UNIQUE_CLOSED_OUTLET_SPECIFIC_MONTH,
    # GET_COUNT_UNIQUE_OPEN_OUTLET_SPECIFIC_DATE,
    # GET_COUNT_UNIQUE_NOT_FOUND_OUTLET_SPECIFIC_DATE,
    # GET_COUNT_UNIQUE_INACTIVE_OUTLET_SPECIFIC_DATE,
    # GET_COUNT_UNIQUE_CLOSED_OUTLET_SPECIFIC_DATE,
    # GET_COUNT_UNIQUE_PIC_URL_SPECIFIC_DATE,
    GET_TOTAL_UNIQUE_OUTLETS,##
    GIT_MERCHANDISER_VISITED_IN_SPECIFIC_OUTLET,

    TEST_GET_COUNT_UNIQUE_OUTLET,##
    # COUNT_OF_ACTIVE_UNIQUE_USER,

    GET_DISTINCT_RAW_DATA_FOR_MERCHANDISER_CSV,##
    GET_DISTINCT_RAW_DATA_FOR_SUPERVISOR_CSV,##
    # GET_MERCHANT_RAW_DUMP_FROM_21DEC_TILL_DATE_SUPERVISOR_CSV,##
    # GET_MERCHANT_RAW_DUMP_FROM_21DEC_TILL_DATE_MERCHANDISER_CSV,##
    GET_RAW_DATA_MERCHANDISER_FOR_MERCHANT_CSV,##
    GET_RAW_DATA_SUPERVISOR_FOR_MERCHANT_CSV,##
    # GET_MERCHANDISER_SUPERVISOR_CALL_START_TIME,
    # GET_ALL_INFO_MERCHANDISER_COMPLAIN_SPECIFIC_DATE,
    # GET_COMPLAIN_OF_A_SPECIFIC_MERCHANDISER,
    GET_LAST_N_OUTLET_INFO_FILTER,##
    # GET_WHITELISTING_NUMBER_BY_CODE,
    GET_DATE_WISE_MERCHANDISER_COMPLAIN_CSV,##
    GET_MERCHANDISER_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE,##
    VISITED_OUTLETS_DATE_RANGE,##
    GET_COUNT_OF_DATA_FROM_UPDATE_OF_OUTLETS_DATE,##
    GET_WHITELISTING_NUMBER_ALL,
    GET_WHITELISTING_NUMBER_USER,
    GET_DUMP_SPECIFIC_OUTLETS,##
    # GET_TERRITORY_BY_AREA_MERCAHNDISER,
    # GET_TERRITORY_BY_AREA_SUPERVISOR,
    GET_DATE_WISE_MERCHANDISER_MAINTENANCE_CSV,##
    GET_DATE_WISE_MERCHANDISER_NEW_TASK_CSV,##
    GET_SELFIE_ATTENDANCE_MERCHANDISER_CSV,##
    GET_SELFIE_ATTENDANCE_SUPERVISOR_CSV,##
    GET_SELFIE_ATTENDANCE_ON_SPECIFIC_DATE_MERCHANDISER,##
    GET_SELFIE_ATTENDANCE_ON_SPECIFIC_DATE_SUPERVISOR,##
    GET_USER_WITH_NO_SELFIE_ATTENDANCE_MERCHANDISER,##
    GET_USER_WITH_NO_SELFIE_ATTENDANCE_SUPERVISOR,##
    GET_USER_SHORT_INFO,##
    # GET_FILTER_OUTLET,
    # GET_LAT_LNG,
    GET_OUTLET_LAT_LNG,##
    GET_OUTLET_LAT_LNG_FUTURE,##
    GET_SUPERVISOR_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE,##
    GET_COUNT_SELFIE_MERCHANDISER,##
    GET_COUNT_SELFIE_SUPERVISOR,##
    GET_TOTAL_MERCHANDISER,##
    GET_TOTAL_SUPERVISOR,##
    GET_COUNT_REGION_WISE,
    # GET_OUTLET_OUTSIDE,

    GET_MERCHANDISERS_BASIC_INFO,##
    GET_MERCHANDISERS_ROUTE_PLAN,##
    MERCHANDISER_SCHEMA,##
    TEST_MERCHANDISER_TABLE,##
    GET_COUNT_ALL_DASHBOARD,##
    OUTLETS_NOT_VISITED_CSV,##
    



   
)

from .utils import table_summary_imputer,table_summary_imputer_2, table_summary_imputer3, table_summary_imputer4, REGIONS, REGIONS2, REGIONS3, REGIONS4,REGIONS5

from .models import UserPermission, BipononCenter

import requests
from PIL import Image
from .prediction_util import load_image_into_numpy_array, get_boxes_and_labels, pre_process, post_process , get_image


def is_central_member(user):
    return user.groups.filter(name='Central').exists()

def get_users_distribution_house(request):
    current_user = request.user

    if( is_central_member( current_user ) ):
        permitted_biponon_center = BipononCenter.objects.all().values_list('distribution_house_code', flat=True)
    else:
        permitted_biponon_center = ( UserPermission.objects.get(user=current_user) ).biponon_center.all().values_list('distribution_house_code', flat=True)
    
    list_of_dh = list(permitted_biponon_center)
    
    dh_list_str_temp = '", "'.join(str(e) for e in list_of_dh)
    dh_list_str = '"' + dh_list_str_temp + '"'

    return dh_list_str
def get_distributor(code):
    
    permitted_biponon_center = BipononCenter.objects.filter( distribution_house_code=code).values_list('name', flat=True)[:1].get()

    return permitted_biponon_center    
    
def date_range_generator(day,month,year,day2,month2,year2):   
    
    
    month_num = month2-month
    
    if(month_num !=0):
        if(month  in [9,4,6,11]):
            n_day2=30
        elif month == 2:
            n_day2=28
        else:
            n_day2=31

        date_query ="(day between "+ str(day) +" and "+str(n_day2)+" and month ="+str(month)+" and year ="+str(year)+")"
        
        for i in range(month+1,month2):
            if(i  in [9,4,6,11]):
                n_day2=30
            elif i == 2:
                n_day2=28
            else:
                n_day2=31        
            date_query =date_query + " or (day between "+ str(1) +" and "+str(n_day2)+" and month ="+str(i)+" and year ="+str(year)+")"
        date_query =date_query + " or (day between "+ str(1) +" and "+str(day2)+" and month ="+str(month2)+" and year ="+str(year)+")"    
    else:
        date_query ="(day between "+ str(day) +" and "+str(day2)+" and month ="+str(month)+" and year ="+str(year)+")"

    return "("+date_query+")"    


    #((day between 1 and 20 and month =1 and year =2019) or (day between 21 and 31 and month =12 and year =2018))




def get_data(bigquery_result,re_name):

    columns_name = [v.name for v in bigquery_result.schema]
    
    data = []

    for row in bigquery_result:
        d ={}
        for c in columns_name:
            #print(c)
            if c== "timestamp" or c== "Call_Timestamp": 
                date_time = datetime.datetime.fromtimestamp(row[c]/1000).strftime("%d/%m/%Y %I:%M %p").split(" ")
                d["Date"]=date_time[0]
                d["Time"]=date_time[1] + " "+date_time[2]
            elif c=="upload_time" or c == "Call_Upload_Time":
                date_time = datetime.datetime.fromtimestamp(row[c]/1000).strftime("%d/%m/%Y %I:%M %p").split(" ")
                d["Upload_Date"]=date_time[0]
                d["Upload_Time"]=date_time[1] + " " +date_time[2]

           
            elif c == "distributor_code":
                d[c]=get_distributor(row[c])    
            else:    
                d[c]= row[c]
            if c in re_name.keys():
               d[re_name[c]]= d[c]
               del d[c] 

        data.append(d)    

    return data




# Active merchandisers
# @api_view()
# def get_active_unique_merchandisers(request, day, month, year):

#     dh_list_str = get_users_distribution_house(request)
    
    

#     day = int(day)
#     month = int(month)
#     year = int(year)

#     QUERY = SELECT_ACTIVE_UNIQUE_MERCHANDISERS_ON_SPECIFIC_DATE.format( dh_list_str )
#     query_job = bigquery_client.query(QUERY)
#     rows = query_job.result()
#     active_merchandisers = [ { "mobile_no" : row.phone, "name" : row.name, "area" : row.area, "distribution_house" : get_distributor(row.distributor_code), "region" : row.region} for row in rows ]
#     return Response({'active_merchandisers': active_merchandisers})


# Count active merchandisers
@api_view()
def get_count_active_unique_merchandisers(request, day, month, year):

    dh_list_str = get_users_distribution_house(request)

    day = int(day)
    month = int(month)
    year = int(year)

    QUERY = COUNT_OF_ACTIVE_UNIQUE_MERCHANDISERS_ON_SPECIFIC_DATE.format(year, month, day, dh_list_str)
    
    query_job = bigquery_client.query(QUERY)
    

    query_result = [ row.ActiveMerchandisers for row in  query_job.result() ]

    if (len(query_result) < 1):
        return Response({"error" : "Query error"}, status=500)

    return Response({
        'count' : query_result[0]
    })



@api_view()
def get_count_active_unique_supervisers(request, day, month, year):
    
    dh= get_users_distribution_house(request)

    day = int(day)
    month = int(month)
    year = int(year)

    QUERY = COUNT_OF_ACTIVE_UNIQUE_SUPERVISERS_ON_SPECIFIC_DATE.format(year, month, day,dh)

    query_job = bigquery_client.query(QUERY)

    query_result = [ row.ActiveSuperviors for row in  query_job.result() ]

    if (len(query_result) < 1):
        return Response({"error" : "Query error"}, status=500)

    return Response({
        'count' : query_result[0]
    })


# Get list of visited outlets on specific date
# @api_view()
# def get_visited_outlets(request, day, month, year):
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     QUERY = VISITED_OUTLETS_ON_SPECIFIC_DATE.format(year, month, day)
#     query_job = bigquery_client.query(QUERY)
#     rows = query_job.result()
#     visited_outlets = [ {"mobile_no" : row.Outlets }  for row in rows]
#     return Response({
#         'visited_outlets': visited_outlets
#     })

# # Count of visited outlets on specific date
# @api_view()
# def get_count_visited_outlets(request, day, month, year):

#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)

#     QUERY = COUNT_OF_VISITED_OUTLETS_ON_SPECIFIC_DATE.format(year, month, day,dh)
#     #print(QUERY)

#     query_job = bigquery_client.query(QUERY)

#     query_result = [row.ActiveMerchandisers for row in query_job.result()]

#     if (len(query_result) < 1):
#         return Response({"error": "Query error"}, status=500)

#     return Response({
#         'count': query_result[0]
#     })


# @api_view()
# def get_count_closed_visited_outlets(request):

#     dh= get_users_distribution_house(request)

#     QUERY = COUNT_OF_UNIQUE_VISITED_CLOSED_OUTLETS.format(dh)

#     query_job = bigquery_client.query(QUERY)

#     query_result = [row.UniqueclosedOutlets for row in query_job.result()]

#     if (len(query_result) < 1):
#         return Response({"error": "Query error"}, status=500)

#     return Response({
#         'count': query_result[0]
#     })


# @api_view()
# def get_table_merchandisers_activity_summary(request, day, month, year):
#     year = int(year)
#     month = int(month)
#     day = int(day)

#     # Findout the yesterday date
#     yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)

#     QUERY_TODAY = TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE.format(year, month, day)
#     QUERY_YESTERDAY = TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE.format(yesterday_date.year, yesterday_date.month, yesterday_date.day)

#     query_job_today = bigquery_client.query(QUERY_TODAY)
#     query_job_yesterday = bigquery_client.query(QUERY_YESTERDAY)

#     # query_result = [row.UniqueclosedOutlets for row in query_job.result()]
#     rows_today = query_job_today.result()
#     rows_yesterday = query_job_yesterday.result()

#     result_today = {
#         row.Region : {
#             'ActiveMerchandisers' : row.ActiveMerchandisers,
#             'VisitedOutlets' : row.VisitedOutlets
#         } for row in rows_today
#     }
    
#     result_yesterday = {
#         row.Region: {
#             'ActiveMerchandisers': row.ActiveMerchandisers,
#             'VisitedOutlets': row.VisitedOutlets
#         } for row in rows_yesterday
#     }

#     normalized_today_data = table_summary_imputer3(result_today)
#     normalized_yesterday_data = table_summary_imputer3(result_yesterday)
    

#     dataSource = [
#         {"region" : region,
#          "active_merchandisers_today" : normalized_today_data[region]['ActiveMerchandisers'],
#          "active_merchandisers_yesterday" : normalized_yesterday_data[region]['ActiveMerchandisers'],
#           "outlets_visited_today" : normalized_today_data[region]['VisitedOutlets'],
#          "outlets_visited_yesterday" : normalized_yesterday_data[region]['VisitedOutlets']
#          }

#         for region in REGIONS3
#     ]

#     return Response({
#         'columns' : [
#             {
#                 "dataField" : "region",
#                 "text" : "Region",
#                 "sort" : True
#             },

#             {
#                 "dataField": "active_merchandisers_yesterday",
#                 "text": "Active Merchandisers Yesterday",
#                 "sort": True
#             }, {
#                 "dataField": "active_merchandisers_today",
#                 "text": "Active Merchandisers Today",
#                 "sort": True
#             }, {
#                 "dataField": "outlets_visited_yesterday",
#                 "text": "Outlets Visited Yesterday",
#                 "sort": True
#             }, {
#                 "dataField": "outlets_visited_today",
#                 "text": "Outlets Visited Today",
#                 "sort": True
#             }
#         ],
#         'dataSource' :   dataSource
#     })



@api_view()
def get_table_merchandisers_activity_summary_with_total(request, day, month, year):
    
    dh = get_users_distribution_house(request)
    year = int(year)
    month = int(month)
    day = int(day)

    # Findout the yesterday date
    yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)

    QUERY_TODAY = TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL.format(day,month,year,dh)
    QUERY_YESTERDAY = TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL.format(yesterday_date.day, yesterday_date.month, yesterday_date.year,dh)

    query_job_today = bigquery_client.query(QUERY_TODAY)
    query_job_yesterday = bigquery_client.query(QUERY_YESTERDAY)

    

    # query_result = [row.UniqueclosedOutlets for row in query_job.result()]
    rows_today = query_job_today.result()
    rows_yesterday = query_job_yesterday.result()

    

    result_today = {
        row.Region : {
            'ActiveMerchandisers' : row.ActiveMerchandisers,
            'VisitedOutlets' : row.VisitedOutlets
        } for row in rows_today
    }
    
    result_yesterday = {
        row.Region: {
            'ActiveMerchandisers': row.ActiveMerchandisers,
            'VisitedOutlets': row.VisitedOutlets
        } for row in rows_yesterday
    }

    normalized_today_data = table_summary_imputer(result_today)
    normalized_yesterday_data = table_summary_imputer(result_yesterday)
    

    dataSource = [
        {"region" : region,
         "active_merchandisers_today" : normalized_today_data[region]['ActiveMerchandisers'],
         "active_merchandisers_yesterday" : normalized_yesterday_data[region]['ActiveMerchandisers'],
          "outlets_visited_today" : normalized_today_data[region]['VisitedOutlets'],
         "outlets_visited_yesterday" : normalized_yesterday_data[region]['VisitedOutlets']
         }

        for region in REGIONS
    ]

    return Response({
        'columns' : [
            {
                "dataField" : "region",
                "text" : "Region",
                "sort" : True
            },

            {
                "dataField": "active_merchandisers_yesterday",
                "text": "Active Merchandisers Yesterday",
                "sort": True
            }, {
                "dataField": "active_merchandisers_today",
                "text": "Active Merchandisers Today",
                "sort": True
            }, {
                "dataField": "outlets_visited_yesterday",
                "text": "Outlets Visited Yesterday",
                "sort": True
            }, {
                "dataField": "outlets_visited_today",
                "text": "Outlets Visited Today",
                "sort": True
            }
        ],
        'dataSource' :   dataSource
    })


# @api_view()
# def get_table_supervisor_activity_summary(request, day, month, year):
#     year = int(year)
#     month = int(month)
#     day = int(day)

#     # Findout the yesterday date
#     yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)

#     QUERY_TODAY = TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE.format(year, month, day)
#     QUERY_YESTERDAY = TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE.format(yesterday_date.year, yesterday_date.month, yesterday_date.day)

#     query_job_today = bigquery_client.query(QUERY_TODAY)
#     query_job_yesterday = bigquery_client.query(QUERY_YESTERDAY)

#     # query_result = [row.UniqueclosedOutlets for row in query_job.result()]
#     rows_today = query_job_today.result()
#     rows_yesterday = query_job_yesterday.result()

#     result_today = {
#         row.Region : {
#             'ActiveSupervisors' : row.ActiveSupervisors,
#             'VisitedOutlets' : row.VisitedOutlets
#         } for row in rows_today
#     }

#     result_yesterday = {
#         row.Region: {
#             'ActiveSupervisors': row.ActiveSupervisors,
#             'VisitedOutlets': row.VisitedOutlets
#         } for row in rows_yesterday
#     }

#     normalized_today_data = table_summary_imputer4(result_today)
#     normalized_yesterday_data = table_summary_imputer4(result_yesterday)


#     dataSource = [
#         {"region" : region,
#          "active_supervisors_today" : normalized_today_data[region]['ActiveSupervisors'],
#          "active_supervisors_yesterday" : normalized_yesterday_data[region]['ActiveSupervisors'],
#           "outlets_visited_today" : normalized_today_data[region]['VisitedOutlets'],
#          "outlets_visited_yesterday" : normalized_yesterday_data[region]['VisitedOutlets']
#          }

#         for region in REGIONS4
#     ]

#     return Response({
#         'columns' : [
#             {
#                 "dataField" : "region",
#                 "text" : "Region",
#                 "sort" : True
#             },

#             {
#                 "dataField": "active_supervisors_yesterday",
#                 "text": "Active Supervisors Yesterday",
#                 "sort": True
#             }, {
#                 "dataField": "active_supervisors_today",
#                 "text": "Active Supervisors Today",
#                 "sort": True
#             }, {
#                 "dataField": "outlets_visited_yesterday",
#                 "text": "Outlets Visited Yesterday",
#                 "sort": True
#             }, {
#                 "dataField": "outlets_visited_today",
#                 "text": "Outlets Visited Today",
#                 "sort": True
#             }
#         ],
#         'dataSource' :   dataSource
#     })




@api_view()
def get_table_supervisor_activity_summary_with_total(request, day, month, year):
    
    dh = get_users_distribution_house(request)
    year = int(year)
    month = int(month)
    day = int(day)

    # Findout the yesterday date
    yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)

    QUERY_TODAY = TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL.format(day,month,year,dh)
    QUERY_YESTERDAY = TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL.format(yesterday_date.day, yesterday_date.month, yesterday_date.year,dh)
    #print(QUERY_TODAY)h, yesterday_date.day
    query_job_today = bigquery_client.query(QUERY_TODAY)
    query_job_yesterday = bigquery_client.query(QUERY_YESTERDAY)

    # query_result = [row.UniqueclosedOutlets for row in query_job.result()]
    rows_today = query_job_today.result()
    rows_yesterday = query_job_yesterday.result()

    result_today = {
        row.Region : {
            'ActiveSupervisors' : row.ActiveSupervisors,
            'VisitedOutlets' : row.VisitedOutlets
        } for row in rows_today
    }

    result_yesterday = {
        row.Region: {
            'ActiveSupervisors': row.ActiveSupervisors,
            'VisitedOutlets': row.VisitedOutlets
        } for row in rows_yesterday
    }

    normalized_today_data = table_summary_imputer_2(result_today)
    normalized_yesterday_data = table_summary_imputer_2(result_yesterday)
    
    re2 = [
    'BARISAL',
    'BOGRA',
    'CHITTAGONG',
    'COMILLA',
    'DHAKA NORTH',
    'DHAKA SOUTH',
    'KHULNA',
    'MYMENSINGH',
    'RANGPUR',
    'SYLHET',
    'TOTAL'
    ]

    dataSource = [
        {"region" : region,
         "active_supervisors_today" : normalized_today_data[region]['ActiveSupervisors'],
         "active_supervisors_yesterday" : normalized_yesterday_data[region]['ActiveSupervisors'],
          "outlets_visited_today" : normalized_today_data[region]['VisitedOutlets'],
         "outlets_visited_yesterday" : normalized_yesterday_data[region]['VisitedOutlets']
         }

        for region in re2
    ]

    return Response({
        'columns' : [
            {
                "dataField" : "region",
                "text" : "Region",
                "sort" : True
            },

            {
                "dataField": "active_supervisors_yesterday",
                "text": "Active Supervisors Yesterday",
                "sort": True
            }, {
                "dataField": "active_supervisors_today",
                "text": "Active Supervisors Today",
                "sort": True
            }, {
                "dataField": "outlets_visited_yesterday",
                "text": "Outlets Visited Yesterday",
                "sort": True
            }, {
                "dataField": "outlets_visited_today",
                "text": "Outlets Visited Today",
                "sort": True
            }
        ],
        'dataSource' :   dataSource
    })





# New unique outlets visited
@api_view()
def get_table_unique_outlets_visited(request,user):
    
    dh= get_users_distribution_house(request)
    
    QUERY = TABLE_NEW_UNIQUE_OUTLETS_VISITED.format(user.lower())

    query_job = bigquery_client.query(QUERY).to_dataframe().to_dict(orient="records")
    #print(query_job)

    # result = query_job.result()

    # data = []

    # for row in result:
    #     data.append({
    #         'date' : row.Date,
    #         'barisal' : row.BARISAL,
    #         'sylhet' : row.SYLHET,
    #         'chittagong' : row.CHITTAGONG,
    #         'bogra' : row.BOGRA,
    #         'dhaka_south' : row.DHAKA_SOUTH,
    #         'dhaka_north' : row.DHAKA_NORTH,
    #         'comilla' : row.COMILLA,
    #         'khulna' : row.KHULNA,
    #         'mymensingh' : row.MYMENSINGH,
    #         'rangpur' : row.RANGPUR,
    #         'total' : row.TOTAL
    #     })


    return Response({
        
        'data' : query_job
    })


# New unique outlets visited
@api_view()
def get_table_unique_outlets_visited_superviser(request):
    

    QUERY = TABLE_NEW_UNIQUE_OUTLETS_VISITED_SUPERVISER

    query_job = bigquery_client.query(QUERY)

    result = query_job.result()

    data = []

    for row in result:
        data.append({
            'date' : row.Date,
            'barisal' : row.BARISAL,
            'sylhet' : row.SYLHET,
            'chittagong' : row.CHITTAGONG,
            'bogra' : row.BOGRA,
            'dhaka_north' : row.DHAKA_NORTH,
            'dhaka_south' : row.DHAKA_SOUTH,
            'comilla' : row.COMILLA,
            'khulna' : row.KHULNA,
            'mymensingh' : row.MYMENSINGH,
            'rangpur' : row.RANGPUR,
            'total' : row.TOTAL
        })


    return Response({
        'columns' : [{
            "dataField": "date",
            "text": "Date",
            "sort": True
          },{
            "dataField": "barisal",
            "text": "Barisal",
            "sort": True
          },{
            "dataField": "bogra",
            "text": "Bogra",
            "sort": True
          },{
            "dataField": "chittagong",
            "text": "Chittagong",
            "sort": True
          },{
            "dataField": "comilla",
            "text": "Comilla",
            "sort": True
          },{
            "dataField": "dhaka",
            "text": "Dhaka",
            "sort": True
          },{
            "dataField": "khulna",
            "text": "Khulna",
            "sort": True
          },{
            "dataField": "mymensingh",
            "text": "Mymensingh",
            "sort": True
          },{
            "dataField": "rangpur",
            "text": "Rangpur",
            "sort": True
          },{
            "dataField": "sylhet",
            "text": "Sylhet",
            "sort": True
          }],
        'data' : data
    })



@api_view()
def get_total_calls_all_day(request, user):
    
    dh= get_users_distribution_house(request)
    user=user.lower()
    today=datetime.datetime.today().__str__().split(" ")[0].split("-")
    print(today)
    day = int(today[2])
    month = int(today[1])
    year = int(today[0])
    

    # Findout the yesterday date
    # yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)

    before_9_am = {
        "from" : int( (datetime.datetime(year, month, day, 6, 0, 0) ).timestamp() * 1000 ) ,
        "to" : int( (datetime.datetime(year, month, day, 9, 0, 0)).timestamp() * 1000 )
    }

    within_9_10_am = {
        "from": int( (datetime.datetime(year, month, day, 9, 0, 0)).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 10, 0, 0)).timestamp() * 1000 )
    }

    within_10_11_am = {
        "from": int( (datetime.datetime(year, month, day, 10, 0, 0)).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 11, 0, 0) ).timestamp() * 1000 )
    }

    within_11_12_pm = {
        "from": int( (datetime.datetime(year, month, day, 11, 0, 0) ).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 12, 0, 0) ).timestamp() * 1000 )
    }

    within_12_1_pm = {
        "from": int( (datetime.datetime(year, month, day, 12, 0, 0)).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 13, 0, 0) ).timestamp() * 1000 )
    }

    within_1_2_pm = {
        "from": int( (datetime.datetime(year, month, day, 13, 0, 0) ).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 14, 0, 0)).timestamp() * 1000 )
    }

    within_2_3_pm = {
        "from": int( (datetime.datetime(year, month, day, 14, 0, 0)).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 15, 0, 0)).timestamp() * 1000 )
    }

    within_3_12_am = {
        "from": int( (datetime.datetime(year, month, day, 15, 0, 0)).timestamp() * 1000 ),
        "to": int( (datetime.datetime(year, month, day, 23, 0, 0)).timestamp() * 1000 )
    }

    # print(before_9_am)

    QUERY_BEFORE_9 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(before_9_am['from'], before_9_am['to'],dh,user)

    QUERY_WITHIN_9_10 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_9_10_am['from'], within_9_10_am['to'],dh,user)
    QUERY_WITHIN_10_11 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_10_11_am['from'], within_10_11_am['to'],dh,user)
    QUERY_WITHIN_11_12 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_11_12_pm['from'], within_11_12_pm['to'],dh,user)
    QUERY_WITHIN_12_1 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_12_1_pm['from'], within_12_1_pm['to'],dh,user)
    QUERY_WITHIN_1_2 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_1_2_pm['from'], within_1_2_pm['to'],dh,user)
    QUERY_WITHIN_2_3 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_2_3_pm['from'], within_2_3_pm['to'],dh,user)
    QUERY_AFTER_3 = TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY.format(within_3_12_am['from'], within_3_12_am['to'],dh,user)

    before_9_result = bigquery_client.query(QUERY_BEFORE_9).result()
    within_9_10_result = bigquery_client.query(QUERY_WITHIN_9_10).result()
    within_10_11_result = bigquery_client.query(QUERY_WITHIN_10_11).result()
    within_11_12_result = bigquery_client.query(QUERY_WITHIN_11_12).result()
    within_12_1_result = bigquery_client.query(QUERY_WITHIN_12_1).result()
    within_1_2_result = bigquery_client.query(QUERY_WITHIN_1_2).result()
    within_2_3_result = bigquery_client.query(QUERY_WITHIN_2_3).result()
    after_3_result = bigquery_client.query(QUERY_AFTER_3).result()
    

    update_data = {
        'before_9am' : {
            region: 0 for region in REGIONS5
        },

        'between_9am_10am' : {
            region: 0 for region in REGIONS5
        },

        'between_10am_11am' : {
            region: 0 for region in REGIONS5
        },

        'between_11am_12pm' : {
            region: 0 for region in REGIONS5
        },

        'between_12pm_1pm' : {
            region: 0 for region in REGIONS5
        },

        'between_1pm_2pm' : {
            region: 0 for region in REGIONS5
        },

        'between_2pm_3pm' : {
            region: 0 for region in REGIONS5
        },

        'after_3pm' : {
            region: 0 for region in REGIONS5
        }
    }

    for result in before_9_result:
        result = dict(result)
        update_data['before_9am'][result['region']] = result['OUTLETS_COUNT']

    for result in within_9_10_result:
        result = dict(result)
        update_data['between_9am_10am'][result['region']] = result['OUTLETS_COUNT']

    for result in within_10_11_result:
        result = dict(result)
        update_data['between_10am_11am'][result['region']] = result['OUTLETS_COUNT']

    for result in within_11_12_result:
        result = dict(result)
        update_data['between_11am_12pm'][result['region']] = result['OUTLETS_COUNT']

    for result in within_12_1_result:
        result = dict(result)
        update_data['between_12pm_1pm'][result['region']] = result['OUTLETS_COUNT']

    for result in within_1_2_result:
        result = dict(result)
        update_data['between_1pm_2pm'][result['region']] = result['OUTLETS_COUNT']

    for result in within_2_3_result:
        result = dict(result)
        update_data['between_2pm_3pm'][result['region']] = result['OUTLETS_COUNT']

    for result in after_3_result:
        result = dict(result)
        update_data['after_3pm'][result['region']] = result['OUTLETS_COUNT']


    dataSource = [
        {'region': region,
         'before_9am' : update_data['before_9am'][region],
         'between_9am_10am' : update_data['between_9am_10am'][region],
         'between_10am_11am' : update_data['between_10am_11am'][region],
         'between_11am_12pm' : update_data['between_11am_12pm'][region],
         'between_12pm_1pm' : update_data['between_12pm_1pm'][region],
         'between_1pm_2pm' : update_data['between_1pm_2pm'][region],
         'between_2pm_3pm' : update_data['between_2pm_3pm'][region],
         'after_3pm' : update_data['after_3pm'][region]
         } for region in REGIONS5
    ]

    total_count = {
        'before_9am' : 0,
        'between_9am_10am' : 0,
        'between_10am_11am' : 0,
        'between_11am_12pm' : 0,
        'between_12pm_1pm' : 0,
        'between_1pm_2pm' : 0,
        'between_2pm_3pm' : 0,
        'after_3pm' : 0
    }

    for source in dataSource:
        for key in total_count.keys():
            total_count[key] += source[key]

    total_count['region'] = 'TOTAL'
    dataSource.append(total_count)

    # columns = [{"text" : "Region" , "dataField" : "region"}]
    # columns.extend({
    #     "text" : key.replace('_', ' ').capitalize(),
    #     "dataField" : key
    # } for key in update_data.keys())


    return Response({
    #    "columns" : columns,
       "data" : dataSource
    })


# @api_view()
# def get_outlet_details(request, outlet_wallet_number):
#     QUERY = GET_OUTLET_DETAILS_BY_WALLET_NO.format(str(outlet_wallet_number))

#     result = bigquery_client.query(QUERY).result()

#     data = []

#     for row in result:
#         data.append(dict(row))

#     if len(data) < 1:
#         return Response({"error" : "No data found"}, status=404)

#     else:
#         del data[0]['upload_time']
#         del data[0]['timestamp']

#     return Response({
#         "columns" : [
#             {"text" : key, "dataField" : key} for key in data[0].keys()
#         ],

#         "dataSource" : data
#     })


# @api_view()
# def get_merchandiser_details(request, whitelisting_number):
#     QUERY = GET_MERCHANDISER_DETAILS_BY_WHITELISTING_NUMBER.format(whitelisting_number)

#     result = bigquery_client.query(QUERY).result()

#     data = []

#     for row in result:
#         data.append(dict(row))

#     if len(data) < 1:
#         return Response({"error": "No data found"}, status=404)

#     return Response({
#         "columns": [
#             {"text": key.replace('_', ' ').capitalize(), "dataField": key} for key in data[0].keys()
#         ],

#         "dataSource": data
#     })


# @api_view()
# def get_outlets_by_reporter(request, phone, day, month, year):
#     month = int(month)
#     year = int(year)
#     day = int(day)

#     QUERY = GET_OUTLETS_GROUPBY_REPORTER.format(str(phone), year, month, day)

#     result = bigquery_client.query(QUERY).result()

#     data = []

#     for row in result:
#         data.append({
#             'wallet_number' : row.wallet_number,
#             'store_name' : row.store_name,
#             'address' : row.address
#         })

#     return Response({
#         'merchandiser' : phone,
#         'outlets' : data
#     })


# @api_view()
# def get_active_merchandisers(request, day, month, year, limit, offset):

#     dh_list_str = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     limit = int(limit)
#     offset = int(offset)

#     QUERY = GET_ACTIVE_OUTLETS.format(year, month, day, limit, offset)

#     result = bigquery_client.query(QUERY).result()

#     data = [ row.ActiveOutlet for row in result ]

#     return Response({
#         'active_outlets' : data
#     })


# @api_view()
# def get_count_visited_unique_outlets(request, month, year):

#     dh= get_users_distribution_house(request)
#     month = int(month)
#     year = int(year)

#     QUERY = VISITED_UNIQUE_OUTLET_COUNT_ON_MONTH.format(year, month,dh)

#     result = bigquery_client.query(QUERY).result()

#     count = [row.ActiveOutlets for row in result]

#     return Response({
#         'count' : count[0]
#     })


# @api_view()
# def get_report_geo_tagging(request, day, month, year, limit, offset):
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     limit = int(limit)
#     offset = int(offset)

#     QUERY = GEO_TAGGING_REPORT_PAGINATED.format(year, month, day, limit, offset)

#     result = bigquery_client.query(QUERY).result()

#     COLUMNS = [
#         'region',
#         'area',
#         'distributor_code',
#         'whitelisting_number',
#         'agent_count',
#         'merchant_count',
#         'closed_outlet',
#         'inactive_outlet',
#         'address_not_ok_found'
#     ]

#     result = [ {
#         'region' : row.region,
#         'area' : row.area,
#         'distributor_code' : get_distributor(row.DistributionHouse),
#         'whitelisting_number' : row.WhitelistingNumber,
#         'agent_count' : row.AgentCount,
#         'merchant_count' : row.MerchantCount,
#         'closed_outlet' : row.ClosedOutlet,
#         'inactive_outlet' : row.InActiveOutlet,
#         'address_not_ok_found' : row.AddressNotOkFound
#     } for row in result ]

#     return Response({
#         'column' : [
#             {"text" : column.replace('_', ' ').capitalize() , "dataField" : column} for column in COLUMNS
#         ],
#         'data' : result
#     })


@api_view()
def get_a_merchandiser_supervisor(request, whitelisting_number):
    
    dh = get_users_distribution_house(request)
    QUERY = GET_A_MERCHANDISER_SUPERVISOR.format(whitelisting_number,dh)

    bigquery_result = bigquery_client.query(QUERY).result()

    # data = [
    #     {
    #         "Whitelisting_Number" : row.Whitelisting_Number,
    #         "region" : row.region,
    #         "area" : row.area,
    #         "territory" : row.territory,
    #         "name" : row.name,
    #         "distributor_code" : get_distributor(row.distributor_code)
    #     } for row in bigquery_result
    # ]

    data = get_data(bigquery_result,re_name={})

    if len(data) == 0:
        data = "no data"

    return Response({
        'data' : data
    })
    

@api_view()
def get_an_outlet(request, wallet):
    
    dh =get_users_distribution_house(request)
    QUERY = GET_AN_OUTLET.format(wallet,dh)

    bigquery_result = bigquery_client.query(QUERY).result()

    # data = [
    #     {
    #         "region" : row.region,
    #         "area" : row.area,
    #         "territory" : row.territory,
    #         "distributor_code" : get_distributor(row.distributor_code),
    #         "wallet_number" : row.wallet_number,
    #         "year" : row.year,
    #         "month" : row.month,
    #         "day" : row.day,
    #         "report_key" : row.report_key,
    #         "address" : row.address,
    #         "agent_or_merchant" : row.agent_or_merchant,
    #         "answer1" : row.answer1,
    #         "answer2" : row.answer2,
    #         "bmcc" : row.bmcc,
    #         "checkin_lat" : row.checkin_lat,
    #         "checkin_lng" : row.checkin_lng,
    #         "closed" : row.closed,
    #         "file_path" : row.file_path,
    #         "file_path2" : row.file_path2,
    #         "is_inactive" : row.is_inactive,
    #         "not_found" : row.not_found,
    #         "lat" : row.lat,
    #         "lng" : row.lng,
    #         "reporter" : row.reporter,
    #         "revalidate_lat" : row.revalidate_lat,
    #         "revalidate_lng" : row.revalidate_lng,
    #         "store_name" : row.store_name,
    #         "timestamp" : int(row.timestamp),
    #         "updated_address" : row.updated_address,
    #         "updated_store_name" : row.updated_store_name,
    #         "upload_time" : int(row.upload_time),
    #         "qr":row.qr,
    #     } for row in bigquery_result
    # ]

    data = get_data(bigquery_result,re_name={})

    return Response({
        "data" : data
    })


# @api_view()
# def get_outlet_geolocation(request, wallet):
#     QUERY = GET_GEOLOCATION_BY_OUTLET.format(wallet)

#     bigquery_result = bigquery_client.query(QUERY).result()

#     # data = [
#     #     {'wallet_number' : row.wallet_number,
#     #      'store_name' : row.store_name,
#     #      'address' : row.address,
#     #      'revalidate_lat' : row.revalidate_lat,
#     #      'revalidate_lng' : row.revalidate_lng
#     #     } for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result,re_name={})

#     return Response({
#         'data' : data
#     })


# @api_view()
# def get_outlets_geo_location(request):
#     QUERY = GET_GEOLOCATION_OF_OUTLETS
    
#     bigquery_result = bigquery_client.query(QUERY).result()

#     # data = [
#     #     {'wallet_number' : row.wallet_number,
#     #      'store_name' : row.store_name,
#     #      'address' : row.address,
#     #      'revalidate_lat' : row.revalidate_lat,
#     #      'revalidate_lng' : row.revalidate_lng
#     #     } for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result,re_name={})

#     return Response({
#         'data' : data
#     })


# @api_view()
# def get_latest_outlet_info(request, limit):
    
#     dh= get_users_distribution_house(request)
    
#     limit = int(limit)

#     QUERY = GET_LAST_N_OUTLET_INFORMATION.format(limit,dh)

#     bigquery_result = bigquery_client.query(QUERY).result()

#     # data = [
#     #     {'region' : row.region,
#     #      'area' : row.area,
#     #      'territory' : row.territory,
#     #      'distributor_code' : get_distributor(row.distributor_code),
#     #      'wallet_number' : row.wallet_number,
#     #      'year' : row.year,
#     #      'month' : row.month,
#     #      'day' : row.day,
#     #      'report_key' : row.report_key,
#     #      'address' : row.address,
#     #      'agent_or_merchant' : row.agent_or_merchant,
#     #      'answer1' : row.answer1,
#     #      'answer2' : row.answer2,
#     #      'bmcc' : row.bmcc,
#     #      'checkin_lat' : row.checkin_lat,
#     #      'checkin_lng' : row.checkin_lng,
#     #      'closed' : row.closed,
#     #      'file_path' : row.file_path,
#     #      'is_inactive' : row.is_inactive,
#     #      'lat' : row.lat,
#     #      'lng' : row.lng,
#     #      'reporter' : row.reporter,
#     #      'revalidate_lat' : row.revalidate_lat,
#     #      'revalidate_lng' : row.revalidate_lng,
#     #      'store_name' : row.store_name,
#     #      'timestamp' : int(row.timestamp),
#     #      'updated_address' : row.updated_address,
#     #      'updated_store_name' : row.updated_store_name,
#     #      'upload_time' : int(row.upload_time),
#     #      'qr' : row.qr,
#     #      } for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result,re_name={})
    
#     if len(data) == 0:
#         return Response({
#         "data" : "no data"
#     })

#     return Response({
#         "data" : data
#     })


# @api_view()
# def merchandisers_didnt_make_a_call(request, day, month, year):
    
#     dh = get_users_distribution_house(request)
    
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     day2 = int(day2)
#     month2 = int(month2)
#     year2 = int(year2)

#     date = "{}-{}-{}".format( str(day).zfill(2),
#                               str(month).zfill(2),
#                               year)

#     date2 = "{}-{}-{}".format( str(day2).zfill(2),
#                               str(month2).zfill(2),
#                               year2)                          
    
  

#     time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
#     time_stamp2=datetime.datetime(year, month, day, 23, 59,59).timestamp()*1000
#     QUERY = MERCHANDISER_DIDNT_MAKE_ANY_CALL.format(time_stamp1,time_stamp2,dh)
#     print(QUERY)
#     bigquery_result = bigquery_client.query(QUERY).result()

#     # data = [
#     #     {
#     #         "Whitelisting_Number" : row.Whitelisting_Number,
#     #         "Region" : row.Region,
#     #         "Distributor_Code" : get_distributor(row.Distributor_Code),
#     #         "Target_Call" : row.Target_Call
#     #     }
    
#     #     for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result,re_name={})

#     COLUMNS = [
#         "Whitelisting_Number",
#         "Region",
#         "Distributor_Code" ,
#         "Target_Call"
#     ]

#     download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
#     file_name = f'merchandiser_who_didnt_make_a_single_call_from_{date}_to_{date2}_{download_time}.xlsx'
#     file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
#     response = HttpResponse(content_type="application/vnd.ms-excel")
#     response['Content-Disposition'] = f'attachment; filename={file_name}'
#     pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")

#     return response


# @api_view()
# def get_target_vs_achievement(request, day, month, year,day2,month2,year2):
#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     day2=int(day2)
#     month2=int(month2)
#     year2=int(year2)

#     date = '{}-{}-{}'.format(str(day).zfill(2), str(month).zfill(2), year)
#     date2 = '{}-{}-{}'.format(str(day2).zfill(2), str(month2).zfill(2), year2)

#     date_range=date_range_generator(day, month, year,day2,month2,year2)
  

#     time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
#     time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

#     QUERY = TARGET_VS_ACHIEVEMENT.format(date_range,dh)
#     print(QUERY)
#     bigquery_result = bigquery_client.query(QUERY).result()
    
#     # data = [
#     #     {"Region" : row.Region,
#     #      "CallTarget" : row.CallTarget,
#     #      "Achievement" : row.Achievement,
#     #      "Percentage" : row.Percentage,
#     #      "Assigned_Merchandiser" : row.Assigned_Merchandiser,
#     #      "Active_Merchandiser" : row.Active_Merchandiser,
#     #      "Inactive" : row.Inactive
#     #      }
    
#     #     for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result,re_name={})

#     COLUMNS = ["Region", "CallTarget", "Achievement", "Percentage", "Assigned_Merchandiser", "Active_Merchandiser", "Inactive"]


#     download_time = datetime.datetime.now().__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
#     file_name = f'Merchandiser_call_targets_vs_call_achievements_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
#     file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
#     response = HttpResponse(content_type="application/vnd.ms-excel")
#     response['Content-Disposition'] = f'attachment; filename={file_name}'
#     df = pd.DataFrame(data)[COLUMNS]

#     total_call_target = df['CallTarget'].sum()
#     total_achievement = df['Achievement'].sum()
#     percentage = int( total_achievement * 100 / total_call_target )
#     total_assigned_merchandiser = df['Assigned_Merchandiser'].sum()
#     total_active_merchandiser = df['Active_Merchandiser'].sum()
#     total_inactive = df['Inactive'].sum()

#     df.append({
#         'Region' : 'TOTAL',
#         'CallTarget' : total_call_target,
#         'Achievement' : total_achievement,
#         'Percentage' : percentage,
#         'Assigned_Merchandiser' : total_assigned_merchandiser,
#         'Active_Merchandiser' : total_active_merchandiser,
#         'Inactive' : total_inactive
#     }, ignore_index=True)[COLUMNS].to_excel(response, index=False, encoding="utf-16")



#     return response
    
    
# @api_view()
# def get_area(request, region):

#     dh=get_users_distribution_house(request)

    
#     QUERY = GET_AREA_BY_REGION.format(region.replace("_"," "),dh)
    

#     result = bigquery_client.query(QUERY).result()

#     data = [
#             row.area.replace(' ','_') for row in result
#         ]

#     return Response({
#         "data" : data
#     })


# @api_view()
# def get_territory(request, area,user):
    
#     dh = get_users_distribution_house(request)
#     if user == "0":
    
      
#        QUERY = GET_TERRITORY_BY_AREA.format(area.replace("_"," "),dh)

#     elif user == "MERCHANDISER":
#         QUERY = GET_TERRITORY_BY_AREA_MERCAHNDISER.format(area.replace("_"," "),dh)

#     elif user == "SUPERVISOR":
#          QUERY = GET_TERRITORY_BY_AREA_SUPERVISOR.format(area.replace("_"," "),dh)   


#     result = bigquery_client.query(QUERY).result()

#     data = [
#             row.territory for row in result
#         ]

#     return Response({
#         "data" : data
#     })


# @api_view()
# def get_distributor_codes(request, territory):

#     dh = get_users_distribution_house(request)
    

#     QUERY = GET_DISTRIBUTOR_CODE_BY_TERRITORY.format(territory, dh)

#     result = bigquery_client.query(QUERY).result()

#     data = [
#             get_distributor(row.distributor_code) for row in result
#         ]

#     return Response({
#         'data' : data
#     })


# @api_view()
# def get_region_list(request,user):
#     # QUERY = GET_REGION_LIST

#     # result = bigquery_client.query(QUERY).result()
    
#     region_all = ['BARISAL','BOGRA', 'CHITTAGONG', 'COMILLA','DHAKA_NORTH','DHAKA_SOUTH', 'KHULNA','MYMENSINGH', 'RANGPUR', 'SYLHET']

      

   

#     return Response({
#         'data' : region_all
#     })


# @api_view()
# def get_merchandisers_by_dh_code(request, code):
    
#     dh = get_users_distribution_house(request)
#     QUERY = GET_MERCHANDISERS_LIST_BY_DISTRIBUTOR_CODE.format(code,dh)

#     result = bigquery_client.query(QUERY).result()

#     data = [
#         row.whitelisting_number for row in result
#     ]

#     return Response({
#         'data' : data
#     })


@api_view()
def get_target_vs_achievement_by_supervisor(request, day, month, year,day2,month2,year2):
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)

    date = "{}-{}-{}".format( str(day).zfill(2),
                              str(month).zfill(2),
                              year)
    
    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)


    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    
    # days,times=list_date_range[0]
    #print(days)
    date_range=date_range_generator(day, month, year,day2,month2,year2)
    QUERY=GET_SUPERVISOR_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE.format(date_range,time_stamp1,time_stamp2,dh)

    QUERY = "select * except(user_type) from (" + QUERY + ")"
    print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()


    data = get_data(bigquery_result,re_name={})
    



    COLUMNS = [k for k in data[0].keys()]
    #print(COLUMNS)

    download_time = datetime.datetime.now().__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'Individual_call_targets_vs_call_achievements_by_supervisors_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")


    return response

@api_view()
def get_target_vs_achievement_by_merchandiser(request, day, month, year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)

    date = "{}-{}-{}".format( str(day).zfill(2),
                              str(month).zfill(2),
                              year)
    
    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)



    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    
    # days,times=list_date_range[0]
    date_range=date_range_generator(day, month,year,day2,month2,year2)
    QUERY=GET_MERCHANDISER_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE.format(date_range,time_stamp1,time_stamp2,dh)

   
    QUERY = "select * except(user_type) from (" + QUERY + ")"    
    print(QUERY)
    # with open(f'{settings.BASE_DIR}/dumps/query.txt',"w") as f:
    #     f.write(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()

    data = get_data(bigquery_result,re_name={})
    

    
    COLUMNS = [k for k in data[0].keys()]
    #print(COLUMNS)

    download_time = datetime.datetime.now().__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'Individual_call_targets_vs_call_achievements_by_merchandisers_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")

    return response

# @api_view()
# def get_target_vs_achievement_by_merchandiser_number(request, whitelisting_number, day, month, year):
#     day = int(day)
#     month = int(month)
#     year = int(year)

#     date = "{}-{}-{}".format( str(day).zfill(2),
#                               str(month).zfill(2),
#                               year)

#     QUERY = TARGET_VS_ACHIEVEMENT_BY_MERCHANDISER_NUMBER.format(whitelisting_number, day, month, year)

#     bigquery_result = bigquery_client.query(QUERY).result()

#     data =  {"data" : 
#         {
#             'Whitelisting_Number' : row.Whitelisting_Number,
#             'Target_Call' : row.Target_Call,
#             'Total_Call' : row.Total_Call,
#             'Unique_Call' :  row.Unique_Call,
#             'Achievement_Percentage' : row.Achievement_Percentage
            
#         } for row in bigquery_result
#     }

#     return Response(data)





# @api_view()
# def get_target_vs_achievement_supervisor(request, day, month, year,day2,month2,year2):
    
#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     day2 = int(day2)
#     month2 = int(month2)
#     year2 = int(year2)

#     date = '{}-{}-{}'.format(str(day).zfill(2), str(month).zfill(2), year)

    
#     date = str(day)+ "_" + str(month)+ "_" + str(year)
#     date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

#     time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
#     time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    
#     date_range=date_range_generator(day, month, year,day2,month2,year2)
#     QUERY = TARGET_VS_ACHIEVEMENT_SUPERVISOR.format(date_range,dh )
#     #print(QUERY)

#     bigquery_result = bigquery_client.query(QUERY).result()
    
#     # data = [
#     #     {"Region" : row.Region,
#     #      "CallTarget" : row.CallTarget,
#     #      "Achievement" : row.Achievement,
#     #      "Percentage" : row.Percentage,
#     #      "Assigned_Supervisor" : row.Assigned_Supervisor,
#     #      "Active_Supervisor" : row.Active_Supervisor,
#     #      "Inactive" : row.Inactive
#     #      }
    
#     #     for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result,re_name={})

#     COLUMNS = ["Region", "CallTarget", "Achievement", "Percentage", "Assigned_Supervisor", "Active_Supervisor", "Inactive"]


#     download_time = datetime.datetime.now().__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
#     file_name = f'Supervisor_call_targets_vs_call_achievements_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
#     file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
#     response = HttpResponse(content_type="application/vnd.ms-excel")
#     response['Content-Disposition'] = f'attachment; filename={file_name}'
#     df = pd.DataFrame(data)[COLUMNS]

#     total_call_target = df['CallTarget'].sum()
#     total_achievement = df['Achievement'].sum()
#     percentage = int( total_achievement * 100 / total_call_target )
#     total_assigned_supervisor = df['Assigned_Supervisor'].sum()
#     total_active_supervisor = df['Active_Supervisor'].sum()
#     total_inactive = df['Inactive'].sum()

#     df.append({
#         'Region' : 'TOTAL',
#         'CallTarget' : total_call_target,
#         'Achievement' : total_achievement,
#         'Percentage' : percentage,
#         'Assigned_Supervisor' : total_assigned_supervisor,
#         'Active_Supervisor' : total_active_supervisor,
#         'Inactive' : total_inactive
#     }, ignore_index=True)[COLUMNS].to_excel(response, index=False, encoding="utf-16")


#     return response



# @api_view()
# def get_visited_outlets_by_merchandiser(request, whitelisting_number, day, month, year):
#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
    
#     time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
#     time_stamp2=datetime.datetime(year, month, day, 23, 59,59).timestamp()*1000

#     QUERY = VISITED_OUTLETS_BY_MERCHANDISER_ON_A_GIVEN_DATE.format( whitelisting_number, time_stamp1,time_stamp2,dh)
#     print(QUERY)

#     bigquery_result = bigquery_client.query(QUERY).result()

#     # result =  [
        
#     #     {'region' : row.region,
#     #      'area' : row.area,
#     #      'territory' : row.territory,
#     #      'distributor_code' : get_distributor(row.distributor_code),
#     #      'wallet_number' : row.wallet_number,
#     #      'year' : row.year,
#     #      'month' : row.month,
#     #      'day' : row.day,
#     #      'report_key' : row.report_key,
#     #      'address' : row.address,
#     #      'agent_or_merchant' : row.agent_or_merchant,
#     #      'answer1' : row.answer1,
#     #      'answer2' : row.answer2,
#     #      'bmcc' : row.bmcc,
#     #      'checkin_lat' : row.checkin_lat,
#     #      'checkin_lng' : row.checkin_lng,
#     #      'closed' : row.closed,
#     #      'file_path' : row.file_path,
#     #      'file_path2' : row.file_path2,
#     #      'is_inactive' : row.is_inactive,
#     #      'not_found' : row.not_found,
#     #      'lat' : row.lat,
#     #      'lng' : row.lng,
#     #      'reporter' : row.reporter,
#     #      'revalidate_lat' : row.revalidate_lat,
#     #      'revalidate_lng' : row.revalidate_lng,
#     #      'store_name' : row.store_name,
#     #      'timestamp' : int(row.timestamp),
#     #      'updated_address' : row.updated_address,
#     #      'updated_store_name' : row.updated_store_name,
#     #      'upload_time' : int(row.upload_time),
#     #      'qr' : row.qr,
#     #      'user_photo' : row.user_photo,
         
            
#     #     } for row in bigquery_result
#     # ]

#     result = get_data(bigquery_result,re_name={})
    
#     count=len(result)
#     if count == 0:
#         result = "no data"

#     return Response({
#         'count' : count,
#         'data' : result
#     })



# # get phone number and pic url
# @api_view()
# def get_phone_and_pic_url(request, day, month, year,limit):
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     limit = int(limit)

#     QUERY = GET_PHONE_AND_PIC_URL.format(day, month, year,limit)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "phone" : row.wallet_number,
#             "pic_url" : row.file_path
 
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })

# # get outlets visitors
# @api_view()
# def get_outlet_visitors(request, wallet):
#     QUERY = GET_OUTLET_VISITORS.format(wallet)

#     bigquery_result = bigquery_client.query(QUERY).result()

#     # data = [
#     #     {'merchandiser' : row.merchandiser,
#     #      'day' : row.day,
#     #      'month' : row.month,
#     #      'year' : row.year
         
#     #     } for row in bigquery_result
#     # ]

#     data = get_data(bigquery_result, re_name= {})

#     return Response({
#         'data' : data
#     })


# # get region_wise_target_vs_achievement_on_specific_date
# @api_view()
# def get_region_wise_target_vs_achievement_on_specific_date(request, day, month, year):
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
#     time_stamp2=datetime.datetime(year, month, day, 23, 59,59).timestamp()*1000

#     date_range=date_range_generator(day,month,year,day,month,year)
#     QUERY = GET_REGION_WISE_TARGET_VS_ACHIEVEMENT_ON_SPECIFIC_DATE.format(date_range)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "region" : row.region,
#             "call_target" : row.call_target,
#             "achievement" : row.achievement

 
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
 
 
#  # get_count_unique_inactive_outlets_on_specific_month
# @api_view()
# def get_count_unique_inactive_outlets_on_specific_month(request,month):
#     dh = get_users_distribution_house(request)
#     month = int(month)
    
    

#     QUERY = GET_COUNT_UNIQUE_INACTIVE_OUTLETS_ON_SPECIFIC_MONTH.format(month,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_inactive_outlets" : row.total_inactive_outlets,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
 
# # get_count_unique_not_found_outlets_on_specific_month
# @api_view()
# def get_count_unique_not_found_outlets_on_specific_month(request,month):
#     dh= get_users_distribution_house(request)
#     month = int(month)
    
    

#     QUERY = GET_COUNT_UNIQUE_NOT_FOUND_OUTLETS_ON_SPECIFIC_MONTH.format(month,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_not_found_outlets" : row.total_not_found_outlets,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  
# # get_count_unique_open_outlet_specific_month
# @api_view()
# def get_count_unique_open_outlet_specific_month(request,month,year):
#     dh = get_users_distribution_house(request)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_OPEN_OUTLET_SPECIFIC_MONTH.format(month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_open_outlets" : row.OpenOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  

# # get_count_unique_not_found_outlet_specific_month
# @api_view()
# def get_count_unique_not_found_outlet_specific_month(request,month,year):
#     dh = get_users_distribution_house(request)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_NOT_FOUND_OUTLET_SPECIFIC_MONTH.format(month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_not_found_outlets" : row.ClosedOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  
# # get_count_unique_inactive_outlet_specific_month
# @api_view()
# def get_count_unique_inactive_outlet_specific_month(request,month,year):
    
#     dh = get_users_distribution_house(request)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_INACTIVE_OUTLET_SPECIFIC_MONTH.format(month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_inactive_outlet" : row.InactiveOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  
# #get_count_unique_closed_outlet_specific_month

# @api_view()
# def get_count_unique_closed_outlet_specific_month(request,month,year):
#     dh = get_users_distribution_house(request)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_CLOSED_OUTLET_SPECIFIC_MONTH.format(month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_closed_outlet" : row.ClosedOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })


# # get_count_unique_open_outlet_specific_date
# @api_view()
# def get_count_unique_open_outlet_specific_date(request,day,month,year):
    
#     dh= get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_OPEN_OUTLET_SPECIFIC_DATE.format(day,month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_open_outlets" : row.OpenOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  

# # get_count_unique_not_found_outlet_specific_date
# @api_view()
# def get_count_unique_not_found_outlet_specific_date(request,day,month,year):
    
#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_NOT_FOUND_OUTLET_SPECIFIC_DATE.format(day,month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_not_found_outlets" : row.ClosedOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  
# # get_count_unique_inactive_outlet_specific_date
# @api_view()
# def get_count_unique_inactive_outlet_specific_date(request,day,month,year):
    
#     dh = get_users_distribution_house(request)
#     day=int(day)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_INACTIVE_OUTLET_SPECIFIC_DATE.format(day,month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_inactive_outlet" : row.InactiveOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })
  
# #get_count_unique_closed_outlet_specific_date

# @api_view()
# def get_count_unique_closed_outlet_specific_date(request,day,month,year):
    
#     dh= get_users_distribution_house(request)
    
#     day = int(day)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_CLOSED_OUTLET_SPECIFIC_DATE.format(day,month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "total_closed_outlet" : row.ClosedOutlet,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     }) 


# #get_count_unique_pic_url_specific_date

# @api_view()
# def get_count_unique_pic_url_specific_date(request,day,month,year):
#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
    
    

#     QUERY = GET_COUNT_UNIQUE_PIC_URL_SPECIFIC_DATE.format(day,month,year,dh)

#     query_job = bigquery_client.query(QUERY)

#     data = [
#         {
#             "count" : row.Count,
    
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     })    
  

# total-unique-outlets
@api_view()
def total_unique_outlets(request):

    dh=get_users_distribution_house(request)

    QUERY = GET_TOTAL_UNIQUE_OUTLETS.format(dh)

    query_job = bigquery_client.query(QUERY)

    query_result = [ row.Total for row in  query_job.result() ]

    if (len(query_result) < 1):
        return Response({"error" : "Query error"}, status=500)

    return Response({
        'count' : query_result[0]
    })

#get_merchandiser_visited_in_specific_outlet
@api_view()
def get_merchandiser_visited_in_specific_outlet(request,wallet_number):
    
    dh = get_users_distribution_house(request)
    wallet_number= str(wallet_number)

    QUERY = GIT_MERCHANDISER_VISITED_IN_SPECIFIC_OUTLET.format(wallet_number,dh)
    #print(QUERY)

    query_job = bigquery_client.query(QUERY).result()

  
    data = get_data(query_job,re_name={"Reporter":"reporter","Distributor":"distributor_code","filepath3":"file_path3"})

    return Response({
        'data' : data
    }) 

#for test

@api_view()
def test_get_count_unique_outlet(request,agent_merchant):
    
    dh = get_users_distribution_house(request)
    today=datetime.datetime.now().__str__().split(" ")[0].split("-")
    pre_month=(datetime.datetime.now().replace(day=1)-datetime.timedelta(days=1)).__str__().split(" ")[0].split("-")

    day = int(today[2])
    month = int(today[1])
    year = int(today[0])
    month_2 = pre_month[1]
    year_2=pre_month[0]
    
    # outlet = str(outlet)
    
    v_outlet = ""
    date=""
    v_region= ""
    v_agent_merchant=""

    # #which outlet status
    # if outlet != "0":
        
        # if outlet == "open":
    v_outlet_1 = "  (not_found="+"\"0\""+" and is_inactive= false and closed= false)"
        # elif outlet == "closed":
    v_outlet_2 = "  closed = true "

        # elif outlet == "is_inactive":
    # v_outlet = " AND is_inactive = true "

        # elif outlet == "not_found":
    v_outlet_3 = "  not_found = " + "\"1\""
         
    

    # #day or month or year?
    
    # if day!=0 or month!=0 or year !=0:
    # date = " AND "
    # if day != 0:
    date_1= " day = {0} and month = {1}  and year = {2}".format(day,month,year)

    # if month != 0:
    date_2 = " month = {0}  and year = {1}".format(month,year)

    # if year != 0:
    date_3 = " month = {0}  and year = {1}".format(month_2,year_2)            
    
    # #which region?

    # if region != "0":
    #     v_region=" AND region = " + "\"" +str(region.upper().replace('_', ' ').replace('-', ' ')) + "\""
        
    
    #Agent or Merchant or All?

    if agent_merchant !="0":
        v_agent_merchant = " AND UPPER(agent_or_merchant) = " + "\""+ agent_merchant.upper().replace("_"," ") + "\" "

    #print(date)
    sub_query_1= v_outlet_1 + v_agent_merchant
    sub_query_2= v_outlet_2 + v_agent_merchant
    sub_query_3= v_outlet_3 + v_agent_merchant
    #print(sub_query)

    QUERY_1 = TEST_GET_COUNT_UNIQUE_OUTLET.format(sub_query_1,date_1,date_2,date_3,dh,"open")
    QUERY_2 = TEST_GET_COUNT_UNIQUE_OUTLET.format(sub_query_2,date_1,date_2,date_3,dh,"closed")
    QUERY_3 = TEST_GET_COUNT_UNIQUE_OUTLET.format(sub_query_3,date_1,date_2,date_3,dh,"not_found")
    # print(QUERY_1)
    # print(QUERY_2)
    # print(QUERY_3)
    QUERY = "("+QUERY_1+") union all ("+ QUERY_2 + ") union all (" + QUERY_3 + ")"
    print(QUERY)
    query_job_1 = bigquery_client.query(QUERY).to_dataframe().to_dict(orient="records")
    print(query_job_1)
    # query_job_2 = bigquery_client.query(QUERY_2).to_dataframe().to_dict(orient="records")
    # query_job_3 = bigquery_client.query(QUERY_3).to_dataframe().to_dict(orient="records")



    # if len(data)==0:
    #     return Response({
    #         "data" : "0"
    #     })

    return Response({
        'data' : {
            query_job_1[0]['type'] : query_job_1[0],
            query_job_1[1]['type'] : query_job_1[1],
            query_job_1[2]['type'] : query_job_1[2]
        }
    }) 

# Count active user
# @api_view()
# def test_get_count_active_unique_user(request, day, month, year):
#     day = int(day)
#     month = int(month)
#     year = int(year)
    


#     QUERY = COUNT_OF_ACTIVE_UNIQUE_USER.format(year, month, day,year,month,day)

#     #print(QUERY)
    
#     query_job = bigquery_client.query(QUERY)
    


#     query_result = [ row.ActiveMerchandisers for row in  query_job.result() ]

#     if (len(query_result) < 1):
#         return Response({"error" : "Query error"}, status=500)

#     return Response({
#         'count_merchandiser' : query_result[0],
#         'count_supervisor' : query_result[1]
#     })

@api_view()
def get_distinct_raw_data_for_merchandiser_csv(request, day, month, year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    
    date = str(day)+ "-" + str(month)+ "-" + str(year)
    date2 = str(day2)+ "-" + str(month2)+ "-" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000


    QUERY = GET_DISTINCT_RAW_DATA_FOR_MERCHANDISER_CSV.format(time_stamp1,time_stamp2,dh)
    print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()
 
    data = get_data(bigquery_result,re_name={})

    COLUMNS =[k for k in data[0].keys()]
   

   # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'raw_data_merchandiser_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")



    return response
    
    
    
@api_view()
def get_distinct_raw_data_for_supervisor_csv(request, day, month, year,day2,month2,year2):
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    
    date = str(day)+ "-" + str(month)+ "-" + str(year)
    date2 = str(day2)+ "-" + str(month2)+ "-" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    
    QUERY = GET_DISTINCT_RAW_DATA_FOR_SUPERVISOR_CSV.format(time_stamp1,time_stamp2, dh)
    print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()

    data = get_data(bigquery_result,re_name={'distributor_code': "Distribution House"})

    COLUMNS =[k for k in data[0].keys()]

    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'raw_data_supervisor_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")



    return response
    
    

# @api_view()
# def get_merchant_raw_dump_from_21dec_till_date_supervisor_csv(request, day, month, year,day2,month2,year2):
    
#     dh= get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     day2 = int(day2)
#     month2 = int(month2)
#     year2 = int(year2)
#     date = str(day)+ "_" + str(month)+ "_" + str(year)
#     date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)
#     time_stamp1 =int(datetime.datetime(year, month, day, 0,0,0).timestamp())*1000
#     time_stamp2 = int(datetime.datetime(year2, month2, day2, 23,59,59).timestamp())*1000

#     QUERY = GET_MERCHANT_RAW_DUMP_FROM_21DEC_TILL_DATE_SUPERVISOR_CSV.format(time_stamp1,time_stamp2,dh)

#     bigquery_result = bigquery_client.query(QUERY).result()


#     data = get_data(bigquery_result,re_name={'distributor_code': "Distribution House"})
    
#     COLUMNS =[k for k in data[0].keys()]

#     #print(COL)

    


#     # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
#     download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
#     file_name = f'merchant_raw_dump_from_{date}_till_{date2}_Supervisor_{download_time}.xlsx'
#     file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
#     response = HttpResponse(content_type="application/vnd.ms-excel")
#     response['Content-Disposition'] = f'attachment; filename={file_name}'
#     pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")



#     return response





# @api_view()
# def get_merchant_raw_dump_from_21dec_till_date_merchandiser_csv(request, day, month, year,day2,month2,year2):
    
#     dh = get_users_distribution_house(request)
#     day = int(day)
#     month = int(month)
#     year = int(year)
#     day2 = int(day2)
#     month2 = int(month2)
#     year2 = int(year2)
#     date = str(day)+ "_" + str(month)+ "_" + str(year)
#     date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)
#     time_stamp1 =int(datetime.datetime(year, month, day, 0,0,0).timestamp())*1000
#     time_stamp2 = int(datetime.datetime(year2, month2, day2, 23,59,59).timestamp())*1000

#     QUERY = GET_MERCHANT_RAW_DUMP_FROM_21DEC_TILL_DATE_MERCHANDISER_CSV.format(time_stamp1,time_stamp2,dh)

#     bigquery_result = bigquery_client.query(QUERY).result()
    

#     data = get_data(bigquery_result,re_name={'distributor_code': "Distribution House"})
    
#     COLUMNS =[k for k in data[0].keys()]

#     #print(COL)

#     download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
#     download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
#     file_name = f'merchant_raw_dump_from_{date}_till_{date2}_Merchandiser_{download_time}.xlsx'
#     file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
#     response = HttpResponse(content_type="application/vnd.ms-excel")
#     response['Content-Disposition'] = f'attachment; filename={file_name}'
#     pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")



#     return response




@api_view()
def get_raw_data_merchandiser_for_merchant_csv(request, day, month, year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)
    
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    
    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_RAW_DATA_MERCHANDISER_FOR_MERCHANT_CSV.format(time_stamp1,time_stamp2, dh)
    #print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()
    b2 = bigquery_client.query(QUERY).result()
    
    data = get_data(bigquery_result,re_name={'distributor_code': "Distribution House"})


    COLUMNS =[k for k in data[0].keys()]




   # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'raw_data_merchandiser_for_only_merchant_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")



    return response



@api_view()
def get_raw_data_supervisor_for_merchant_csv(request, day, month, year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    
    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_RAW_DATA_SUPERVISOR_FOR_MERCHANT_CSV.format(time_stamp1,time_stamp2,dh)
    print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()

    data = get_data(bigquery_result,re_name={'distributor_code': "Distribution House"})


    COLUMNS =[k for k in data[0].keys()]

    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'raw_data_supervisor_for_only_merchant_from_{date}_to_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")


   
    return response
 

#get_merchandiser_supervisor_call_start_time
# @api_view()
# def get_merchandiser_supervisor_call_start_time(request,day,month,year,day2,month2,year2):
    
#     dh = get_users_distribution_house(request)
#     day= int(day)
#     month = int(month)
#     year = int(year)
#     day2 = int(day2)
#     month2 = int(month2)
#     year2 = int(year2)

#     time_stamp1 = int(datetime.datetime(year, month, day, 0,0,0).timestamp())*1000
#     time_stamp2 =int(datetime.datetime(year2, month2, day2, 23,59,59).timestamp())*1000

#     #print(time_stamp1)
#     #print(time_stamp2)
    

    

#     QUERY = GET_MERCHANDISER_SUPERVISOR_CALL_START_TIME.format(time_stamp1,time_stamp2, dh)

#     query_job = bigquery_client.query(QUERY)

#     #distinct reporter,region, area, day, month, year, distributor_code

#     data = [
#         {
#             "region" : row.region,
#             "outlets_count" : row.outlets_count

#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     }) 


# #get_all_info_merchandiser_complain_specific_date
# @api_view()
# def get_all_info_merchandiser_complain_specific_date(request,day,month,year):

#     dh=get_users_distribution_house(request)
#     day= int(day)
#     month = int(month)
#     year = int(year)
    
#     time_stamp1 =int(datetime.datetime(year, month, day, 0,0,0).timestamp())*1000
#     time_stamp2 =int(datetime.datetime(year, month, day, 23,59,59).timestamp())*1000

#     QUERY = GET_ALL_INFO_MERCHANDISER_COMPLAIN_SPECIFIC_DATE.format(time_stamp1,time_stamp2,dh)

#     query_job = bigquery_client.query(QUERY).result()

#     #distinct reporter,region, area, day, month, year, distributor_code

#     # data = [
#     #     {
#     #         "reporter" : row.reporter,
#     #         "wallet_number" : row.wallet_number,
#     #         "complain": row.complain,
#     #         "date_time": ((datetime.datetime.fromtimestamp(row.timestamp/1000))).strftime("%Y/%m/%d %I:%M %p") 

#     #     } for row in query_job
#     # ]

#     data = get_data(query_job,re_name={})

#     return Response({
#         'data' : data
#     }) 


# #get_complain_of_a_specific_merchandiser
# @api_view()
# def get_complain_of_a_specific_merchandiser(request,reporter):

#     dh = get_users_distribution_house(request)

#     QUERY = GET_COMPLAIN_OF_A_SPECIFIC_MERCHANDISER.format(reporter,dh)

#     query_job = bigquery_client.query(QUERY).result()

#     #distinct reporter,region, area, day, month, year, distributor_code

#     # data = [
#     #     {
#     #         "complain": row.complain,
#     #         "date_time": ((datetime.datetime.fromtimestamp(row.timestamp/1000))).strftime("%Y/%m/%d %I:%M %p") 

#     #     } for row in query_job
#     # ]

#     data = get_data(query_job,re_name={"timestamp":"date_time"})

#     return Response({
#         'data' : data
#     }) 


@api_view()

def get_latest_outlet_info_filterwise(request,region, area,territory,house,user, agent_merchant,bmcc,outlet,whitelisting_number,day,month,year,day2,month2,year2,limit,offset):
    
    dh = get_users_distribution_house(request)
    limit = int(limit)
    sub_query=""
    v_region=""
    v_user=""
    v_agent_merchant=""
    v_bmcc=""
    v_outlet=""
    v_code = ""
    
    #date range
    day=int(day)
    month=int(month)
    year=int(year)
    day2=int(day2)
    month2=int(month2)
    year2=int(year2)
    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    v_date_range = " AND upload_time > " + str(time_stamp1) + " AND upload_time < " + str(time_stamp2) + " "

    #region->area->territory->house

    if region != "0":
        v_region = v_region + " AND a.region = " + "\"" +str(region.replace('_', ' ')) + "\""
    if area != "0":
        v_region = v_region + " AND a.area = " + "\"" +str(area.replace('_', ' ')) + "\""
    if territory != "0":
                
        v_region= v_region + " AND a.territory = " + "\"" + str(territory)+ "\""
    if house !="0":
        v_region= v_region + " AND  b.Distributor = " + "\"" +str(house.replace('_', ' ')) + "\""
    
    
    #Merchandiser or Supervisor or All?

    if user !="0":
        if user == "MERCHANDISER":
           v_user = " AND reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ("+ dh +")) " 
        elif user == "SUPERVISOR":
            v_user = " AND reporter in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ("+ dh +")) " 
    else:
        v_user = " AND reporter in (select whitelisting_number  from ((select whitelisting_number, distributor_code   from `biponon.merchandiser_basic_info`) union all (select whitelisting_number, distributor_code  from `biponon.supervisor_basic_info`)  ) where  distributor_code in (" + dh +") ) "
    


    #Agent or Merchant or All?

    if agent_merchant !="0":
        v_agent_merchant = " AND UPPER(agent_or_merchant) = " + "\""+ agent_merchant.upper().replace("_"," ") + "\""    
    
    #bmcc?

    if bmcc != "0":
        v_bmcc = " AND bmcc = " + "\""+bmcc+ "\""
    

    if outlet != "0":
        if outlet == "open":
            v_outlet = " AND (not_found="+"\"0\""+" and is_inactive= false and closed= false)"
        elif outlet == "closed":
            v_outlet = " AND closed = true "

        elif outlet == "is_inactive":
            v_outlet = " AND is_inactive = true "

        elif outlet == "not_found":
            v_outlet = " AND not_found = " + "\"1\""           
    
    if whitelisting_number != "0":
        v_code = " AND reporter = " + "\"" + whitelisting_number+ "\""


    sub_query= v_date_range + v_region +v_user+ v_agent_merchant + v_bmcc + v_outlet + v_code

    #print(sub_query)    

    QUERY = GET_LAST_N_OUTLET_INFO_FILTER.format(sub_query,limit,offset)

    #print(QUERY)

    bigquery_result = bigquery_client.query(QUERY).to_dataframe().rename(columns={"Distributor":"distributor_code","filepath3":"file_path3"})
    # .result()
    
    data = bigquery_result.to_dict(orient='records')
   

 

    # data = get_data(bigquery_result,re_name={"Distributor":"distributor_code","filepath3":"file_path3"})
    
    if len(data) == 0:
        return Response({
        "count" : len(data) ,    
        "data" : "no data"
    })

    QUERY2 = GET_COUNT_OF_DATA_FROM_UPDATE_OF_OUTLETS_DATE.format(sub_query)

    #print(QUERY2)

    bigquery_result2 = bigquery_client.query(QUERY2).result()
    #print([row.Count for row in bigquery_result2])
    for row in bigquery_result2:
        count = row.Count 

    if len(data) == 0:
        data = "no data"    
    return Response({
        "count" : count,
        "data" : data
    })


#get_whitelisting_number_by_distributor_code
# @api_view()
# def get_whitelisting_number_by_distributor_code(request,distributor_code):
    
#     dh = get_users_distribution_house(request)
#     QUERY = GET_WHITELISTING_NUMBER_BY_CODE.format(distributor_code,dh)

#     query_job = bigquery_client.query(QUERY)

#     #distinct reporter,region, area, day, month, year, distributor_code

#     data = [
#         {
#             "whitelisting_number" : row.number
#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     }) 


@api_view()
def get_date_wise_merchandiser_complain_csv(request, day, month, year,day2,month2,year2):
    
    dh=get_users_distribution_house(request)
    
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)


    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_DATE_WISE_MERCHANDISER_COMPLAIN_CSV.format(time_stamp1,time_stamp2,dh)

    bigquery_result = bigquery_client.query(QUERY).result()

    
    
    # data = [
    #     {
    #        "Wallet Number" : row.wallet_number,
    #        "Reporter" : row.reporter,
    #        "Complain" : row.complain,
    #        "day" : row.day,
    #        "month" : row.month,
    #        "year" : row.year
    #     }
    
    #     for row in bigquery_result
    # ]

    data = get_data(bigquery_result,re_name={"wallet_number": "Wallet Number", "reporter": "Reporter", "complain": "Complain"})

    #print(data[1].values())
    COLUMNS =[k for k in data[0].keys()]

    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    


    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'merchandiser_complain_from_date_{date}_to_date_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")


    

    return response


#get_merchandiser_wise_target_vs_achievement_specific_date
@api_view()
def get_merchandiser_wise_target_vs_achievement_specific_date(request,day,month,year,user):
    
    dh = get_users_distribution_house(request)
    
    day=int(day)
    month=int(month)
    year=int(year)
    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year, month, day, 23, 59,59).timestamp()*1000
    
    date_range = date_range_generator(day,month,year,day,month,year)
    #time = " timestamp >= {0} and timestamp <= {1}".format(time_stamp1,time_stamp2)
    
    if user == "MERCHANDISER":
        ALL_QUERY = GET_MERCHANDISER_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE.format(date_range,time_stamp1,time_stamp2, dh) 

    elif user == "SUPERVISOR":
        ALL_QUERY=GET_SUPERVISOR_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE.format(date_range,time_stamp1,time_stamp2 ,dh)
        

    else:
        QUERY = GET_MERCHANDISER_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE.format(date_range,time_stamp1,time_stamp2, dh)
        
        
        QUERY2=GET_SUPERVISOR_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE.format(date_range,time_stamp1,time_stamp2,dh)
        
        #print(QUERY)
        ALL_QUERY = "("+QUERY + ") union all (" + QUERY2 + ")"
    print(ALL_QUERY)
    query_job = bigquery_client.query(ALL_QUERY).result()



    data = get_data(query_job,re_name={"whitelisting_number":"Merchandiser_Number", "name": "Name","region":"Region","territory":"Territory","area":"Area","Distributor":"Distribution_House","Effective_CallTarget":"CallTarget"})

    return Response({
        'count': len(data),
        'data' : data,
        'test' : "HI SWAD 4444"
    }) 



@api_view()
def get_visited_outlets_date_range(request, whitelisting_number, day, month, year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)

    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    
    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = VISITED_OUTLETS_DATE_RANGE.format( whitelisting_number, time_stamp1,time_stamp2,dh)
    #print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).to_dataframe().rename(columns={"Distributor":"distributor_code","Reporter":"reporter","filepath3":"file_path3"})
    # .result()
    
    data = bigquery_result.to_dict(orient='records')
    # .result()



    # data = get_data(bigquery_result, re_name={"Distributor":"distributor_code","Reporter":"reporter","filepath3":"file_path3"})

    return Response({
        "count" : len(data),
        "data" : data
    })

#get_whitelisting_number_by_distributor_code
@api_view()
def get_whitelisting_number_by_user(request,user):
    
    dh = get_users_distribution_house(request)

    query_map = {
                    "0": GET_WHITELISTING_NUMBER_ALL.format(dh),
                    "MERCHANDISER": GET_WHITELISTING_NUMBER_USER.format("biponon.merchandiser_basic_info",dh) ,
                    "SUPERVISOR" : GET_WHITELISTING_NUMBER_USER.format("biponon.supervisor_basic_info",dh)

    }
    # if user == "0":
    QUERY = query_map[user]

    # elif user == "MERCHANDISER":
    #     v= "biponon.merchandiser_basic_info"
    #     QUERY= GET_WHITELISTING_NUMBER_USER.format(v,dh)  

    # elif user == "SUPERVISOR":
    #     v = "biponon.supervisor_basic_info"
    #     QUERY= GET_WHITELISTING_NUMBER_USER.format(v,dh)      

    bigquery_result = bigquery_client.query(QUERY).to_dataframe().rename(columns={"number": "whitelisting_number"})
    # .result()
    
    data = bigquery_result.to_dict(orient='records')

    #distinct reporter,region, area, day, month, year, distributor_code

    # data = [
    #     {
    #         "whitelisting_number" : row.number
    #     } for row in query_job
    # ]

    return Response({
        'data' : data
    })     


#get_merchandiser_wise_target_vs_achievement_specific_date
# @api_view()
# def get_dump_specific_outlet(request,number):
    
#     dh = get_users_distribution_house(request)
#     #print(str(number))
#     QUERY = GET_DUMP_SPECIFIC_OUTLETS.format(str(number),dh)

#     query_job = bigquery_client.query(QUERY).result()
#     #print(bigquery_client.query(QUERY).flatten_results)
#     col= [v.name for v in query_job.schema]
#     #print(col)

#     #distinct reporter,region, area, day, month, year, distributor_code

#     data = [
#         {
#           'region' : row.Region,

#         } for row in query_job
#     ]

#     return Response({
#         'data' : data
#     }) 


class FileView(APIView):

    parser_classes = (MultiPartParser,FormParser)

    def post(self,request,day,month,year,day2,month2,year2, *args, **kwargs):
        
        dh = get_users_distribution_house(request)

        file_serializer= FileSerializer(data=request.data)

        if file_serializer.is_valid():
           file_serializer.save()
           day= int(day)
           month = int(month)
           year = int(year)
           day2 = int(day2)
           month2 = int(month2)
           year2 = int(year2)

           date = str(day)+ "_" + str(month)+ "_" + str(year)
           date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

           time_stamp1 = int(datetime.datetime(year, month, day, 0,0,0).timestamp())*1000
           time_stamp2 =int(datetime.datetime(year2, month2, day2, 23,59,59).timestamp())*1000
           file_data= file_serializer.data['file']
           #print(file_data)
           file_path = f'{settings.BASE_DIR}/{file_data}'
           dataset=pd.read_excel(file_path, dtype=str)

           df = pd.DataFrame(dataset)
           col=df.columns
           #print(col)


           dff=df['Wallet No']
           r=dff.shape[0]
           if r >= 20000:

              return Response({"data": "Excceed 20K data"})
           pp=[x for x in dff]
           c="("
           for p in pp:
                if len(p) == 10:
                    p= "+880" + p
                elif len(p) == 11:
                    p="+88" + p    
                c=c+ "\"" + p + "\"" +","
           c=c+"end"   
           c=c.replace(",end",")")     
           #print(c)

           col=df.columns
           #print(col)
           QUERY = GET_DUMP_SPECIFIC_OUTLETS.format(time_stamp1,time_stamp2,c,dh)
           print(QUERY)
           query_job = bigquery_client.query(QUERY).result()


           
           data = get_data(query_job, re_name={})
           
           print(data)

           
           COLUMNS =[k for k in data[0].keys()]

        # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
            
           download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
           file_name = f'raw_data_for_specific_outlets_from_{date}_to_{date2}_download_at_{download_time}.csv'
           file_path2 = f'{settings.BASE_DIR}/dumps/{file_name}'
        #    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(file_path2, index=False, encoding="utf-16")
           response = HttpResponse(content_type="text/csv")
           response['Content-Disposition'] = f'attachment; filename={file_name}'
           pd.DataFrame(data)[COLUMNS].astype(str).to_csv(response, index=False)


           #return Response(file_csv, status=status.HTTP_201_CREATED)
           
           os.remove(file_path)
           #print("File Removed!")

           return response

        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)  


class FileViewLatestCall(APIView):

    parser_classes = (MultiPartParser,FormParser)

    def post(self,request,region, area,territory,house,user, agent_merchant,bmcc,outlet,whitelisting_number,day,month,year,day2,month2,year2,limit,offset,*args, **kwargs):
        

        file_serializer= FileSerializer(data=request.data)

        if file_serializer.is_valid():
           file_serializer.save()
           file_data= file_serializer.data['file']

           #print(file_data)
           #return Response(file_serializer.data, status= status.HTTP_201_CREATED )
           file_path = f'{settings.BASE_DIR}/{file_data}'
           dataset=pd.read_excel(file_path, dtype=str)

           df = pd.DataFrame(dataset)
           col=df.columns
           


           dff=df['Wallet No']
           pp=[x for x in dff]
           c="("
           for p in pp:
                if len(p) == 10:
                    p= "+880" + p
                elif len(p) == 11:
                    p="+88" + p    
                c=c+ "\"" + p + "\"" +","
           c=c+"end"   
           c=c.replace(",end",")")     
           #print(c)

           col=df.columns
           #print(col)           


           limit = int(limit)
           sub_query=""
           v_region=""
           v_user=""
           v_agent_merchant=""
           v_bmcc=""
           v_outlet=""
           v_code = ""
            
            #date range
           day=int(day)
           month=int(month)
           year=int(year)
           day2=int(day2)
           month2=int(month2)
           year2=int(year2)
           time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
           time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

           v_date_range = " AND upload_time > " + str(time_stamp1) + " AND upload_time < " + str(time_stamp2) + " "

           #region->area->territory->house

           if region != "0":
                v_region = " AND region = " + "\"" +str(region.replace('_', ' ')) + "\""
                if area != "0":
                    v_region = v_region + " AND area = " + "\"" +str(area.replace('_', ' ')) + "\""
                    if territory != "0":
                        
                        v_region= v_region + " AND territory = " + "\"" + str(territory)+ "\""
                        if house !="0":
                            v_region= v_region + " AND  distributor_code = " + "\"" +house + "\""
    
    
            #Merchandiser or Supervisor or All?

           if user !="0":
              if user == "MERCHANDISER":
                 v_user = "AND reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info`) " 
              elif user == "SUPERVISOR":
                 v_user = " AND reporter in (select whitelisting_number  from `biponon.supervisor_basic_info`)  "


            #Agent or Merchant or All?

           if agent_merchant !="0":
               v_agent_merchant = " AND UPPER(agent_or_merchant) = " + "\""+ agent_merchant.upper().replace("_"," ") + "\""    
    
            #bmcc?

           if bmcc != "0":
              v_bmcc = " AND bmcc = " + "\""+bmcc+ "\""
        

           if outlet != "0":
                if outlet == "open":
                    v_outlet = " AND (not_found="+"\"0\""+" and is_inactive= false and closed= false)"
                elif outlet == "closed":
                    v_outlet = " AND closed = true "

                elif outlet == "is_inactive":
                    v_outlet = " AND is_inactive = true "

                elif outlet == "not_found":
                    v_outlet = " AND not_found = " + "\"1\""           
    
           if whitelisting_number != "0":
                v_code = " AND reporter = " + "\"" + whitelisting_number+ "\""


           

           #print(sub_query)

           #print(col)
           sub_query= v_date_range + v_region +v_user+ v_agent_merchant + v_bmcc + v_outlet + v_code + " AND wallet_number in " + c

           QUERY = GET_LAST_N_OUTLET_INFO_FILTER.format(sub_query,limit,offset)

           #print(QUERY)

           bigquery_result = bigquery_client.query(QUERY).result()
           
           data = get_data(bigquery_result,re_name={"Distributor":"distributor_code"})

           COLUMNS =[k for k in data[0].keys()]
           QUERY2 = GET_COUNT_OF_DATA_FROM_UPDATE_OF_OUTLETS_DATE.format(sub_query)

           print(QUERY2)

           bigquery_result2 = bigquery_client.query(QUERY2).result()
           #print([row.Count for row in bigquery_result2])
           for row in bigquery_result2:
               count = row.Count

            
           os.remove(file_path)

           return Response({
                    "count" : count,
                    "data" : data
                    }, status= status.HTTP_201_CREATED)
        
          

        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)  


# @api_view()
# def get_user_type(request):
    
#     data= ["am","tm","rm","dhm"]

#     return Response({
#         'user_type' : data[randint(0,3)]})


@api_view()
def get_date_wise_merchandiser_maintenance_csv(request, day, month, year,day2,month2,year2):
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)


    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_DATE_WISE_MERCHANDISER_MAINTENANCE_CSV.format(time_stamp1,time_stamp2,dh)

    bigquery_result = bigquery_client.query(QUERY).result()

    
    
    # data = [
    #     {
    #        "Wallet Number" : row.wallet_number,
    #        "Reporter" : row.reporter,
    #        "Maintenance task" : row.task,
    #        "day" : row.day,
    #        "month" : row.month,
    #        "year" : row.year
    #     }
    
    #     for row in bigquery_result
    # ]

    data = get_data(bigquery_result,re_name={"wallet_number": "Wallet Number", "reporter": "Reporter", "task": "Maintenance task"})

    #print(data[1].values())
    COLUMNS =[k for k in data[0].keys()]

    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    


    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'merchandiser_maintenance_task_from_date_{date}_to_date_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")


    

    return response


@api_view()
def get_date_wise_merchandiser_new_task_csv(request, day, month, year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)


    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_DATE_WISE_MERCHANDISER_NEW_TASK_CSV.format(time_stamp1,time_stamp2,dh)

    bigquery_result = bigquery_client.query(QUERY).result()

    
    
    # data = [
    #     {
    #        "Wallet Number" : row.wallet_number,
    #        "Reporter" : row.reporter,
    #        "New task" : row.task,
    #        "day" : row.day,
    #        "month" : row.month,
    #        "year" : row.year
    #     }
    
    #     for row in bigquery_result
    # ]

    data = get_data(bigquery_result,re_name={"wallet_number": "Waller Number", "reporter": "Reporter", "task": "New task"})

    #print(data[1].values())
    COLUMNS =[k for k in data[0].keys()]

    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    


    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'merchandiser_new_task_from_date_{date}_to_date_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")


    

    return response    

@api_view()
def get_selfie_attendance_merchandiser_csv(request, day, month, year,day2,month2,year2):
    dh= get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)
    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_SELFIE_ATTENDANCE_MERCHANDISER_CSV.format(time_stamp1,time_stamp2,dh)

    bigquery_result = bigquery_client.query(QUERY).result()


    data = get_data(bigquery_result, re_name={})
    
    COLUMNS =[k for k in data[0].keys()]

    #print(COL)

    


    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'Selfie_Attendance_Merchandiser_from_{date}_till_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")


    return response


@api_view()
def get_selfie_attendance_supervisor_csv(request, day, month, year,day2,month2,year2):
    dh= get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    date = str(day)+ "_" + str(month)+ "_" + str(year)
    date2 = str(day2)+ "_" + str(month2)+ "_" + str(year2)
    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000

    QUERY = GET_SELFIE_ATTENDANCE_SUPERVISOR_CSV.format(time_stamp1,time_stamp2,dh)

    bigquery_result = bigquery_client.query(QUERY).result()


    data = get_data(bigquery_result, re_name={})
    
    COLUMNS =[k for k in data[0].keys()]

    #print(COL)

    


    # download_time = ( datetime.datetime.now() + datetime.timedelta(hours=6) ).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    
    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    file_name = f'Selfie_Attendance_Supervisor_from_{date}_till_{date2}_download_at_{download_time}.xlsx'
    file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    pd.DataFrame(data)[COLUMNS].astype(str).to_excel(response, index=False, encoding="utf-16")



    return response

@api_view()
def get_selfie_attendance_on_specific_date(request, day, month, year,day2,month2,year2,user):
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    user = str(user)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000



    query_map={"MERCHANDISER": GET_SELFIE_ATTENDANCE_ON_SPECIFIC_DATE_MERCHANDISER,
                "SUPERVISOR": GET_SELFIE_ATTENDANCE_ON_SPECIFIC_DATE_SUPERVISOR
                }
    
    QUERY = query_map[user].format(time_stamp1,time_stamp2,dh)
    print(QUERY) 

    bigquery_result = bigquery_client.query(QUERY).to_dataframe()
    # .result()
    
    data = bigquery_result.to_dict(orient='records')
    
    # # print(t[0])
    # # print(datetime.datetime.fromtimestamp((t[0]/1e3)))
    # # print(datetime.datetime.fromtimestamp(t[0]/1e3).strftime("%Y/%m/%d %H:%M"))
    
    # data = [
    #     {
    #         "name" : row.name,
    #         "whitelisting_number" : row.whitelisting_number,
    #         "region" : row.region,
    #         "area" : row.area,
    #         "territory" : row.territory,
    #         "distributor_code" : get_distributor(row.distributor_code),
    #         "selfieAttendance" : row.selfieAttendance,
    #         "time" : datetime.datetime.fromtimestamp(row.timestamp/1000).strftime("%d/%m/%Y %H:%M"),
    #         "day" : row.day,
    #         "month" : row.month,
    #         "year" : row.year

          

    #     }
    
    #     for row in bigquery_result
    # ]

    # data = get_data(bigquery_result,re_name={})


    return Response(
                  {"count" : len(data),
                  "data"  : data}

    )


@api_view()
def get_user_with_no_selfie_attendance(request, day, month, year,day2,month2,year2,user):
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    user = str(user)
    #print(date_range_generator(day,month,year,day2,month2,year2))
    
    

    date_range = []
    if (month2-month !=0):
        if(month  in [9,4,6,11]):
                    n_day2=30
        elif month == 2:
                    n_day2=28
        else:
                    n_day2=31

        for d in range(day,n_day2+1):
                #print(d)

                date_range.append(date_range_generator(d, month,year,d,month,year2))
                #print(date_range)
        for m in range(month-1,month2):
            #print("diff"+str(month2-month))
            if (month2-month !=0):
            
                if(m  in [9,4,6,11]):
                    n_day2=30
                elif m == 2:
                    n_day2=28
                else:
                    n_day2=31
            else:
                n_day2=day2

            #print(m)
            for d in range(day,n_day2+1):
                #print(d)

                date_range.append(date_range_generator(d, m,year,d,m,year2))
                #print(date_range)
        #if(month2-month!=0):        
        for d in range(1,day2+1):
                    #print(d)

            date_range.append(date_range_generator(d, month2,year,d,month2,year2))

    else:
        for d in range(day,day2+1):
                    #print(d)

            date_range.append(date_range_generator(d, month2,year,d,month2,year2))


    #print(date_range[1:-1].split(" or "))
    #list_date_range = date_range[1:-1].split(" or ")
    list_date_range = date_range
    #print(list_date_range)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    

    query_map={"MERCHANDISER": GET_USER_WITH_NO_SELFIE_ATTENDANCE_MERCHANDISER,
                "SUPERVISOR": GET_USER_WITH_NO_SELFIE_ATTENDANCE_SUPERVISOR
                }
    
    # if user == "MERCHANDISER":
    QUERY=query_map[user].format(list_date_range[0],dh)
    list_date_range2 = list_date_range[1:]
    for l in list_date_range2:
        print(l)
        Q = query_map[user].format(l,dh)
        QUERY= "("+QUERY +")\nunion all\n(" + Q+")"
        #QUERY=QUERY + "\norder by timestamp desc "
    print(QUERY)
        


    # if user == "SUPERVISOR":
    #     QUERY=GET_USER_WITH_NO_SELFIE_ATTENDANCE_SUPERVISOR.format(list_date_range[0],dh)
    #     list_date_range2 = list_date_range[1:]
    #     for l in list_date_range2:
    #         #print(l)
    #         Q = GET_USER_WITH_NO_SELFIE_ATTENDANCE_SUPERVISOR.format(l,dh)
    #         QUERY= "("+QUERY +")\nunion all\n(" + Q+")"
    #     #QUERY=QUERY + "\norder by timestamp desc "
    #     print(QUERY)


    #print(QUERY)    

    bigquery_result = bigquery_client.query(QUERY).to_dataframe().rename(columns={"distributor": "distributor_code"})
    # .result()
    
    data = bigquery_result.to_dict(orient='records')
    # .result()
    
   
    
    # data = get_data(bigquery_result, re_name={"distributor": "distributor_code"})

    return Response(
                  {"count" : len(data),
                  "data"  : data}

    )
    # return Response(
    #               {"count" : 0,
    #               "data"  : 0}

    # )

@api_view()
def get_selfie_count(request, day, month, year,day2,month2,year2):
    
    dh= get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    
   
    QUERY1=GET_COUNT_SELFIE_MERCHANDISER.format(time_stamp1,time_stamp2,dh)
    #print(QUERY1)
    QUERY2 = GET_TOTAL_MERCHANDISER.format(dh)
    


    QUERY3=GET_COUNT_SELFIE_SUPERVISOR.format(time_stamp1,time_stamp2,dh)
    QUERY4 = GET_TOTAL_SUPERVISOR.format(dh)
       

    bigquery_result1 = bigquery_client.query(QUERY1).result()
    bigquery_result2 = bigquery_client.query(QUERY2).result()
    bigquery_result3 = bigquery_client.query(QUERY3).result()
    bigquery_result4 = bigquery_client.query(QUERY4).result()
    
    # # print(t[0])
    # # print(datetime.datetime.fromtimestamp((t[0]/1e3)))
    # # print(datetime.datetime.fromtimestamp(t[0]/1e3).strftime("%Y/%m/%d %H:%M"))
    #imei	whitelisting_number	name	region	area	territory	distributor_code	type
    data1 = [ row.count_merchandiser_selfie for row in bigquery_result1]

    data2 = [ row.total for row in bigquery_result2]  
    

    data3 = [ row.count_supervisor_selfie for row in bigquery_result3]

    data4 = [ row.total for row in bigquery_result4]

   


    return Response(
                  {"count_merchandiser_selfie" : data1[0],
                  "count_merchandiser_leave" : data2[0] - data1[0],
                  "count_supervisor_selfie" : data3[0],
                  "count_supervisor_leave" : data4[0] - data3[0]}

    )


@api_view()
def get_user_short_info(request, whitelisting_number,day, month, year):
    
    dh = get_users_distribution_house(request)
    whitelisting_number = str(whitelisting_number)
    day = int(day)
    month = int(month)
    year = int(year)
   
    QUERY1=GET_USER_SHORT_INFO.format(whitelisting_number,day,month,year,dh)
    #print(QUERY1)

    bigquery_result1 = bigquery_client.query(QUERY1).to_dataframe().rename(columns={"Target":"target","Achievement":"achievement"})
    # .result()
    
    data = bigquery_result1.to_dict(orient='records')
   

    # get_data(bigquery_result1,re_name={"Target":"target","Achievement":"achievement"})

    if len(data) == 0:
        return Response({
        "data" : "no data"
    })

    

    # date_time =.apply( increase_version_number ) datetime.datetime.fromtimestamp(int(data["timestamp"])/1000).strftime("%d/%m/%Y %I:%M %p").split(" ")
    # data["Date"]=date_time[0]
    # data["Time"]=date_time[1] + " "+date_time[2]

    return Response(
                  {"data" : data,
}

    )


# @api_view()
# def get_filter_user(request, query):
#     print(query)
#     dh = get_users_distribution_house(request)
#     #region, area, territory, distributor_code, wallet_number
#     QUERY = GET_FILTER_OUTLET.format(query,dh)
#     print(QUERY)
#     bigquery_result = bigquery_client.query(QUERY).result()

#     data = get_data(bigquery_result)

#     return Response(
#                   {"data" : data,
# }

#     ) 

# @api_view()
# def get_user_lat_lng(request, whitelisting_number,day,month,year):
    
#     dh = get_users_distribution_house(request)

#     QUERY = GET_LAT_LNG.format(whitelisting_number,day,month,year,dh)
#     #print(QUERY)
#     bigquery_result = bigquery_client.query(QUERY).result()

#     data = get_data(bigquery_result)

#     return Response(
#                   {"data" : data,
# }

#     )       

@api_view()
def get_outlet_lat_lng(request, whitelisting_number,day,month,year):
    
    dh = get_users_distribution_house(request)
    day = int(day)
    month = int(month)
    year = int(year)

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    
    #print(time_stamp1)
    st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d').replace("-"," ").split()

    time_stamp2=datetime.datetime(int(st[0]),int(st[1]),int(st[2]), 0, 0,0).timestamp()*1000
    #print(time_stamp2)
    

    # time_stamp2=datetime.datetime(year2, month2, day2, 23, 59,59).timestamp()*1000
    

    if time_stamp1 <= time_stamp2:
        QUERY = GET_OUTLET_LAT_LNG.format(whitelisting_number,day,month,year,dh)
    else:
        QUERY = GET_OUTLET_LAT_LNG_FUTURE.format(whitelisting_number,day,month,year,dh)

    #print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()

    

    data = get_data(bigquery_result,re_name={}) 

    if len(data) == 0:
        return Response(
                  {"data" : "no data",
}

    )


    columns_name = [v.name for v in bigquery_result.schema]
    
    df = pd.DataFrame(data,columns=columns_name)
    wallet_number= df["wallet_number"].unique()
    #print(df.groupby('wallet_number').groups)
    grouped= df.groupby('wallet_number').groups
    
    data2 = {}
    data2["whitelisting_number"] = whitelisting_number

    data3 = []
    for group in wallet_number:
        #print(df.loc[df["wallet_number"]==group])
        
        df2=df.loc[df["wallet_number"]==group]
        info2= [{
            "lat" : row.lat,
            "lng" : row.lng,
            "revalidate_lat" : row.revalidate_lat,
            "revalidate_lng" : row.revalidate_lng,
            "day" : row.day,
            "month" : row.month,
            "year" : row.year,
    
        }for index, row in df2.iterrows()]
        
        #data3["wallet_number"] = group
        #data3["list"] = info2

        data3.append( {"wallet_number" : group, "list" : info2} )
    
    data2["outlets"] = data3


    #print(data2)    

    return Response(
                  {"data" : data2,
}

    )     

@api_view()
def get_total_user(request, user):
    
    dh = get_users_distribution_house(request)
    #region, area, territory, distributor_code, wallet_number
    if user == "MERCHANDISER":
        QUERY = GET_TOTAL_MERCHANDISER.format(dh)
    elif user == "SUPERVISOR":
        QUERY = GET_TOTAL_SUPERVISOR.format(dh)
        

    #print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()

    data = [ row.total for row in bigquery_result
    ]

    return Response(
                  {"data" : data[0],
})


def sum_total(list_data):
    total = 0

    for i in list_data:
        total += i

    return total
###
@api_view()
def get_count_region_wise(request,user):
    
    QUERY = GET_COUNT_REGION_WISE.format(user.lower())
        

    #print(QUERY)
    bigquery_result = bigquery_client.query(QUERY).result()
    data = get_data(bigquery_result,re_name={})
    v=pd.DataFrame(data)
    v = v.astype({"call_count": int})
    v_list=v.region.unique()
    #print(v_list[1:])    
    v=pd.pivot_table(v,index="date",columns="region",values="call_count",fill_value=0)
    v.reset_index(inplace=True)
    #print( list(v) )
    #v['TOTAL'] = 0
    #for i in list(v):
    #    v['TOTAL'] = v['TOTAL'] + v[i]
    # v['TOTAL'] = v[v_list].sum()
    #print(v.head())
    v['total']= v[v_list[0:]].sum(1)
    v.sort_values(by='date', ascending=False)
    #print(v.head())
    #print(v.head)

    return Response(
                  {   "count": len(v),
                      "data" : v.to_dict(orient="records")
}

    )


"""
#test comments
@api_view()
def test_api_link(request):
    return Response( {"data" : "Success test 2"} )
"""


import uuid
from pathlib import Path
from tqdm import tqdm

def clean_phone_number(phone_number):
    clean_phone_number = "+880" + phone_number[-10:]
    return clean_phone_number

def increase_version_number( version ):
    return str( int(version) + 1 )



class RoutePlanUploader(APIView):

    parser_classes = (MultiPartParser,FormParser,JSONParser)

    def post(self,request,day,month,year, *args, **kwargs):
        

        datadict = dict( (request.data) )
        file_path = None
        if 'filetoken' in datadict:
            if datadict['filetoken']:
                filetoken = datadict['filetoken'][0] 
                if Path(f'{settings.BASE_DIR}/media/{filetoken}.csv').is_file():
                    file_path = f'{settings.BASE_DIR}/media/{filetoken}.csv'
                else: 
                    file_path = None
                    # return Response({ "error" : "please provide valid file-token",
                    #                   "filetoken": filetoken   })
        if file_path:

            new_route_plan = pd.read_csv(file_path, dtype=str)
            
            
            QUERY = GET_MERCHANDISERS_ROUTE_PLAN.format( str(day) , str(month) , str(year) )
            existing_route_plan_query_job = bigquery_client.query( QUERY )
            existing_route_plan_df = existing_route_plan_query_job.to_dataframe()
            existing_route_plan_df = existing_route_plan_df[ ['whitelisting_number' , 'active_status'] ]
            existing_route_plan_df.active_status = existing_route_plan_df.active_status.apply( increase_version_number )
            

            new_route_plan = new_route_plan.merge( existing_route_plan_df , on='whitelisting_number' , how='left')
            new_route_plan.active_status = new_route_plan.active_status.fillna( '1' )

            print( new_route_plan.head() )
            os.remove(file_path)
            return Response({ "data" : new_route_plan.to_dict(orient='records') })

            cols = [s.name for s in MERCHANDISER_SCHEMA ]
            df = new_route_plan[cols[:-1]]

            BATCH_SIZE = 500
            TOTAL_LENGTH = df.shape[0]

            for i in tqdm(range(0, TOTAL_LENGTH, BATCH_SIZE)):
                _df = df[ i  : min( i + BATCH_SIZE, TOTAL_LENGTH ) ]
                rows_to_insert = []
                for j in range(_df.shape[0]):
                    rows_to_insert.append(
                        tuple(_df.iloc[j].get_values().tolist() + [ int(time.time() * 1000 ) ])
                    )
                errors = bigquery_client.insert_rows(TEST_MERCHANDISER_TABLE, rows_to_insert)
                assert errors == []


            return Response({ "data" : new_route_plan })


        else:
            file_serializer= FileSerializer(data=request.data)


            
            if file_serializer.is_valid():
                file_serializer.save()

                day= int(day)
                month = int(month)
                year = int(year)
                date = str(day)+ "_" + str(month)+ "_" + str(year)           
                time_stamp = int(datetime.datetime(year, month, day, 0,0,0).timestamp())*1000

                file_data= file_serializer.data['file']
                #print(file_data)
                file_path = f'{settings.BASE_DIR}/{file_data}'

                dataset = pd.read_excel(file_path, dtype=str)
                dataset = dataset.drop_duplicates(subset='wallet_no', keep='first')
                dataset.wallet_no = dataset.wallet_no.apply(clean_phone_number)
                dataset.whitelisting_number = dataset.whitelisting_number.apply(clean_phone_number)
                

                QUERY = GET_MERCHANDISERS_BASIC_INFO
                basic_info_query_job = bigquery_client.query( QUERY )
                basic_info_df = basic_info_query_job.to_dataframe()

                

                new_route_plan = dataset.merge( basic_info_df , on='whitelisting_number' , how='left')
                new_route_plan['year'] = year
                new_route_plan['month'] = month
                new_route_plan['day'] = day
                #new_route_plan.wallet_no = new_route_plan.wallet_no.fillna( 'NO OUTLET' )
                new_route_plan = new_route_plan.astype( 'str' )
                
                
                print(new_route_plan.shape)
                print(new_route_plan.whitelisting_number.unique().shape)
                
                outletcount = ( new_route_plan['whitelisting_number'].value_counts() ).reset_index()
                outletcount.rename(columns={'index':'whitelisting_number', 'whitelisting_number':'outlet_count'}, inplace=True)
                outletcount = outletcount.merge( basic_info_df , on='whitelisting_number' , how='left')
                print( outletcount )

                
                filename = uuid.uuid4().hex
                new_file_path = f'{settings.BASE_DIR}/media/{filename}.csv'
                new_route_plan.to_csv( new_file_path , index=False )
                os.remove(file_path)

                return Response({ 
                    "filetoken" : filename,
                    "outlet_count_by_whitelisting_number" : outletcount.to_dict(orient='records') 
                    
                })

            


            else:
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)  



    def get(self,request,day,month,year, *args, **kwargs):

        datadict = dict( (request.data) )
        file_path = None
        if 'filetoken' in datadict:
            if datadict['filetoken']:
                filetoken = datadict['filetoken'][0] 
                if Path(f'{settings.BASE_DIR}/media/{filetoken}.csv').is_file():
                    file_path = f'{settings.BASE_DIR}/media/{filetoken}.csv'
                else: 
                    file_path = None
                    return Response({ "error" : "please provide valid file-token",
                                      "filetoken": filetoken   })

        
        if file_path:

            new_route_plan = pd.read_csv(file_path, dtype=str)
            
            
            QUERY = GET_MERCHANDISERS_ROUTE_PLAN.format( str(day) , str(month) , str(year) )
            existing_route_plan_query_job = bigquery_client.query( QUERY )
            existing_route_plan_df = existing_route_plan_query_job.to_dataframe()
            existing_route_plan_df = existing_route_plan_df[ ['whitelisting_number' , 'active_status'] ]
            existing_route_plan_df.active_status = existing_route_plan_df.active_status.apply( increase_version_number )
            

            new_route_plan = new_route_plan.merge( existing_route_plan_df , on='whitelisting_number' , how='left')
            new_route_plan.active_status = new_route_plan.active_status.fillna( '1' )

            print( new_route_plan.head() )
            os.remove(file_path)
            return Response({ "data" : new_route_plan.to_dict(orient='records') })

            cols = [s.name for s in MERCHANDISER_SCHEMA ]
            df = new_route_plan[cols[:-1]]

            BATCH_SIZE = 500
            TOTAL_LENGTH = df.shape[0]

            for i in tqdm(range(0, TOTAL_LENGTH, BATCH_SIZE)):
                _df = df[ i  : min( i + BATCH_SIZE, TOTAL_LENGTH ) ]
                rows_to_insert = []
                for j in range(_df.shape[0]):
                    rows_to_insert.append(
                        tuple(_df.iloc[j].get_values().tolist() + [ int(time.time() * 1000 ) ])
                    )
                errors = bigquery_client.insert_rows(TEST_MERCHANDISER_TABLE, rows_to_insert)
                assert errors == []


            return Response({ "data" : new_route_plan })
        
        else:
            return Response({ "error" : "please provide valid file-token" })
        
    def delete(self,request,day,month,year, *args, **kwargs):

        datadict = dict( (request.data) )
        file_path = None
        if 'filetoken' in datadict:
            if datadict['filetoken']:
                filetoken = datadict['filetoken'][0] 
                if Path(f'{settings.BASE_DIR}/media/{filetoken}.csv').is_file():
                    file_path = f'{settings.BASE_DIR}/media/{filetoken}.csv'
                else: 
                    file_path = None
        
        if file_path:
            os.remove(file_path)
            return Response({ "results" : "success" })
        else:
            return Response({ "error" : "please provide valid file-token" })




from google.cloud import vision

class AnnotateImage(APIView):
    
    parser_classes = ( FormParser, JSONParser)
    
    def post( self, request , *args, **kwargs):

        datadict = dict( (request.data) )
        image_url = None
        if 'image_url' in datadict:
            if datadict['image_url']:
                image_url = datadict['image_url'][0]
        
        if image_url:
            try:
                client = vision.ImageAnnotatorClient()
                image = vision.types.Image()
                image.source.image_uri = image_url

                logos_response = client.logo_detection(image=image)
                logos = logos_response.logo_annotations

                texts_response = client.text_detection(image=image , image_context={ 'language_hints': [ 'bn']} )
                texts = texts_response.text_annotations
                
                text_dict = []
                for text in texts:
                    vertices = ( [{ 'y' : vertex.y, 'x' : vertex.x } for vertex in text.bounding_poly.vertices])
                    text_dict.append( { 'text' : (text.description).encode('utf8') , 'vertices' : vertices } )
                
                logo_dict = []
                for logo in logos:
                    vertices = ( [{ 'y' : vertex.y, 'x' : vertex.x } for vertex in logo.bounding_poly.vertices])
                    logo_dict.append( { 'logo' : (logo.description).encode('utf8') , 'vertices' : vertices } )

                
                data = { 'text_annotation' : text_dict , 'logo_annotation' : logo_dict}
                return Response( 
                    {"data" : data} 
                )
            
            except:
                return Response( 
                    {"data" : 'something happened'} 
                )
        
        else:
            return Response({ "error" : "please provide valid image_url" })





class PredictRetailLogo(APIView):
    
    parser_classes = ( FormParser, JSONParser)
    
    def post( self, request , *args, **kwargs):

        datadict = dict( (request.data) )
        image_url = None
        if 'image_url' in datadict:
            if datadict['image_url']:
                image_url = datadict['image_url'][0]
        
        if image_url:
            server_url = "http://35.225.210.84:8501/v1/models/retail_logo_detection:predict"
            image = get_image(image_url)
            formatted_json_input = pre_process(image)

            # Call tensorflow server
            headers = {"content-type": "application/json"}
            server_response = requests.post(
                server_url, data=formatted_json_input, headers=headers)
            
            print( "response received" )

            # Post process output
            im_width, im_height = image.size
            image_np = load_image_into_numpy_array(image)
            output_dict = post_process(server_response, image_np.shape)

            data = get_boxes_and_labels(
                output_dict['detection_boxes'],
                output_dict['detection_classes'],
                output_dict['detection_scores'],
                im_width,
                im_height
            )

            return Response( 
                {"data" : data} 
            )
        
        else:
            return Response({ "error" : "please provide valid image_url" })




 
@api_view()
def get_count_all_dashboard(request, user):
    
    dh = get_users_distribution_house(request)
    #region, area, territory, distributor_code, wallet_number
    user = user.lower()

    today=datetime.datetime.today().__str__().split(" ")[0].split("-")
    day = int(today[2])
    month = int(today[1])
    year = int(today[0])
    yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)
    

    time_stamp1=datetime.datetime(year, month, day, 0, 0,0).timestamp()*1000
    time_stamp2=datetime.datetime(year, month, day, 23, 59,59).timestamp()*1000

    time_stamp3=datetime.datetime(yesterday_date.year, yesterday_date.month, yesterday_date.day, 0, 0,0).timestamp()*1000
    time_stamp4=datetime.datetime(yesterday_date.year, yesterday_date.month, yesterday_date.day, 23, 59,59).timestamp()*1000
    

    QUERY = GET_COUNT_ALL_DASHBOARD.format(user,time_stamp1,time_stamp2,time_stamp3, time_stamp4,dh)

        

    #print(QUERY)
    bigquery_result = bigquery_client.query(QUERY)

    data = bigquery_result.to_dataframe()
    print(data)

    return Response(
                  {"data" : data.to_dict(orient='records')[0],
}

    )

 
@api_view()
def get_table_users_activity_summary_with_total(request, user):
    
    dh = get_users_distribution_house(request)
    today=datetime.datetime.today().__str__().split(" ")[0].split("-")
    day = int(today[2])
    month = int(today[1])
    year = int(today[0])
    

    # Findout the yesterday date
    yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)

    query_map = {
        "MERCHANDISER" : TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL,
        "SUPERVISOR" :TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL
            
    }

    QUERY_TODAY = query_map[user].format(day,month,year,dh)
    QUERY_YESTERDAY = query_map[user].format(yesterday_date.day, yesterday_date.month, yesterday_date.year,dh)



    query_job_today = bigquery_client.query(QUERY_TODAY)
    query_job_yesterday = bigquery_client.query(QUERY_YESTERDAY)


    

    # query_result = [row.UniqueclosedOutlets for row in query_job.result()]


    

    data1 = query_job_today.to_dataframe().rename(columns={"Region":"region","ActiveUsers":"active_today","VisitedOutlets":"outlets_visited_today"})
    # .to_dict(orient="records")
    data2 = query_job_yesterday.to_dataframe().rename(columns={"Region":"region","ActiveUsers":"active_yesterday","VisitedOutlets":"outlets_visited_yesterday"})

    data = pd.merge(data1, data2, left_on='region', right_on='region').to_dict(orient="records")
    # .to_dict(orient="records")


    # print(data1)
    # print(data2)
    # print(data)

    

    # result_today = {
    #     row.Region : {
    #         'ActiveUsers' : row.ActiveUsers,
    #         'VisitedOutlets' : row.VisitedOutlets
    #     } for row in rows_today
    # }
    
    # result_yesterday = {
    #     row.Region: {
    #         'ActiveUsers': row.ActiveUsers,
    #         'VisitedOutlets': row.VisitedOutlets
    #     } for row in rows_yesterday
    # }

    # normalized_today_data = table_summary_imputer(result_today)
    # normalized_yesterday_data = table_summary_imputer(result_yesterday)
    

    # dataSource = [
    #     {"region" : region,
    #      "active_today" : normalized_today_data[region]['ActiveUsers'],
    #      "active_yesterday" : normalized_yesterday_data[region]['ActiveUsers'],
    #       "outlets_visited_today" : normalized_today_data[region]['VisitedOutlets'],
    #      "outlets_visited_yesterday" : normalized_yesterday_data[region]['VisitedOutlets']
    #      }

    #     for region in REGIONS
    # ]

    return Response({
        
        'data' :  data 
    })            

###
@api_view()
def get_outlets_not_visited_csv(request,day,month,year,day2,month2,year2):
    
    dh = get_users_distribution_house(request)
    #region, area, territory, distributor_code, wallet_number
    # user = user.lower()

    # today=datetime.datetime.today().__str__().split(" ")[0].split("-")
    day = int(day)
    month = int(month)
    year = int(year)
    day2 = int(day2)
    month2 = int(month2)
    year2 = int(year2)
    # yesterday_date = datetime.datetime(year, month, day) - datetime.timedelta(days=1)
    
    date_range = date_range_generator(day,month,year,day2,month2,year2)
    date1 = datetime.datetime(year, month, day, 0, 0,0)
    date2=datetime.datetime(year2, month2, day2, 23, 59,59)
    time_stamp1=date1.timestamp()*1000
    time_stamp2=date2.timestamp()*1000

    # time_stamp3=datetime.datetime(yesterday_date.year, yesterday_date.month, yesterday_date.day, 0, 0,0).timestamp()*1000
    # time_stamp4=datetime.datetime(yesterday_date.year, yesterday_date.month, yesterday_date.day, 23, 59,59).timestamp()*1000
    

    QUERY1 = OUTLETS_NOT_VISITED_CSV.format(date_range,time_stamp1,time_stamp2,dh,"merchandiser")
    QUERY2 = OUTLETS_NOT_VISITED_CSV.format(date_range,time_stamp1,time_stamp2,dh,"supervisor")

    QUERY = QUERY1 + "union all" + QUERY2

        

    print(QUERY)
    bigquery_result = bigquery_client.query(QUERY)

    data = bigquery_result.to_dataframe()
    print(data)

    download_time = ( datetime.datetime.now()).__str__().replace(' ', '_').split('.')[0][:-3].replace(':', '_')
    date1 = date1.__str__().split(" ")[0]
    date2 = date2.__str__().split(" ")[0]
    file_name = f'outlets_not_visited_from_{date1}_to_{date2}_download_at_{download_time}.xlsx'
    # file_path = f'{settings.BASE_DIR}/dumps/{file_name}'
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    data.to_excel(response, index=False, encoding="utf-16")
    #  pd.DataFrame(data)[COLUMNS].astype(str)



    return response