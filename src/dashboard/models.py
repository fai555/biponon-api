from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class File(models.Model):
    file = models.FileField(blank=False, null=False)
    remark= models.CharField(max_length=20)
    timestamp=models.DateTimeField(auto_now_add=True)





class BipononCenterRegion(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class BipononCenterArea(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class BipononCenterTerritory(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class BipononCenter(models.Model):
    name = models.CharField(max_length=200)
    distribution_house_code = models.CharField(max_length=10)
    ma_wallet = models.CharField(max_length=14, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class UserPermission(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    biponon_center = models.ManyToManyField(BipononCenter)

    def __str__(self):
        return self.user.first_name