###############################################################################
# Believe me, I didn't write any of these code, I just ported it from         #
# Flask to Django on a tight schedule, so couldn't refactor.                  #
# My manager promised me to allow the time to refactor in future.             #
#                                      - (See the name from git blame? :p)    #
###############################################################################
from google.cloud import bigquery

bigquery_client = bigquery.Client()

PROJECT_ID = "bkashbiponon"
DATASET_ID = "biponon"
TABLE_ID = "update_of_outlet"

dataset_ref = bigquery_client.dataset(DATASET_ID)
table_ref = dataset_ref.table(TABLE_ID)
table = bigquery_client.get_table(table_ref)


# SELECT_ACTIVE_UNIQUE_MERCHANDISERS_ON_SPECIFIC_DATE = (
#     """
#     select distinct(whitelisting_number) as phone, name as name, region , distributor_code, Area as area  from `biponon.merchandiser_basic_info`  WHERE  distributor_code IN ({}) AND region <> "TESTER"
  
#     """
# )

###
COUNT_OF_ACTIVE_UNIQUE_MERCHANDISERS_ON_SPECIFIC_DATE= (
    """
    SELECT COUNT(DISTINCT(reporter)) as `ActiveMerchandisers` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {}  AND region <> "TESTER" and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` WHERE  distributor_code IN ({})  )
    """
)
###
COUNT_OF_ACTIVE_UNIQUE_SUPERVISERS_ON_SPECIFIC_DATE= (
    """
 SELECT COUNT(DISTINCT(reporter)) as `ActiveSuperviors` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {} and reporter in (select whitelisting_number  from `biponon.supervisor_basic_info` WHERE  distributor_code IN ({})  )
    """
)


# COUNT_OF_VISITED_OUTLETS_ON_SPECIFIC_DATE= (
#     """
#     SELECT COUNT(DISTINCT(wallet_number)) as `ActiveMerchandisers` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {} and distributor_code IN ({})
#     """
# )


# VISITED_OUTLETS_ON_SPECIFIC_DATE = (
#     """
#     SELECT DISTINCT(wallet_number) as `Outlets` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {}
#     """
# )

# COUNT_OF_UNIQUE_VISITED_CLOSED_OUTLETS = (
#     """
# SELECT COUNT(DISTINCT(wallet_number)) as `UniqueclosedOutlets` FROM `bkashbiponon.biponon.update_of_outlet` where closed=true and distributor_code in ({})
#     """
# )



# TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE = (
# """
# SELECT region as Region, COUNT(DISTINCT(reporter)) as ActiveMerchandisers, COUNT(Distinct(wallet_number)) as VisitedOutlets
# FROM biponon.update_of_outlet
# WHERE year ={} and month = {} and day = {} AND region NOT IN ("TESTER","DHAKA") AND distributor_code NOT IN ("901","902","903","904","905","906","907","908","909")
# GROUP BY region
# """
# )

###
TABLE_MERCHANDISERS_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL = (
"""
select * from
((select A.region as Region, count(distinct B.reporter  ) as ActiveUsers, count(distinct B.wallet_number ) as VisitedOutlets from `biponon.merchandiser_basic_info` as A  left outer join `biponon.update_of_outlet` as  B on A.whitelisting_number = B.reporter and day = {0} and month = {1} and year = {2} and A.distributor_code in ({3})
 group by A.region)
 union all
(SELECT "TOTAL" as Region , COUNT(DISTINCT(B.reporter)) as ActiveMerchandisers, COUNT(Distinct(B.wallet_number)) as VisitedOutlets from `biponon.merchandiser_basic_info` as A  left outer join `biponon.update_of_outlet` as  B on A.whitelisting_number = B.reporter   and day = {0} and month = {1} and year = {2} and A.distributor_code in ({3})
 )) order by Region 

"""
)


# TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE= (
# """
# SELECT region as Region, COUNT(DISTINCT(reporter)) as ActiveSupervisors, COUNT(Distinct(wallet_number)) as VisitedOutlets
# FROM biponon.update_of_outlet
# WHERE year ={} and month = {} and day = {} AND region NOT IN ("TESTER","DHAKA NORTH", "DHAKA SOUTH") AND distributor_code IN ("901","902","903","904","905","906","907","908","909")
# GROUP BY region
# """
# )

###
TABLE_SUPERVISOR_ACTIVITY_SUMMARY_BY_DATE_WITH_TOTAL = (
"""
select * from
((select A.region as Region, count(distinct B.reporter  ) as ActiveUsers, count(distinct B.wallet_number ) as VisitedOutlets from `biponon.supervisor_basic_info` as A  left outer join `biponon.update_of_outlet` as  B on A.whitelisting_number = B.reporter   and day = {0} and month = {1} and year = {2} and A.distributor_code in ({3}) 
 group by A.region)
 union all
(SELECT "TOTAL" as Region , COUNT(DISTINCT(B.reporter)) as ActiveSupervisors, COUNT(Distinct(B.wallet_number)) as VisitedOutlets from `biponon.supervisor_basic_info` as A  left outer join `biponon.update_of_outlet` as  B on A.whitelisting_number = B.reporter   and day = {0} and month = {1} and year = {2} and A.distributor_code in ({3}) 
 )) order by Region 
"""
)

###
TABLE_NEW_UNIQUE_OUTLETS_VISITED = (
"""
SELECT
 concat( cast(B.day as String),'-', cast(month as String),'-', cast(year as String)) as date,
 SUM(IF(A.region = "BARISAL", 1, 0)) AS barisal,
 SUM(IF(A.region = "SYLHET", 1, 0)) AS  sylhet,
 SUM(IF(A.region = "DHAKA NORTH", 1, 0)) AS dhaka_north,
  SUM(IF(A.region = "DHAKA SOUTH", 1, 0)) AS dhaka_south,
 SUM(IF(A.region = "CHITTAGONG", 1, 0)) AS chittagong,
 SUM(IF(A.region = "BOGRA", 1, 0)) AS bogra,
 SUM(IF(A.region = "KHULNA", 1, 0)) AS khulna,
 SUM(IF(A.region = "COMILLA", 1, 0)) AS comilla,
 SUM(IF(A.region = "MYMENSINGH", 1, 0)) AS mymensingh,
 SUM(IF(A.region = "RANGPUR", 1, 0)) AS rangpur
 ,count(A.region) as total
from `biponon.{0}_basic_info` as A  left outer join `biponon.update_of_outlet` as  B on A.whitelisting_number = B.reporter 
where whitelisting_number  in (select distinct  reporter  from `biponon.update_of_outlet` group by reporter  )
GROUP BY
 day, month, year
ORDER BY 
 year desc, month desc, day desc
"""
)

###
TABLE_NEW_UNIQUE_OUTLETS_VISITED_SUPERVISER = (
    """
 SELECT
 concat( cast(B.day as String),'-', cast(month as String),'-', cast(year as String)) as Date,
 SUM(IF(A.region = "BARISAL", 1, 0)) AS BARISAL,
 SUM(IF(A.region = "SYLHET", 1, 0)) AS SYLHET,
 SUM(IF(A.region = "DHAKA NORTH", 1, 0)) AS DHAKA_NORTH,
  SUM(IF(A.region = "DHAKA SOUTH", 1, 0)) AS DHAKA_SOUTH,
 SUM(IF(A.region = "CHITTAGONG", 1, 0)) AS CHITTAGONG,
 SUM(IF(A.region = "BOGRA", 1, 0)) AS BOGRA,
 SUM(IF(A.region = "KHULNA", 1, 0)) AS KHULNA,
 SUM(IF(A.region = "COMILLA", 1, 0)) AS COMILLA,
 SUM(IF(A.region = "MYMENSINGH", 1, 0)) AS MYMENSINGH,
 SUM(IF(A.region = "RANGPUR", 1, 0)) AS RANGPUR
 ,count(A.region) as TOTAL
from `biponon.supervisor_basic_info` as A  left outer join `biponon.update_of_outlet` as  B on A.whitelisting_number = B.reporter 
where whitelisting_number  in (select distinct  reporter  from `biponon.update_of_outlet` group by reporter  )
GROUP BY
 day, month, year
ORDER BY 
 year desc, month desc, day desc
    """
)
###
TOTAL_NUMBER_OF_CALLS_BY_MERCHANDISERS_ALL_DAY = (
    """
SELECT region, COUNT(wallet_number) as OUTLETS_COUNT FROM `biponon.update_of_outlet` WHERE timestamp  >= {0} AND timestamp  <= {1}  and reporter in ( select whitelisting_number  from `biponon.{3}_basic_info` where distributor_code in ({2}) )  GROUP BY region
    """
)

# GET_OUTLET_DETAILS_BY_WALLET_NO = (
#     """
#     SELECT * FROM `biponon.update_of_outlet` WHERE wallet_number='{}' order by upload_time DESC limit 1
#     """
# )

# GET_MERCHANDISER_DETAILS_BY_WHITELISTING_NUMBER = (
#     """
# select whitelisting_number as PhoneNumber, name as name, region as Region , distributor_code as DistributeCode from `biponon.merchandiser_basic_info`  where whitelisting_number="{}" limit 1
#     """
# )

# GET_OUTLETS_GROUPBY_REPORTER = (
#     """
#     select reporter, wallet_number, store_name, address from `biponon.update_of_outlet` where reporter='{}' AND year = {} AND month = {} AND day = {} and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info`  ) group by reporter, wallet_number, store_name, address 
#     """
# )

# GET_RAW_DATA_BY_MONTH = (
#     """
#     select * from biponon.update_of_outlet where  year = {} and month = {}
#     """
# )

# GET_RAW_DATA_BY_DAY = (
#     """
#     select * from biponon.update_of_outlet where  year = {} and month = {} and day = {}
#     """
# )

# GET_ACTIVE_OUTLETS = (
#     """
#     SELECT DISTINCT(wallet_number)as `ActiveOutlet` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {} LIMIT {} OFFSET {}
#     """
# )

# VISITED_UNIQUE_OUTLET_COUNT_ON_MONTH = (
#     """
#     SELECT COUNT(DISTINCT(wallet_number)) as `ActiveOutlets` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and distributor_code IN ({}) 
#     """
# )


# GEO_TAGGING_REPORT_PAGINATED = (
#     """
#     SELECT
#  region,area, distributor_code as DistributionHouse, reporter as WhitelistingNumber,
#  SUM(IF(agent_or_merchant  = "Agent", 1, 0)) AS AgentCount,
#  SUM(IF(agent_or_merchant  = "Merchant", 1, 0)) AS MerchantCount,
#  SUM(IF(closed   = true, 1, 0)) AS ClosedOutlet,
#  SUM(IF(is_inactive = true, 1, 0)) AS InActiveOutlet,
#  SUM(IF(updated_address is not null, 1, 0)) AS AddressNotOkFound


# FROM
#  `biponon.update_of_outlet`
# WHERE
#  year={} and month={} and day={}
# GROUP BY
#  region, area ,distributor_code , reporter limit {} offset {};
#     """
# )


# GET_LAST_N_OUTLET_INFORMATION = (
#     """
#     SELECT * FROM `biponon.update_of_outlet` WHERE region <> "TESTER" AND reporter in (select whitelisting_number  from ((select whitelisting_number, distributor_code   from `biponon.merchandiser_basic_info`) union all (select whitelisting_number, distributor_code  from `biponon.supervisor_basic_info`)  ) where  distributor_code in ({1}) ) ORDER BY upload_time DESC LIMIT {0}
#     """
# )



# TARGET_VS_ACHIEVEMENT = (
#     """
# WITH A as
# (SELECT region,  count( wallet_no ) as CallTarget
# FROM(
# SELECT region, wallet_no , row_number() over (partition by wallet_no ) row_number
# FROM `biponon.merchandiser`
# WHERE {0} and region <> "DHAKA" and whitelisting_number  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301")
# ORDER BY timestamp desc )
# WHERE row_number = 1
# GROUP BY region),
# B as
# (SELECT  region , count(distinct wallet_number) as Achievement
# FROM `bkashbiponon.biponon.update_of_outlet`
# where  {0} and region<> "DHAKA"  and distributor_code<> "000"  and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({1}) )
# GROUP BY region),
# C as
# (
# SELECT region, count(distinct whitelisting_number ) as Assigned_Merchandiser
# from `biponon.merchandiser`
# where {0} and region <> "DHAKA" and whitelisting_number  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301")
# GROUP BY region),
# D as
# (
# select region, count(distinct reporter) as Active_Merchandiser
# from `biponon.update_of_outlet`
# where {0} and region<> "DHAKA"  and distributor_code <> "000" and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({1}) )
# GROUP BY region),
# E as
# (
# select  A.region , CEIL(B.Achievement*100/A.CallTarget) as Percentage
# from A join B on A.region = B.region
# ),
# F as
# (
# select  A.region , C.Assigned_Merchandiser-D.Active_Merchandiser as Inactive
# from (A join C on A.region = C.region) join D on A.region = D.region
# )
# select A.region as Region , A.CallTarget , B.Achievement, E.Percentage, C.Assigned_Merchandiser , D.Active_Merchandiser, F.Inactive    from ((((A join B on A.region = B.region) join C on A.region= C.region) join D on A.region= D.region) join E on A.region= E.region) join F on A.region= F.region
# order by region
#     """
# )

# MERCHANDISER_DIDNT_MAKE_ANY_CALL = (
#     """
#      select distinct(whitelisting_number) as Whitelisting_Number, L.region as Region, L.distributor_code as Distributor_Code, count(distinct(L.wallet_no))as Target_Call from `biponon.merchandiser`  as L  where L.whitelisting_number not in (select distinct(reporter) from `biponon.update_of_outlet` where timestamp  >= {0} AND timestamp  <= {1}  ) and distributor_code in ({2}) and timestamp  >= {0} AND timestamp  <= {1} group by L.whitelisting_number, L.region , L.distributor_code
#     """ 
# )


# GET_AREA_BY_REGION = (
#     """
# select distinct area from(
# select distinct area from `biponon.merchandiser_basic_info` where region  = "{0}" and distributor_code in ({1})
# union all 
# select distinct area from `biponon.supervisor_basic_info`  where region  = "{0}" and distributor_code in ({1}))
#     """
# )

# GET_TERRITORY_BY_AREA = (
#     """

# select distinct territory  from(
# select distinct territory from `biponon.merchandiser_basic_info` where area  = "{0}" and distributor_code in ({1})
# union all 
# select distinct territory from `biponon.supervisor_basic_info`  where area  = "{0}" and distributor_code in ({1}))
#     """
# )

# GET_TERRITORY_BY_AREA_MERCAHNDISER = (
#     """
# select distinct territory from `biponon.merchandiser_basic_info` where area  = "{0}" and distributor_code in ({1})

#     """
# )

# GET_TERRITORY_BY_AREA_SUPERVISOR = (
#     """
# select distinct territory from `biponon.supervisor_basic_info`  where area  = "{0}" and distributor_code in ({1})
#     """
# )

# GET_DISTRIBUTOR_CODE_BY_TERRITORY = (
#     """
#     -- v2_get_distributor_code
# select distinct distributor_code   from(
# select distinct distributor_code  from `biponon.merchandiser_basic_info` where territory   = "{0}" and distributor_code in ({1})
# union all 
# select distinct distributor_code  from `biponon.supervisor_basic_info`  where territory   = "{0}" and distributor_code in ({1}) )
#     """
# )

# GET_MERCHANDISERS_LIST_BY_DISTRIBUTOR_CODE = (
#     """
#     select distinct(whitelisting_number) from `biponon.merchandiser_basic_info`  where distributor_code = "{}" and distributor_code in ({})
#     """
# )

# GET_REGION_LIST = (
#     """
#     select distinct(region) from `biponon.merchandiser` 
#     """
# )


# TARGET_VS_ACHIEVEMENT_BY_MERCHANDISER = (
#     """
# with a as
# (SELECT whitelisting_number,  count( distinct wallet_no ) as CallTarget
# FROM(
# SELECT wallet_no, whitelisting_number ,timestamp, row_number() over (partition by wallet_no ) row_number
# FROM `biponon.merchandiser` 
# WHERE {0} 
# ORDER BY timestamp desc )
# WHERE row_number = 1
# GROUP BY whitelisting_number),
# b as
# (SELECT reporter ,  count( distinct wallet_number  ) as Achievement
# FROM(
# SELECT wallet_number , reporter 
# FROM `biponon.update_of_outlet`  
# WHERE timestamp >= {1} and timestamp <= {2}
# )
# GROUP BY reporter ),
#  c as
# (select whitelisting_number, name, region, area, territory, distributor_code, Distributor, type from `biponon.merchandiser_basic_info` where distributor_code in ({3})  )
# SELECT  c.whitelisting_number as whitelisting_number, c.name as name, c.region as region, c.area as area, c.territory as territory, c.Distributor as distributor, c.type as type, 
# case
# when c.type = "Metro" and CallTarget >45
# then 45
# when c.type = "Rural" and CallTarget >35
# then 35
# when c.type = "Urban" and CallTarget >40
# then 40
# else ifnull(CallTarget,0)
# end
# as CallTarget ,ifnull(CallTarget,0) as Actual_CallTarget,
# ifnull(b.Achievement,0)  as Achievement
# FROM ( a  left join c on a.whitelisting_number= c.whitelisting_number  )left join  b on 
# a.whitelisting_number = b.reporter

#     """
# )

# VISITED_OUTLETS_BY_MERCHANDISER_ON_A_GIVEN_DATE = (
# """
# SELECT *
# FROM `biponon.update_of_outlet`
# WHERE reporter ="{0}" and timestamp  > {1} and timestamp  < {2} and reporter in ( select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({3})
# union all 
# select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({3})
# ) ORDER BY upload_time DESC
# """
# )

# GET_GEOLOCATION_OF_OUTLETS = (
#     """
#     SELECT row.wallet_number, row.store_name,row.address, row.revalidate_lat,row.revalidate_lng FROM (
# SELECT ARRAY_AGG(t ORDER BY timestamp DESC LIMIT 1)[OFFSET(0)] AS row
# FROM `biponon.update_of_outlet` AS t
# GROUP BY t.wallet_number
# )
#     """
# )


# GET_GEOLOCATION_BY_OUTLET = (
#     """
#     SELECT row.wallet_number, row.store_name,row.address, row.revalidate_lat,row.revalidate_lng FROM (
# SELECT ARRAY_AGG(t ORDER BY timestamp DESC LIMIT 1)[OFFSET(0)] AS row
# FROM `biponon.update_of_outlet` AS t
# GROUP BY t.wallet_number
# )  WHERE row.wallet_number = "{}"
#     """
# )
###
GET_AN_OUTLET = (
    """
SELECT row.* FROM (
SELECT ARRAY_AGG(t ORDER BY timestamp DESC LIMIT 1)[OFFSET(0)] AS row
FROM `biponon.update_of_outlet` AS t
where t.wallet_number="{0}" and t.distributor_code in ({1})
)
    """
)

###
GET_A_MERCHANDISER_SUPERVISOR = (
    """

select *
from
((SELECT row.whitelisting_number as Whitelisting_Number    , row.region  , row.area, row.territory, row.name, row.distributor_code FROM (
SELECT ARRAY_AGG(t ORDER BY t.name  DESC LIMIT 1)[OFFSET(0)] AS row
FROM `biponon.merchandiser_basic_info`    AS t
where t.whitelisting_number   = "{0}" and t.distributor_code in ({1})
))
UNION all
(SELECT row.whitelisting_number as Whitelisting_Number    , row.region  , row.area, row.territory, row.name, row.distributor_code FROM (
SELECT ARRAY_AGG(t ORDER BY t.name  DESC LIMIT 1)[OFFSET(0)] AS row
FROM `biponon.supervisor_basic_info`     AS t
where t.whitelisting_number   = "{0}" and t.distributor_code in ({1})
)))
where Whitelisting_Number <> "null"
    """
)

# GET_PHONE_AND_PIC_URL = (
#     """
#     select distinct wallet_number , file_path  from `biponon.update_of_outlet` where day = {} and month = {} and year = {} LIMIT {}
#     """
# )

# GET_OUTLET_VISITORS = (
#     """
# select reporter as merchandiser, day, month, year from `biponon.update_of_outlet` where wallet_number = "{}" order by year , month, day
#     """
# )

# GET_REGION_WISE_TARGET_VS_ACHIEVEMENT_ON_SPECIFIC_DATE = (
#     """
#     With subq1 as
# (SELECT region, count(wallet_no) AS CallTarget
# FROM `bkashbiponon.biponon.merchandiser`
# WHERE {0}
# GROUP BY region
# ORDER BY region),
# subq2 as
# (SELECT  region, count(distinct wallet_number) as Achievement
# FROM `bkashbiponon.biponon.update_of_outlet`
# WHERE {0} and region <> "DHAKA" and area <> "SUPERVISOR" and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info`  )
# GROUP BY region
# ORDER BY region)
# select L.region as region, L.CallTarget as call_target, R.Achievement as achievement  from subq1 as L join subq2 as R on L.region = R.region
#     """
# )


# GET_COUNT_UNIQUE_INACTIVE_OUTLETS_ON_SPECIFIC_MONTH=(

# """
# select count(distinct wallet_number) as total_inactive_outlets from `biponon.update_of_outlet` where is_inactive= true and month ={}  and distributor_code in ({})

# """

# )

# GET_COUNT_UNIQUE_NOT_FOUND_OUTLETS_ON_SPECIFIC_MONTH=(

# """
# select count(distinct wallet_number) as total_not_found_outlets  from `biponon.update_of_outlet` where not_found= "1" and month ={}  and distributor_code in ({})

# """
# )

# GET_COUNT_UNIQUE_OPEN_OUTLET_SPECIFIC_MONTH =(

# """
# SELECT count(distinct (wallet_number)) as OpenOutlet
# FROM `biponon.update_of_outlet`
# WHERE month = {} AND year ={} AND (not_found="0"  and is_inactive= false and closed= false)  and distributor_code in ("100","164")
# """

# )


# GET_COUNT_UNIQUE_NOT_FOUND_OUTLET_SPECIFIC_MONTH = (

# """
# SELECT count(distinct (wallet_number)) as ClosedOutlet
# FROM `biponon.update_of_outlet`
# WHERE month = {} AND year = {} AND not_found = "1"  and distributor_code in ({})
# """
# )

# GET_COUNT_UNIQUE_INACTIVE_OUTLET_SPECIFIC_MONTH = (

# """
# SELECT count(distinct (wallet_number)) as InactiveOutlet
# FROM `biponon.update_of_outlet`
# WHERE month = {} AND year = {} AND is_inactive= true  and distributor_code in ({})
# """
# )

# GET_COUNT_UNIQUE_CLOSED_OUTLET_SPECIFIC_MONTH = (

# """
# SELECT count(distinct (wallet_number)) as ClosedOutlet
# FROM `biponon.update_of_outlet`
# WHERE month = {} AND year = {} AND closed= true and distributor_code in ({})
# """
# )


# GET_COUNT_UNIQUE_OPEN_OUTLET_SPECIFIC_DATE =(

# """
# SELECT count(distinct (wallet_number)) as OpenOutlet
# FROM `biponon.update_of_outlet`
# WHERE day = {} AND month = {} AND year = {} AND (not_found="0"  and is_inactive= false and closed= false)  and distributor_code in ("100","164")
# """

# )


# GET_COUNT_UNIQUE_NOT_FOUND_OUTLET_SPECIFIC_DATE = (

# """
# SELECT count(distinct (wallet_number)) as ClosedOutlet
# FROM `biponon.update_of_outlet`
# WHERE day= {} AND month = {} AND year = {} AND not_found = "1"  and distributor_code in ({})
# """
# )

# GET_COUNT_UNIQUE_INACTIVE_OUTLET_SPECIFIC_DATE = (

# """
# SELECT count(distinct (wallet_number)) as InactiveOutlet
# FROM `biponon.update_of_outlet`
# WHERE day= {} AND month = {} AND year = {} AND is_inactive= true and distributor_code in ({})
# """
# )

# GET_COUNT_UNIQUE_CLOSED_OUTLET_SPECIFIC_DATE = (

# """
# SELECT count(distinct (wallet_number)) as ClosedOutlet
# FROM `biponon.update_of_outlet`
# WHERE day = {} AND month = {} AND year = {} AND closed= true and distributor_code in ({})
# """
# )




# GET_COUNT_UNIQUE_PIC_URL_SPECIFIC_DATE = (
# """ 
# SELECT count(distinct file_path) AS Count
# FROM `biponon.update_of_outlet`
# WHERE day = {} and month = {} and year = {}  and distributor_code in ({})
# """
# )
###
GET_TOTAL_UNIQUE_OUTLETS =(

"""
SELECT count(distinct(wallet_number)) as Total FROM `biponon.update_of_outlet` where distributor_code in ({})
"""
)
###
GIT_MERCHANDISER_VISITED_IN_SPECIFIC_OUTLET =(

"""
(select distinct a.reporter as Reporter,b.name ,"Merchandiser" as user_type,a.region,a.area,a.territory,b.Distributor,wallet_number,year,month,day,report_key,address,agent_or_merchant,answer1,answer2,bmcc,checkin_lat,checkin_lng,closed,file_path,file_path2,filepath3,is_inactive,not_found,lat,lng,revalidate_lat,revalidate_lng,store_name,timestamp,updated_address,updated_store_name,upload_time,qr
from `biponon.update_of_outlet` as a join `biponon.merchandiser_basic_info` as b on reporter = whitelisting_number 
where wallet_number = "{0}" and a.distributor_code in ({1})
order by year desc,month desc,day desc)
union all 
(select distinct a.reporter as Reporter,b.name ,"Supervisor" as user_type,a.region,a.area,a.territory,b.Distributor,wallet_number,year,month,day,report_key,address,agent_or_merchant,answer1,answer2,bmcc,checkin_lat,checkin_lng,closed,file_path,file_path2,filepath3,is_inactive,not_found,lat,lng,revalidate_lat,revalidate_lng,store_name,timestamp,updated_address,updated_store_name,upload_time,qr
from `biponon.update_of_outlet` as a join `biponon.supervisor_basic_info` as b on reporter = whitelisting_number 
where wallet_number = "{0}" and a.distributor_code in ({1})
order by year desc,month desc,day desc)
"""

)

###
TEST_GET_COUNT_UNIQUE_OUTLET= (

"""
(with a as
(SELECT count(distinct (wallet_number)) as total,"{5}" as type
FROM `biponon.update_of_outlet`
WHERE {1} and
{0}
and
 region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter in (select whitelisting_number  from ((select whitelisting_number, distributor_code   from `biponon.merchandiser_basic_info`) union all (select whitelisting_number, distributor_code  from `biponon.supervisor_basic_info`)  ) 
 where  distributor_code in ({4})
 )
and distributor_code in ({4})

),
b as
(
SELECT count(distinct (wallet_number)) as this_month, "{5}" as type
FROM `biponon.update_of_outlet`
WHERE  {2} and
{0}
and
 region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter in (select whitelisting_number  from ((select whitelisting_number, distributor_code   from `biponon.merchandiser_basic_info`) union all (select whitelisting_number, distributor_code  from `biponon.supervisor_basic_info`)  ) 
 where  distributor_code in ({4})
 )
and distributor_code in ({4})

),
c as
(
SELECT count(distinct (wallet_number)) as pre_month, "{5}" as type
FROM `biponon.update_of_outlet`
WHERE  {3} and
{0}
and
 region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter in (select whitelisting_number  from ((select whitelisting_number, distributor_code   from `biponon.merchandiser_basic_info`) union all (select whitelisting_number, distributor_code  from `biponon.supervisor_basic_info`)  ) 
 where  distributor_code in ({4})
 )
and distributor_code in ({4})

)
select "{5}" as type,total, this_month, pre_month  from ( a join b on a.type = b.type ) join c on b.type=c.type)

"""
)


# COUNT_OF_ACTIVE_UNIQUE_USER= (
#     """
#  SELECT COUNT(DISTINCT(reporter)) as `ActiveMerchandisers` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {} AND distributor_code not in ("901","902","903","904","905","906","907","908","909") AND region <> "TESTER"
# Union all
# SELECT COUNT(DISTINCT(reporter)) as `ActiveSupervisor` FROM `bkashbiponon.biponon.update_of_outlet` where year = {} and month = {} and day = {} AND distributor_code in ("901","902","903","904","905","906","907","908","909") AND region <> "TESTER"  

#    """
# )

###
GET_DISTINCT_RAW_DATA_FOR_MERCHANDISER_CSV = (

"""
(select * except(row_number,time) from
(SELECT region as Region,area as Area, territory as Territory, Distributor, 
substr(reporter ,  -10)
 as Merchandiser , 
 substr(wallet_number  ,  -10)
as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , 
IF((not_found = "1"), 'Not Found','Found') 
 as Is_the_Outlet_Reported_Unfound,
case
when not_found = "0" and closed = true 
then "Closed"
when not_found = "0" and closed = false and is_inactive= false 
then "Open"
when not_found = "0" and closed = false and is_inactive= true 
then "Inactive"
when not_found ="1"
then "N/A"
end as Is_Outlet_Status_Open_Closed_Inactive,
 transaction_id as Transaction_ID, date as Calls_Date,FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Calls_Time , FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)),"+06:00")) as Calls_Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Calls_Upload_Time,accuracy as Accuracy, 
IF((agent = 'true'), 'yes','no') as Is_there_Another_Account_Agent, 
IF((merchant = 'true'), 'yes','no') as Is_there_Another_Account_Merchant , 
IF((merchantPlus = 'true'), 'yes','no')  as Is_there_Another_Account_Merchant_Plus, 
IF((sme = 'true'), 'yes','no')  as Is_there_Another_Account_SME, 
substr(agentNumber, -10)
 as The_Other_Number_Agent,  
substr(merchantNumber,  -10)
 as The_Other_Number_Merchant,
 substr(merchantPlusNumber,  -10)
as The_Other_Number_Merchant_Plus,
substr(smeNumber,  -10)
as The_Other_Number_SME, 
case
when qr= true
then "yes"
when qr=false
then "no"
else "N/A"
end as QR,
row_number() over (partition by reporter,wallet_number ,date  ) row_number,time
from
(
SELECT distinct wallet_number ,a.region, a.area , a.territory , a.distributor_code ,b.Distributor ,reporter ,  bmcc , store_name , address , updated_address , updated_store_name , agent_or_merchant , answer1 , answer2, lat, lng , closed , is_inactive , not_found , transaction_id , timestamp , upload_time , accuracy , agent, merchant  , merchantPlus , sme, agentNumber ,  merchantNumber ,merchantPlusNumber ,smeNumber, qr, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date ,timestamp as time
FROM `bkashbiponon.biponon.update_of_outlet` as a left join `bkashbiponon.biponon.merchandiser_basic_info` as b on a.distributor_code = b.distributor_code
where 
timestamp >= {0} AND 
timestamp <= {1} and
is_inactive = false and
 a.region <>"TESTER" and a.distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({2})  ) 
order by timestamp desc)
)
where  row_number=1
order by time desc)


"""


)

###
GET_DISTINCT_RAW_DATA_FOR_SUPERVISOR_CSV = (

"""
(select * except(row_number,time) from
(SELECT region as Region,area as Area, territory as Territory, Distributor, 
substr(reporter ,  -10)
 as Supervisor , 
 substr(wallet_number  ,  -10)
as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , 
IF((not_found = "1"), 'Not Found','Found') 
 as Is_the_Outlet_Reported_Unfound,
case
when not_found = "0" and closed = true 
then "Closed"
when not_found = "0" and closed = false and is_inactive= false 
then "Open"
when not_found = "0" and closed = false and is_inactive= true 
then "Inactive"
when not_found ="1"
then "N/A"
end as Is_Outlet_Status_Open_Closed_Inactive,
 transaction_id as Transaction_ID, date as Calls_Date,FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Calls_Time , FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)),"+06:00")) as Calls_Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Calls_Upload_Time,accuracy as Accuracy, 
IF((agent = 'true'), 'yes','no') as Is_there_Another_Account_Agent, 
IF((merchant = 'true'), 'yes','no') as Is_there_Another_Account_Merchant , 
IF((merchantPlus = 'true'), 'yes','no')  as Is_there_Another_Account_Merchant_Plus, 
IF((sme = 'true'), 'yes','no')  as Is_there_Another_Account_SME, 
substr(agentNumber, -10)
 as The_Other_Number_Agent,  
substr(merchantNumber,  -10)
 as The_Other_Number_Merchant,
 substr(merchantPlusNumber,  -10)
as The_Other_Number_Merchant_Plus,
substr(smeNumber,  -10)
as The_Other_Number_SME, 
case
when qr= true
then "yes"
when qr=false
then "no"
else "N/A"
end as QR,
row_number() over (partition by reporter,wallet_number ,date  ) row_number,time
from
(
SELECT distinct wallet_number ,a.region, a.area , a.territory , a.distributor_code ,b.Distributor ,reporter ,  bmcc , store_name , address , updated_address , updated_store_name , agent_or_merchant , answer1 , answer2, lat, lng , closed , is_inactive , not_found , transaction_id , timestamp , upload_time , accuracy , agent, merchant  , merchantPlus , sme, agentNumber ,  merchantNumber ,merchantPlusNumber ,smeNumber, qr, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date ,timestamp as time
FROM `bkashbiponon.biponon.update_of_outlet` as a left join `bkashbiponon.biponon.merchandiser_basic_info` as b on a.distributor_code = b.distributor_code
where 
timestamp >= {0} AND 
timestamp <= {1} and
is_inactive = false and
 a.region <>"TESTER" and a.distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({2})  ) 
order by timestamp desc)
)
where  row_number=1
order by time desc)

"""
)
###
# GET_MERCHANT_RAW_DUMP_FROM_21DEC_TILL_DATE_SUPERVISOR_CSV =(

# """
# SELECT region as Region,area as Area, territory as Territory, distributor_code, 
# case 
# when length(reporter) > 11
# then substr(reporter, 5, 14)
# else substr(reporter, 2, 11)
# end as Supervisor , 
# case 
# when length(wallet_number) > 11
# then substr(wallet_number,  5, 14)
# else substr(wallet_number, 2, 11)
# end as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , closed as Is_the_Outlet_Closed, is_inactive as Is_the_Outlet_Inactive , 
# case 
# when not_found = "0" 
# then false 
# else true 
# end as Is_the_Outlet_Reported_Unfound , transaction_id as Transaction_ID, timestamp as Call_Timestamp, upload_time as Call_Upload_Time, accuracy as Accuracy, agent as Is_there_Another_Account_Agent, merchant as Is_there_Another_Account_Merchant , merchantPlus as Is_there_Another_Account_Merchant_Plus, sme as Is_there_Another_Account_SME, 
# case 
# when length(agentNumber) > 11
# then substr(agentNumber, 5, 14)
# else substr(agentNumber, 2, 11)
# end as The_Other_Number_Agent,  
# case 
# when length(merchantNumber) > 11
# then substr(merchantNumber,  5, 14)
# else substr(merchantNumber, 2, 11)
# end  as The_Other_Number_Merchant,
# case 
# when length(merchantPlusNumber) > 11
# then substr(merchantPlusNumber,  5, 14)
# else substr(merchantPlusNumber, 2, 11)
# end as The_Other_Number_Merchant_Plus,
# case 
# when length(smeNumber) > 11
# then substr(smeNumber,  5, 14)
# else substr(smeNumber, 2, 11)
# end as The_Other_Number_SME, qr as QR         
#   FROM `biponon.update_of_outlet`
#   WHERE (timestamp >= {0} AND timestamp <= {1} ) and region <> "DHAKA" and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") AND agent_or_merchant<> "Agent" AND agent_or_merchant<> "AGENT" AND agent_or_merchant<> "agent" and reporter in (select whitelisting_number  from `biponon.supervisor_basic_info`  ) and distributor_code in ({2}) order by timestamp desc

# """

# )
###
# GET_MERCHANT_RAW_DUMP_FROM_21DEC_TILL_DATE_MERCHANDISER_CSV =(

# """
# SELECT region as Region,area as Area, territory as Territory, distributor_code, 
# case 
# when length(reporter) > 11
# then substr(reporter, 5, 14)
# else substr(reporter, 2, 11)
# end as Merchandiser , 
# case 
# when length(wallet_number) > 11
# then substr(wallet_number,  5, 14)
# else substr(wallet_number, 2, 11)
# end as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , closed as Is_the_Outlet_Closed, is_inactive as Is_the_Outlet_Inactive , 
# case 
# when not_found = "0" 
# then false 
# else true 
# end as Is_the_Outlet_Reported_Unfound , transaction_id as Transaction_ID, timestamp as Call_Timestamp, upload_time as Call_Upload_Time, accuracy as Accuracy, agent as Is_there_Another_Account_Agent, merchant as Is_there_Another_Account_Merchant , merchantPlus as Is_there_Another_Account_Merchant_Plus, sme as Is_there_Another_Account_SME, 
# case 
# when length(agentNumber) > 11
# then substr(agentNumber, 5, 14)
# else substr(agentNumber, 2, 11)
# end as The_Other_Number_Agent,  
# case 
# when length(merchantNumber) > 11
# then substr(merchantNumber,  5, 14)
# else substr(merchantNumber, 2, 11)
# end  as The_Other_Number_Merchant,
# case 
# when length(merchantPlusNumber) > 11
# then substr(merchantPlusNumber,  5, 14)
# else substr(merchantPlusNumber, 2, 11)
# end as The_Other_Number_Merchant_Plus,
# case 
# when length(smeNumber) > 11
# then substr(smeNumber,  5, 14)
# else substr(smeNumber, 2, 11)
# end as The_Other_Number_SME, qr as QR         
#   FROM `biponon.update_of_outlet`
#   WHERE (timestamp >= {0} AND timestamp <= {1} ) and region <> "DHAKA" and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") AND agent_or_merchant<> "Agent" AND agent_or_merchant<> "AGENT" AND agent_or_merchant<> "agent" and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info`  ) and distributor_code in ({2}) order by timestamp desc


# """


# )
###
GET_RAW_DATA_MERCHANDISER_FOR_MERCHANT_CSV = (

"""
(select * except(row_number,time) from
(SELECT region as Region,area as Area, territory as Territory, Distributor, 
substr(reporter ,  -10)
 as Merchandiser , 
 substr(wallet_number  ,  -10)
as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , 
IF((not_found = "1"), 'Not Found','Found') 
 as Is_the_Outlet_Reported_Unfound,
case
when not_found = "0" and closed = true 
then "Closed"
when not_found = "0" and closed = false and is_inactive= false 
then "Open"
when not_found = "0" and closed = false and is_inactive= true 
then "Inactive"
when not_found ="1"
then "N/A"
end as Is_Outlet_Status_Open_Closed_Inactive,
 transaction_id as Transaction_ID, date as Calls_Date,FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Calls_Time , FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)),"+06:00")) as Calls_Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Calls_Upload_Time,accuracy as Accuracy, 
IF((agent = 'true'), 'yes','no') as Is_there_Another_Account_Agent, 
IF((merchant = 'true'), 'yes','no') as Is_there_Another_Account_Merchant , 
IF((merchantPlus = 'true'), 'yes','no')  as Is_there_Another_Account_Merchant_Plus, 
IF((sme = 'true'), 'yes','no')  as Is_there_Another_Account_SME, 
substr(agentNumber, -10)
 as The_Other_Number_Agent,  
substr(merchantNumber,  -10)
 as The_Other_Number_Merchant,
 substr(merchantPlusNumber,  -10)
as The_Other_Number_Merchant_Plus,
substr(smeNumber,  -10)
as The_Other_Number_SME, 
case
when qr= true
then "yes"
when qr=false
then "no"
else "N/A"
end as QR,
row_number() over (partition by reporter,wallet_number ,date  ) row_number,time
from
(
SELECT distinct wallet_number ,a.region, a.area , a.territory , a.distributor_code ,b.Distributor ,reporter ,  bmcc , store_name , address , updated_address , updated_store_name , agent_or_merchant , answer1 , answer2, lat, lng , closed , is_inactive , not_found , transaction_id , timestamp , upload_time , accuracy , agent, merchant  , merchantPlus , sme, agentNumber ,  merchantNumber ,merchantPlusNumber ,smeNumber, qr, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date ,timestamp as time
FROM `bkashbiponon.biponon.update_of_outlet` as a left join `bkashbiponon.biponon.merchandiser_basic_info` as b on a.distributor_code = b.distributor_code
where 
timestamp >= {0} AND 
timestamp <= {1} and
is_inactive = false and
 a.region <>"TESTER" and a.distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({2})  ) 
 and agent_or_merchant not in ("AGENT" , "Agent")
order by timestamp desc)
)
where  row_number=1
order by time desc)


"""
)
###
GET_RAW_DATA_SUPERVISOR_FOR_MERCHANT_CSV = (

"""
(select * except(row_number,time) from
(SELECT region as Region,area as Area, territory as Territory, Distributor, 
substr(reporter ,  -10)
 as Supervisor , 
 substr(wallet_number  ,  -10)
as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , 
IF((not_found = "1"), 'Not Found','Found') 
 as Is_the_Outlet_Reported_Unfound,
case
when not_found = "0" and closed = true 
then "Closed"
when not_found = "0" and closed = false and is_inactive= false 
then "Open"
when not_found = "0" and closed = false and is_inactive= true 
then "Inactive"
when not_found ="1"
then "N/A"
end as Is_Outlet_Status_Open_Closed_Inactive,
 transaction_id as Transaction_ID, date as Calls_Date,FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Calls_Time , FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)),"+06:00")) as Calls_Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Calls_Upload_Time,accuracy as Accuracy, 
IF((agent = 'true'), 'yes','no') as Is_there_Another_Account_Agent, 
IF((merchant = 'true'), 'yes','no') as Is_there_Another_Account_Merchant , 
IF((merchantPlus = 'true'), 'yes','no')  as Is_there_Another_Account_Merchant_Plus, 
IF((sme = 'true'), 'yes','no')  as Is_there_Another_Account_SME, 
substr(agentNumber, -10)
 as The_Other_Number_Agent,  
substr(merchantNumber,  -10)
 as The_Other_Number_Merchant,
 substr(merchantPlusNumber,  -10)
as The_Other_Number_Merchant_Plus,
substr(smeNumber,  -10)
as The_Other_Number_SME, 
case
when qr= true
then "yes"
when qr=false
then "no"
else "N/A"
end as QR,
row_number() over (partition by reporter,wallet_number ,date  ) row_number,time
from
(
SELECT distinct wallet_number ,a.region, a.area , a.territory , a.distributor_code ,b.Distributor ,reporter ,  bmcc , store_name , address , updated_address , updated_store_name , agent_or_merchant , answer1 , answer2, lat, lng , closed , is_inactive , not_found , transaction_id , timestamp , upload_time , accuracy , agent, merchant  , merchantPlus , sme, agentNumber ,  merchantNumber ,merchantPlusNumber ,smeNumber, qr, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date ,timestamp as time
FROM `bkashbiponon.biponon.update_of_outlet` as a left join `bkashbiponon.biponon.merchandiser_basic_info` as b on a.distributor_code = b.distributor_code
where 
timestamp >= {0} AND 
timestamp <= {1} and
is_inactive = false and
 a.region <>"TESTER" and a.distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({2})  ) 
  and agent_or_merchant not in ("AGENT" , "Agent")
order by timestamp desc)
)
where  row_number=1
order by time desc)

"""
)

# TARGET_VS_ACHIEVEMENT_SUPERVISOR = (
#     """
#    WITH A as
# (SELECT region,  count( wallet_no ) as CallTarget
# FROM(
# SELECT region, wallet_no , row_number() over (partition by wallet_no ) row_number
# FROM `biponon.supervisor`
# WHERE {0} and region <> "DHAKA" and whitelisting_number  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301")
# ORDER BY timestamp desc )
# WHERE row_number = 1
# GROUP BY region),
# B as
# (SELECT  region , count(distinct wallet_number) as Achievement
# FROM `bkashbiponon.biponon.update_of_outlet`
# where  {0} and region<> "DHAKA"  and distributor_code<> "000"  and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({1}) )
# GROUP BY region),
# C as
# (
# SELECT region, count(distinct whitelisting_number ) as Assigned_Supervisor
# from `biponon.supervisor`
# where {0} and region <> "DHAKA" and whitelisting_number  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301")
# GROUP BY region),
# D as
# (
# select region, count(distinct reporter) as Active_Supervisor
# from `biponon.update_of_outlet`
# where {0} and region<> "DHAKA"  and distributor_code <> "000" and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({1}) )
# GROUP BY region),
# E as
# (
# select  A.region , CEIL(B.Achievement*100/A.CallTarget) as Percentage
# from A join B on A.region = B.region
# ),
# F as
# (
# select  A.region , C.Assigned_Supervisor-D.Active_Supervisor as Inactive
# from (A join C on A.region = C.region) join D on A.region = D.region
# )
# select A.region as Region , A.CallTarget , B.Achievement, E.Percentage, C.Assigned_Supervisor , D.Active_Supervisor, F.Inactive    from ((((A join B on A.region = B.region) join C on A.region= C.region) join D on A.region= D.region) join E on A.region= E.region) join F on A.region= F.region
# order by region
#     """
# )

# TARGET_VS_ACHIEVEMENT_BY_SUPERVISOR = (
#     """
#     with a as
# (SELECT whitelisting_number,  count( distinct wallet_no ) as CallTarget
# FROM(
# SELECT wallet_no, whitelisting_number ,timestamp, row_number() over (partition by wallet_no ) row_number
# FROM `biponon.supervisor` 
# WHERE {0} 
# ORDER BY timestamp desc )
# WHERE row_number = 1
# GROUP BY whitelisting_number),
# b as
# (SELECT reporter ,  count( distinct wallet_number  ) as Achievement
# FROM(
# SELECT wallet_number , reporter 
# FROM `biponon.update_of_outlet`  
# WHERE {0}
# )
# GROUP BY reporter ),
#  c as
# (select whitelisting_number, name, region, area, territory, distributor_code, Distributor, type from `biponon.supervisor_basic_info` where distributor_code in ({1})  )
# SELECT  c.whitelisting_number as whitelisting_number, c.name as name, c.region as region, c.area as area, c.territory as territory, c.Distributor as distributor, c.type as type, 
# case
# when  CallTarget >35
# then 35
# else ifnull(CallTarget,0)
# end
# as CallTarget ,ifnull(CallTarget,0) as Actual_CallTarget,
# ifnull(b.Achievement,0)  as Achievement
# FROM ( a  left join c on a.whitelisting_number= c.whitelisting_number  )left join  b on 
# a.whitelisting_number = b.reporter
#     """
# )



# TARGET_VS_ACHIEVEMENT_BY_MERCHANDISER_NUMBER = (
#     """
# select distinct(whitelisting_number) as Whitelisting_Number, COUNT(Distinct(L.wallet_no)) as Target_Call,COUNT(DISTINCT(report_key)) as Total_Call, COUNT(DISTINCT(wallet_number)) as Unique_Call, round(COUNT(DISTINCT(wallet_number))/COUNT(DISTINCT(wallet_no))* 100,2) as Achievement_Percentage from `biponon.merchandiser` as L, `biponon.update_of_outlet` where whitelisting_number = reporter and L.whitelisting_number ="{}" and L.day = {} and L.month = {} and L.year = {} and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info`  ) group by whitelisting_number
#     """
# )

# GET_MERCHANDISER_SUPERVISOR_CALL_START_TIME = (

# """
# SELECT region, COUNT(wallet_number) as outlets_count FROM `biponon.update_of_outlet` WHERE timestamp>={} and timestamp<= {} and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({}) )   GROUP BY region

# """

# )

# GET_ALL_INFO_MERCHANDISER_COMPLAIN_SPECIFIC_DATE = (

# """
# select * 
# from `biponon.merchandiser_complain` 
# where timestamp > {} and timestamp < {} and reporter in ( select whitelisting_number  from `biponon.merchandiser_basic_info` where  distributor_code IN ({}))  

# """

# )

# GET_COMPLAIN_OF_A_SPECIFIC_MERCHANDISER = (

# """
# select complain,timestamp
# from `biponon.merchandiser_complain`
# where reporter = "{}" and reporter in ( select whitelisting_number  from `biponon.merchandiser_basic_info` where  distributor_code IN ({}))   order by timestamp desc

# """
# )
###
GET_LAST_N_OUTLET_INFO_FILTER = (
    """

(select * except (row_numbers) from
(SELECT  region, area, territory, Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, Date,  Time, updated_address, updated_store_name,Upload_Date,  Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3 as file_path3, user_photo, merchantPlusLite, merchantPlusLiteNumber,name ,  user_type, row_number() over (partition by wallet_number ,Date  ) row_numbers
from
(SELECT  a.region, a.area, a.territory, b.Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time, updated_address, updated_store_name,FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00")) as Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3, user_photo, merchantPlusLite, merchantPlusLiteNumber,b.name , "Merchandiser" as user_type FROM `biponon.update_of_outlet` as a join `biponon.merchandiser_basic_info` as b on reporter= whitelisting_number WHERE a.region <> "TESTER"  {0}
order by Time desc)
)
where row_numbers = 1 
order by Time desc )
union all 
(
select * except (row_numbers) from
(SELECT  region, area, territory, Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, Date,  Time, updated_address, updated_store_name,Upload_Date,  Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3 as file_path3, user_photo, merchantPlusLite, merchantPlusLiteNumber,name ,  user_type, row_number() over (partition by wallet_number ,Date  ) row_numbers
from
(SELECT  a.region, a.area, a.territory, b.Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time, updated_address, updated_store_name,FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00")) as Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3, user_photo, merchantPlusLite, merchantPlusLiteNumber,b.name , "Supervisor" as user_type FROM `biponon.update_of_outlet` as a join `biponon.supervisor_basic_info` as b on reporter= whitelisting_number WHERE a.region <> "TESTER"  {0}  
order by Time desc)
)
where row_numbers = 1 
order by Time desc
)
ORDER BY Time DESC LIMIT {1} OFFSET {2}

    """
)


# GET_WHITELISTING_NUMBER_BY_CODE = (

# """
# select distinct(whitelisting_number  ) as number from `biponon.merchandiser` where distributor_code  ="{}"

# """
# )

# GET_WHITELISTING_NUMBER_BY_CODE = (

# """
# select distinct(whitelisting_number  ) as number from 
# (
# (select whitelisting_number,distributor_code  from 
# `biponon.merchandiser_basic_info` where distributor_code in ({1})) 
# union all 
# (
# select whitelisting_number,distributor_code  from 
# `biponon.supervisor_basic_info` where distributor_code in ({1})
# )
# )
# where distributor_code  ="{0}"

# """
# )
###
GET_DATE_WISE_MERCHANDISER_COMPLAIN_CSV = (

"""
SELECT wallet_number, reporter, complain,day,month,year
FROM `biponon.merchandiser_complain` 
WHERE timestamp > {} and timestamp < {} and  reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({}) )
"""

)
###
GET_MERCHANDISER_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE = (

"""
select whitelisting_number,	name,	region,	area,	territory,	Distributor,	type,	Actual_CallTarget,	Effective_CallTarget,	Achievement,
case
when  Effective_CallTarget=0
then 100
else ROUND((Achievement/Effective_CallTarget)*100,2)
end as Percentage,
outlet_not_found,	found_open,	found_closed,	qr_found,	qr_not_found,	qr_not_applicable,Date, "Merchandiser" as user_type from
(with a as
(
SELECT whitelisting_number  , 
sum( count_wallet_no )
as CallTarget, 
FORMAT_DATE("%d %b %Y",date) as date
FROM(
SELECT 
case
when wallet_no = "NO OUTLET"
then 0
else 1
end as count_wallet_no
, 
whitelisting_number ,row_number() over (partition by whitelisting_number,wallet_no,day,month,year   ) row_number ,DATE(year,month,day) as date
FROM `biponon.merchandiser`   
WHERE {0} and
 wallet_no != "NO OUTLET" and 
 region <>"TESTER" and 
 distributor_code <> "000" and 
 whitelisting_number  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 whitelisting_number  in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({3})  ) 
)
WHERE row_number = 1
group by whitelisting_number ,date
),
b as
(SELECT reporter ,   count(distinct wallet_number) as Achievement,sum(unfound) as outlet_not_found,sum(open_yes) as found_open, sum(closed_yes) as found_closed,sum(qr_yes) as qr_found,sum(qr_no) as qr_not_found,sum(unfound)+sum(closed_yes) as qr_not_applicable,date
FROM
( select wallet_number , reporter,closed, not_found,
case
when not_found = "1"
then 0
when closed = true
then 0
when qr= true
then 1
when qr=false
then 0
else 0
end as qr_yes,
case
when not_found = "1"
then 0
when closed = true
then 0
when qr= true
then 0
when qr=false
then 1
else 0
end as qr_no,
case
when not_found = "1"
then 0
when closed = true
then 1
when closed= false
then 0
end as closed_yes,
case
when not_found = "1"
then 0
when closed = true
then 0
when closed= false
then 1
end as open_yes,
case
when not_found = "1"
then 1
else 0
end
as unfound,
timestamp,date,Call_Time,row_number() over (partition by reporter,wallet_number ,date ) row_number 
from(
SELECT  wallet_number , reporter,closed, not_found,timestamp,FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Call_Time,qr
FROM `bkashbiponon.biponon.update_of_outlet` 
where 
timestamp >= {1} AND 
timestamp <= {2} and
is_inactive = false 
 and region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({3})  ) 
order by timestamp desc
)) where row_number=1

group by reporter ,date
order by reporter,date
)
,
c as
(
select whitelisting_number, name, region, area, territory, distributor_code, Distributor, type from `biponon.merchandiser_basic_info` where distributor_code in ({3})  
)
select ifnull(a.whitelisting_number,b.reporter) as whitelisting_number, name, region, area, territory, distributor_code, Distributor, type, 
ifnull(CallTarget,0) as Actual_CallTarget,
case
when c.type = "Metro" and CallTarget >45
then 45
when c.type = "Rural" and CallTarget >35
then 35
when c.type = "Urban" and CallTarget >40
then 40
else ifnull(CallTarget,0)
end
as Effective_CallTarget ,
ifnull(a.date,b.date) as Date,  ifnull(Achievement,0) as Achievement,ifnull(outlet_not_found,0) as outlet_not_found, ifnull(found_open,0) as found_open,  ifnull(found_closed,0) as found_closed, ifnull(qr_found,0) as qr_found, ifnull(qr_not_found,0) as qr_not_found, ifnull(qr_not_applicable,0) as qr_not_applicable
from (a full outer join b on a.whitelisting_number =b.reporter and a.date=b.date  ) left join c on ifnull(a.whitelisting_number,b.reporter)=c.whitelisting_number 

order by PARSE_DATE("%d %b %Y",Date)

)
"""
)
###
GET_SUPERVISOR_WISE_TARGET_VS_ACHIEVEMENT_SPECIFIC_DATE = (

"""
select whitelisting_number,	name,	region,	area,	territory,	Distributor,	type,	Actual_CallTarget,	Effective_CallTarget,	Achievement,
case
when  Effective_CallTarget=0
then 100
else ROUND((Achievement/Effective_CallTarget)*100,2)
end as Percentage,
outlet_not_found,	found_open,	found_closed,	qr_found,	qr_not_found,	qr_not_applicable,Date, "Supervisor" as user_type from
(with a as
(
SELECT whitelisting_number  , 
sum( count_wallet_no )
as CallTarget, 
FORMAT_DATE("%d %b %Y",date) as date
FROM(
SELECT 
case
when wallet_no = "NO OUTLET"
then 0
else 1
end as count_wallet_no
, 
whitelisting_number ,row_number() over (partition by whitelisting_number,wallet_no,day,month,year   ) row_number ,DATE(year,month,day) as date
FROM `biponon.supervisor`   
WHERE {0} and
 wallet_no != "NO OUTLET" and 
 region <>"TESTER" and 
 distributor_code <> "000" and 
 whitelisting_number  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 whitelisting_number  in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({3})  ) 
)
WHERE row_number = 1
group by whitelisting_number ,date
),
b as
(SELECT reporter ,   count(distinct wallet_number) as Achievement,sum(unfound) as outlet_not_found,sum(open_yes) as found_open, sum(closed_yes) as found_closed,sum(qr_yes) as qr_found,sum(qr_no) as qr_not_found,sum(unfound)+sum(closed_yes) as qr_not_applicable,date
FROM
( select wallet_number , reporter,closed, not_found,
case
when not_found = "1"
then 0
when closed = true
then 0
when qr= true
then 1
when qr=false
then 0
else 0
end as qr_yes,
case
when not_found = "1"
then 0
when closed = true
then 0
when qr= true
then 0
when qr=false
then 1
else 0
end as qr_no,
case
when not_found = "1"
then 0
when closed = true
then 1
when closed= false
then 0
end as closed_yes,
case
when not_found = "1"
then 0
when closed = true
then 0
when closed= false
then 1
end as open_yes,
case
when not_found = "1"
then 1
else 0
end
as unfound,
timestamp,date,Call_Time,row_number() over (partition by reporter,wallet_number ,date ) row_number 
from(
SELECT  wallet_number , reporter,closed, not_found,timestamp,FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Call_Time,qr
FROM `bkashbiponon.biponon.update_of_outlet` 
where 
timestamp >= {1} AND 
timestamp <= {2} and
is_inactive = false 
 and region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.supervisor_basic_info` where distributor_code in ({3})  ) 
order by timestamp desc
)) where row_number=1

group by reporter ,date
order by reporter,date
)
,
c as
(
select whitelisting_number, name, region, area, territory, distributor_code, Distributor, type from `biponon.supervisor_basic_info` where distributor_code in ({3})  
)
select ifnull(a.whitelisting_number,b.reporter) as whitelisting_number, name, region, area, territory, Distributor, type, 
ifnull(CallTarget,0) as Actual_CallTarget,
case
when c.type = "Metro" and CallTarget >45
then 45
when c.type = "Rural" and CallTarget >35
then 35
when c.type = "Urban" and CallTarget >40
then 40
else ifnull(CallTarget,0)
end
as Effective_CallTarget ,
ifnull(a.date,b.date) as Date,  ifnull(Achievement,0) as Achievement,ifnull(outlet_not_found,0) as outlet_not_found, ifnull(found_open,0) as found_open,  ifnull(found_closed,0) as found_closed, ifnull(qr_found,0) as qr_found, ifnull(qr_not_found,0) as qr_not_found, ifnull(qr_not_applicable,0) as qr_not_applicable
from (a full outer join b on a.whitelisting_number =b.reporter and a.date=b.date  ) left join c on ifnull(a.whitelisting_number,b.reporter)=c.whitelisting_number 

order by PARSE_DATE("%d %b %Y",Date)

)

"""
)

###
VISITED_OUTLETS_DATE_RANGE = (
"""
(select distinct a.reporter as Reporter,b.name ,"Merchandiser" as user_type,a.region,a.area,a.territory,b.Distributor,wallet_number,year,month,day,report_key,address,agent_or_merchant,answer1,answer2,bmcc,checkin_lat,checkin_lng,closed,file_path,file_path2,filepath3,is_inactive,not_found,lat,lng,revalidate_lat,revalidate_lng,store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time,updated_address,updated_store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00")) as Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Upload_Time,qr
from `biponon.update_of_outlet` as a join `biponon.merchandiser_basic_info` as b on reporter = whitelisting_number 
WHERE reporter ="{0}" and upload_time >= {1} and upload_time <= {2} and b.distributor_code in ({3}) ORDER BY upload_time DESC)
union all 
(select distinct a.reporter as Reporter,b.name ,"Supervisor" as user_type,a.region,a.area,a.territory,b.Distributor,wallet_number,year,month,day,report_key,address,agent_or_merchant,answer1,answer2,bmcc,checkin_lat,checkin_lng,closed,file_path,file_path2,filepath3,is_inactive,not_found,lat,lng,revalidate_lat,revalidate_lng,store_name,FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time,updated_address,updated_store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00")) as Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Upload_Time,qr
from `biponon.update_of_outlet` as a join `biponon.supervisor_basic_info` as b on reporter = whitelisting_number 
WHERE reporter ="{0}" and upload_time >= {1} and upload_time <= {2} and b.distributor_code in ({3}) ORDER BY upload_time DESC)
"""
)
###
GET_COUNT_OF_DATA_FROM_UPDATE_OF_OUTLETS_DATE = (

"""
 select count(*) as Count from
    ((select * except (row_numbers) from
(SELECT  region, area, territory, Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, Date,  Time, updated_address, updated_store_name,Upload_Date,  Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3, user_photo, merchantPlusLite, merchantPlusLiteNumber,name ,  user_type, row_number() over (partition by wallet_number ,Date  ) row_numbers
from
(SELECT  a.region, a.area, a.territory, b.Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time, updated_address, updated_store_name,FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00")) as Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3, user_photo, merchantPlusLite, merchantPlusLiteNumber,b.name , "Merchandiser" as user_type FROM `biponon.update_of_outlet` as a join `biponon.merchandiser_basic_info` as b on reporter= whitelisting_number WHERE a.region <> "TESTER"  {0}
order by Time desc)
)
where row_numbers = 1 
order by Time desc )
union all 
(
select * except (row_numbers) from
(SELECT  region, area, territory, Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, Date,  Time, updated_address, updated_store_name,Upload_Date,  Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3, user_photo, merchantPlusLite, merchantPlusLiteNumber,name ,  user_type, row_number() over (partition by wallet_number ,Date  ) row_numbers
from
(SELECT  a.region, a.area, a.territory, b.Distributor, wallet_number, year, month, day, report_key, address, agent_or_merchant, answer1, answer2, bmcc, checkin_lat, checkin_lng, closed, file_path, is_inactive, lat, lng, reporter, revalidate_lat, revalidate_lng, store_name, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time, updated_address, updated_store_name,FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00")) as Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Upload_Time , transaction_id, not_found, accuracy, sme, agent, merchant, merchantPlus, smeNumber, merchantNumber, merchantPlusNumber, agentNumber, qr, file_path2,filepath3, user_photo, merchantPlusLite, merchantPlusLiteNumber,b.name , "Supervisor" as user_type FROM `biponon.update_of_outlet` as a join `biponon.supervisor_basic_info` as b on reporter= whitelisting_number WHERE a.region <> "TESTER"  {0}  
order by Time desc)
)
where row_numbers = 1 
order by Time desc
)
)
     
"""

)
###
GET_WHITELISTING_NUMBER_ALL = (

"""
(select DISTINCT whitelisting_number as number  from `biponon.merchandiser_basic_info` where distributor_code in ({0}) )
union all
(select DISTINCT whitelisting_number as number  from `biponon.supervisor_basic_info` where distributor_code in ({0}) )

"""

)
###
GET_WHITELISTING_NUMBER_USER = (

"""
select DISTINCT whitelisting_number as number  from `{}` where distributor_code in ({})

"""

)
###
GET_DUMP_SPECIFIC_OUTLETS = (

"""
(select * except(row_number,time) from
(SELECT region as Region,area as Area, territory as Territory, distributor_code, 
substr(reporter ,  -10)
 as Merchandiser , 
 substr(wallet_number  ,  -10)
as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , 
IF((not_found = "1"), 'Not Found','Found') 
 as Is_the_Outlet_Reported_Unfound,
case
when not_found = "0" and closed = true 
then "Closed"
when not_found = "0" and closed = false and is_inactive= false 
then "Open"
when not_found = "0" and closed = false and is_inactive= true 
then "Inactive"
when not_found ="1"
then "N/A"
end as Is_Outlet_Status_Open_Closed_Inactive,
 transaction_id as Transaction_ID, date as Calls_Date,FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Calls_Time , FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST( upload_time as INT64)),"+06:00")) as Calls_Upload_Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( upload_time as INT64)), "+06:00") as Calls_Upload_Time,accuracy as Accuracy, 
IF((agent = 'true'), 'yes','no') as Is_there_Another_Account_Agent, 
IF((merchant = 'true'), 'yes','no') as Is_there_Another_Account_Merchant , 
IF((merchantPlus = 'true'), 'yes','no')  as Is_there_Another_Account_Merchant_Plus, 
IF((sme = 'true'), 'yes','no')  as Is_there_Another_Account_SME, 
substr(agentNumber, -10)
 as The_Other_Number_Agent,  
substr(merchantNumber,  -10)
 as The_Other_Number_Merchant,
 substr(merchantPlusNumber,  -10)
as The_Other_Number_Merchant_Plus,
substr(smeNumber,  -10)
as The_Other_Number_SME, 
case
when qr= true
then "yes"
when qr=false
then "no"
else "N/A"
end as QR,
row_number() over (partition by wallet_number ,date  ) row_number,time
from
(
SELECT  wallet_number ,region, area , territory , distributor_code , reporter ,  bmcc , store_name , address , updated_address , updated_store_name , agent_or_merchant , answer1 , answer2, lat, lng , closed , is_inactive , not_found , transaction_id , timestamp , upload_time , accuracy , agent, merchant  , merchantPlus , sme, agentNumber ,  merchantNumber ,merchantPlusNumber ,smeNumber, qr, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date ,timestamp as time
FROM `bkashbiponon.biponon.update_of_outlet` 
where 
timestamp >= {0} AND 
timestamp <= {1} and
is_inactive = false and
 region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter in (select whitelisting_number  from ((select whitelisting_number, distributor_code   from `biponon.merchandiser_basic_info`) union all (select whitelisting_number, distributor_code  from `biponon.supervisor_basic_info`)  ) where  distributor_code in ({3}) ) and wallet_number in {2}
order by timestamp desc)
)
where  row_number=1
order by time desc)

"""
)
###
GET_DATE_WISE_MERCHANDISER_MAINTENANCE_CSV = (

"""
SELECT wallet_number, reporter, task,day,month,year
FROM `biponon.merchandiser_maintenance_task` 
WHERE timestamp >{} and timestamp <{} and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({}) ) order by timestamp desc
"""

)
###
GET_DATE_WISE_MERCHANDISER_NEW_TASK_CSV = (

"""
SELECT wallet_number, reporter, task,day,month,year
FROM `biponon.merchandiser_new_task` 
WHERE timestamp >{} and timestamp <{} and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in ({}) ) order by timestamp desc
"""

)
###
GET_SELFIE_ATTENDANCE_MERCHANDISER_CSV = (

"""
SELECT name, 
  whitelisting_number ,
region, 
 area,
territory,
 Distributor ,
selfieAttendance ,
 timestamp from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.Distributor  as Distributor ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.merchandiser_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({2}))
where timestamp >= {0} and timestamp <= {1} 
order by timestamp desc
"""
)
###
GET_SELFIE_ATTENDANCE_SUPERVISOR_CSV = (

"""
SELECT name, 
  whitelisting_number ,
region, 
 area,
territory,
 Distributor ,
selfieAttendance ,
 timestamp from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.Distributor  as Distributor ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.supervisor_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({2}))
where timestamp >= {0} and timestamp <= {1} 
order by timestamp desc
"""
)
###
GET_SELFIE_ATTENDANCE_ON_SPECIFIC_DATE_MERCHANDISER = (

"""
SELECT name, 
  whitelisting_number ,
region, 
 area,
territory,
 distributor_code ,
selfieAttendance ,
 FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time,
 day,  month,  year from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.Distributor  as distributor_code ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.merchandiser_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({2}))
where timestamp >= {0} and timestamp <= {1} 
order by timestamp desc
"""

)
###
GET_SELFIE_ATTENDANCE_ON_SPECIFIC_DATE_SUPERVISOR = (

"""
SELECT name, 
  whitelisting_number ,
region, 
 area,
territory,
 distributor_code ,
selfieAttendance ,
 FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00") as Time,
 day,  month,  year from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.Distributor  as distributor_code ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.supervisor_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({2}))
where timestamp >= {0} and timestamp <= {1} 
order by timestamp desc
"""

)
###
GET_USER_WITH_NO_SELFIE_ATTENDANCE_MERCHANDISER = (

    """
select distinct a.whitelisting_number as whitelisting_number, b.name as name, b.region as region, b.area as area, b.territory as territory ,b.Distributor as distributor,day,month,year from `biponon.merchandiser` as a join `biponon.merchandiser_basic_info` as b on a.whitelisting_number=b.whitelisting_number  where  
{0}
 and a.whitelisting_number not in
(
SELECT phone from `biponon.selfieAttendanceData` where 
{0} 

) and   a.distributor_code in ({1})
    """
)
###
GET_USER_WITH_NO_SELFIE_ATTENDANCE_SUPERVISOR = (
"""
select distinct a.whitelisting_number as whitelisting_number, b.name as name, b.region as region, b.area as area, b.territory as territory, b.Distributor as distributor,day,month,year from `biponon.supervisor` as a join `biponon.supervisor_basic_info` as b on a.whitelisting_number=b.whitelisting_number  where  
{0}
 and a.whitelisting_number not in
(
SELECT phone from `biponon.selfieAttendanceData` where 
{0} 

) and   a.distributor_code in ({1})
    """
)
###
GET_USER_SHORT_INFO = (

"""
select phone , selfieAttendance , Date,Time,  Target,  Achievement , day ,month ,year   from
(select a.phone as phone , a.selfieAttendance as selfieAttendance, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( a.timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( a.timestamp as INT64)), "+06:00") as Time, count( distinct b.wallet_no ) as Target, count( distinct c.wallet_number  ) as Achievement , a.day as day,a.month as month,a.year as year   from (`biponon.selfieAttendanceData` as a left join `biponon.merchandiser` as b on a.phone = b.whitelisting_number  and a.day = b.day and a.month =b.month and a.year =b.year) left join `biponon.update_of_outlet` as c on a.phone = c.reporter  and a.day = c.day and a.month =c.month and a.year =c.year where a.phone  = "{0}" and a.day= {1} and a.month= {2} and a.year= {3} and a.phone in (select whitelisting_number  from `biponon.merchandiser_basic_info`  ) 
and b.distributor_code in ({4})
group by a.phone , a.selfieAttendance , a.timestamp,a.day ,a.month ,a.year)
union all 
(select a.phone as phone , a.selfieAttendance as selfieAttendance, FORMAT_DATE("%d/%m/%Y",DATE(TIMESTAMP_MILLIS(CAST( a.timestamp as INT64)), "+06:00")) as Date, FORMAT_TIMESTAMP("%I:%M %p",TIMESTAMP_MILLIS(CAST( a.timestamp as INT64)), "+06:00") as Time, count( distinct b.wallet_no ) as Target, count( distinct c.wallet_number  ) as Achievement , a.day as day,a.month as month,a.year as year  from (`biponon.selfieAttendanceData` as a left join `biponon.supervisor` as b on a.phone = b.whitelisting_number  and a.day = b.day and a.month =b.month and a.year =b.year) left join `biponon.update_of_outlet` as c on a.phone = c.reporter  and a.day = c.day and a.month =c.month and a.year =c.year  where a.phone  = "{0}" and a.day= {1} and a.month= {2} and a.year= {3} and a.phone in (select whitelisting_number  from `biponon.supervisor_basic_info`  )
and b.distributor_code in ({4})
group by a.phone , a.selfieAttendance , a.timestamp,a.day ,a.month ,a.year)
"""

)

# GET_FILTER_OUTLET = (

# """
# SELECT  *  FROM `bkashbiponon.biponon.update_of_outlet` where {} and distributor_code in ({}) 
# """
# )

# GET_LAT_LNG = (

# """
# SELECT reporter ,lat, lng, revalidate_lat, revalidate_lng,day ,month, year FROM `bkashbiponon.biponon.update_of_outlet`  where reporter = "{0}" and day={1} and month={2} and year={3} and distributor_code in ({4})
# """

# )
###
GET_OUTLET_LAT_LNG = (

"""
select reporter ,wallet_number ,lat, lng, revalidate_lat, revalidate_lng, day, month, year, timestamp from `biponon.update_of_outlet` where wallet_number in
(SELECT wallet_no  FROM `bkashbiponon.biponon.merchandiser` where whitelisting_number = "{0}" and day = {1} and month= {2} and year= {3} and distributor_code in ({4}) 
union all 
 SELECT wallet_no  FROM `bkashbiponon.biponon.supervisor` where whitelisting_number = "{0}" and day = {1} and month= {2} and year= {3} and distributor_code in ({4})
)

 group by reporter ,wallet_number ,lat, lng, revalidate_lat, revalidate_lng, day, month, year, timestamp order by wallet_number, timestamp desc
"""

)
###
GET_OUTLET_LAT_LNG_FUTURE = (

"""
select reporter ,wallet_number ,lat, lng, revalidate_lat, revalidate_lng, day, month, year, timestamp from `biponon.update_of_outlet` where wallet_number in
(SELECT wallet_no  FROM `bkashbiponon.biponon.merchandiser_future` where whitelisting_number = "{0}" and day = {1} and month= {2} and year= {3} and distributor_code in ({4}) 
union all 
 SELECT wallet_no  FROM `bkashbiponon.biponon.supervisor_future` where whitelisting_number = "{0}" and day = {1} and month= {2} and year= {3} and distributor_code in ({4})
)

 group by reporter ,wallet_number ,lat, lng, revalidate_lat, revalidate_lng, day, month, year, timestamp order by wallet_number, timestamp desc
"""

)
###
GET_COUNT_SELFIE_MERCHANDISER = (
    """
SELECT count(*) as count_merchandiser_selfie from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.distributor_code  as distributor_code ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.merchandiser_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({2}))
where timestamp >= {0} and timestamp <= {1} 

    """
)
###
GET_COUNT_SELFIE_SUPERVISOR = (
    """
SELECT count(*) as count_supervisor_selfie from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.distributor_code  as distributor_code ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.supervisor_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({2}))
where timestamp >= {0} and timestamp <= {1} 

    """
)
###
GET_TOTAL_MERCHANDISER = (
    """
select count(*) as total from `biponon.merchandiser_basic_info` where distributor_code in ({0})
    """
)
###
GET_TOTAL_SUPERVISOR = (
    """
select count(*) as total from `biponon.supervisor_basic_info` where distributor_code in ({0})
    """
)

###
GET_COUNT_REGION_WISE = (
    """
select count(distinct wallet_number) as call_count, LOWER(region) as region,FORMAT_DATE("%d-%m-%Y",DATE(TIMESTAMP_MILLIS(CAST( timestamp as INT64)), "+06:00")) as date  from `biponon.update_of_outlet` where region <> "TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in ( select whitelisting_number  from `biponon.{0}_basic_info` ) group by region,Date order by region,Date desc
    """
)


GET_COUNT_ALL_DASHBOARD = (

"""
with a as
(select count(*) as total, "{0}" as type from `biponon.{0}_basic_info`
where distributor_code in ({5})
),
b as
(SELECT count(*) as count_selfie, "{0}" as type  from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.distributor_code  as distributor_code ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.{0}_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({5})
)
where timestamp >= {1} and timestamp <= {2}  ),
c as
(
    SELECT COUNT(DISTINCT(reporter)) as active_today, "{0}" as type FROM `bkashbiponon.biponon.update_of_outlet` where timestamp >= {1} and timestamp <= {2}  AND region <> "TESTER" and reporter in (select whitelisting_number  from `biponon.{0}_basic_info`
    WHERE  distributor_code IN ({5})
    )
    
),
d as
(
    SELECT COUNT(DISTINCT(reporter)) as active_yesterday, "{0}" as type FROM `bkashbiponon.biponon.update_of_outlet` where timestamp >= {3} and timestamp <= {4}  AND region <> "TESTER" and reporter in (select whitelisting_number  from `biponon.{0}_basic_info`
    WHERE  distributor_code IN ({5})
    )
    
)
select ifnull(total,0) as total ,  ifnull(active_today,0) as active_today ,   ifnull(active_yesterday,0) as active_yesterday  , ifnull(count_selfie,0) as count_selfie , ifnull(total,0) - ifnull(count_selfie,0) as count_leave from ((a full outer join b on a.type = b.type ) full outer join c on b.type = c.type ) full outer join d on c.type = d.type 
"""

)
###
OUTLETS_NOT_VISITED_CSV = (

"""
(select * except (row_number) from
(select wallet_no ,"{4}" as user_type, whitelisting_number, name , region, area, territory, Distributor,date, row_number() over (partition by wallet_no ,date ) row_number from
(select   wallet_no,a.whitelisting_number, a.name , a.region, a.area, a.territory, a.distributor_code, b.Distributor  ,FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date  from `biponon.{4}` as a join `biponon.{4}_basic_info` as b on a.distributor_code = b.distributor_code where {0}
and  a.whitelisting_number  in (select whitelisting_number  from `biponon.{4}_basic_info` where distributor_code in ({3})  ) 
order by timestamp desc ))
where row_number = 1 and wallet_no  not in
(with a as
(select wallet_no ,date ,row_number from
(select wallet_no , date, row_number() over (partition by wallet_no ,date ) row_number from
(select   wallet_no, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date  from `biponon.{4}` where {0} 
and  whitelisting_number  in (select whitelisting_number  from `biponon.{4}_basic_info` where distributor_code in ({3})  ) 

order by timestamp desc ))
where row_number = 1),
b as
(
select wallet_number ,date , row_number from
(select wallet_number , date, row_number() over (partition by wallet_number ,date ) row_number from
(select   wallet_number, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date  from `bkashbiponon.biponon.update_of_outlet` where timestamp >= {1} and timestamp <= {2}  and
region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.{4}_basic_info` where distributor_code in ({3})  ) 

order by timestamp desc  ))
where row_number = 1
)
SELECT   b.wallet_number FROM  a join  b on b.wallet_number = a.wallet_no and a.date = b.date ))

"""

)

# GET_OUTLET_OUTSIDE = (

# """
# (select distinct Agent_Merchants_Wallet_Number,Outlet_Name,Address, "{0}" as Merchandiser_Region,Region,	Area	,Territory	,Distribution_House,	Merchandiser,	Edited_Outlet_Name  from
# (SELECT region as Region,area as Area, territory as Territory, distributor_code as Distribution_House , 
# case 
# when length(reporter) > 11
# then substr(reporter, 5, 14)
# else substr(reporter, 2, 11)
# end as Merchandiser , 
# case 
# when length(wallet_number) > 11
# then substr(wallet_number,  5, 14)
# else substr(wallet_number, 2, 11)
# end as Agent_Merchants_Wallet_Number, bmcc as BMCC , store_name as Outlet_Name, address as Address  ,  updated_store_name as Edited_Outlet_Name ,updated_address as Edited_Outlet_Address , agent_or_merchant as Category , answer1 as Is_Merchant_Aware_They_Can_Accept_bKash , answer2 as Is_the_Phone_in_the_Outlet, lat as Latitude, lng as Longitude , closed as Is_the_Outlet_Closed, is_inactive as Is_the_Outlet_Inactive , 
# case 
# when not_found = "0" 
# then false 
# else true 
# end as Is_the_Outlet_Reported_Unfound , transaction_id as Transaction_ID, timestamp as Call_Timestamp, upload_time as Call_Upload_Time, accuracy as Accuracy, agent as Is_there_Another_Account_Agent, merchant as Is_there_Another_Account_Merchant , merchantPlus as Is_there_Another_Account_Merchant_Plus, sme as Is_there_Another_Account_SME, 
# case 
# when length(agentNumber) > 11
# then substr(agentNumber, 5, 14)
# else substr(agentNumber, 2, 11)
# end as The_Other_Number_Agent,  
# case 
# when length(merchantNumber) > 11
# then substr(merchantNumber,  5, 14)
# else substr(merchantNumber, 2, 11)
# end  as The_Other_Number_Merchant,
# case 
# when length(merchantPlusNumber) > 11
# then substr(merchantPlusNumber,  5, 14)
# else substr(merchantPlusNumber, 2, 11)
# end as The_Other_Number_Merchant_Plus,
# case 
# when length(smeNumber) > 11
# then substr(smeNumber,  5, 14)
# else substr(smeNumber, 2, 11)
# end as The_Other_Number_SME, qr as QR, day,month,year
# FROM (
#   SELECT
#       wallet_number ,region, area , territory , distributor_code , reporter ,  bmcc , store_name , address , updated_address , updated_store_name , agent_or_merchant , answer1 , answer2, lat, lng , closed , is_inactive , not_found , transaction_id , timestamp , upload_time , accuracy , agent, merchant  , merchantPlus , sme, agentNumber ,  merchantNumber ,merchantPlusNumber ,smeNumber,day, month, year, qr,
#       ROW_NUMBER()
#           OVER (PARTITION BY wallet_number)
#           row_number          
#   FROM `biponon.update_of_outlet`
#   WHERE
#   --upload_time >= 1551376800000.0 AND upload_time <= 1552154399000.0 and 
#   region <>"TESTER" and distributor_code <> "000" and reporter Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and reporter in (select whitelisting_number  from `biponon.merchandiser_basic_info` where distributor_code in {1}  ) order by upload_time  desc
# )
# WHERE row_number = 1 order by upload_time desc) where Region <> "{0}" )

# """

# )



GET_MERCHANDISERS_BASIC_INFO = (
    """
select * from `test.merchandiser_basic_info_test` 
    """
)

GET_MERCHANDISERS_ROUTE_PLAN = (
    """
    SELECT
    imei , name	, year , month , day , territory , area , distributor_code , whitelisting_number , wallet_no , region , active_status
    FROM (
        SELECT 
        imei , name	, year , month , day , territory , area , distributor_code , whitelisting_number , wallet_no , region , active_status , 
        ROW_NUMBER() OVER (PARTITION BY whitelisting_number) row_number 
        FROM (
            SELECT 
            *
            FROM `test.merchandiser_test` 
            WHERE day={} and month={} and year={} 
            ORDER BY active_status desc
        ) 
    ) WHERE row_number = 1
    """
)

MERCHANDISER_SCHEMA = [
    bigquery.SchemaField('imei' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('name' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('year' , "INTEGER", mode="NULLABLE" ),
    bigquery.SchemaField('month' , "INTEGER", mode="NULLABLE" ),
    bigquery.SchemaField('day' , "INTEGER", mode="NULLABLE" ),
    bigquery.SchemaField('territory' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('area' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('distributor_code' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('whitelisting_number' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('wallet_no' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('region' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('active_status' , "STRING", mode="NULLABLE" ),
    bigquery.SchemaField('timestamp', 'NUMERIC', mode='NULLABLE')
]



TEST_DATASET_ID = "test"
TEST_MERCHANDISER_TABLE_ID = "merchandiser_test"
test_dataset_ref = bigquery_client.dataset(TEST_DATASET_ID)
TEST_MERCHANDISER_TABLE_REF = test_dataset_ref.table(TEST_MERCHANDISER_TABLE_ID)
TEST_MERCHANDISER_TABLE = bigquery.Table(TEST_MERCHANDISER_TABLE_REF, schema=MERCHANDISER_SCHEMA)

###
GET_COUNT_ALL_DASHBOARD = (

"""
with a as
(select count(*) as total, "{0}" as type from `biponon.{0}_basic_info`
where distributor_code in ({5})
),
b as
(SELECT count(*) as count_selfie, "{0}" as type  from
(SELECT 
msb.name as name, 
 msb.whitelisting_number as whitelisting_number ,
msb.region as region, 
 msb.area as area,
msb.territory as territory,
 msb.distributor_code  as distributor_code ,
slfd.selfieAttendance as selfieAttendance ,
case
when CHAR_LENGTH(CAST(slfd.timestamp as STRING ) ) = 10
then slfd.timestamp*1000
else slfd.timestamp
end as timestamp,
slfd.day as day, slfd.month as month, slfd.year as year
FROM biponon.{0}_basic_info as msb INNER JOIN biponon.selfieAttendanceData as slfd ON slfd.phone= msb.whitelisting_number

where  msb.distributor_code in ({5})
)
where timestamp >= {1} and timestamp <= {2}  ),
c as
(
    SELECT COUNT(DISTINCT(reporter)) as active_today, "{0}" as type FROM `bkashbiponon.biponon.update_of_outlet` where timestamp >= {1} and timestamp <= {2}  AND region <> "TESTER" and reporter in (select whitelisting_number  from `biponon.{0}_basic_info`
    WHERE  distributor_code IN ({5})
    )
    
),
d as
(
    SELECT COUNT(DISTINCT(reporter)) as active_yesterday, "{0}" as type FROM `bkashbiponon.biponon.update_of_outlet` where timestamp >= {3} and timestamp <= {4}  AND region <> "TESTER" and reporter in (select whitelisting_number  from `biponon.{0}_basic_info`
    WHERE  distributor_code IN ({5})
    )
    
)
select ifnull(total,0) as total ,  ifnull(active_today,0) as active_today ,   ifnull(active_yesterday,0) as active_yesterday  , ifnull(count_selfie,0) as count_selfie , ifnull(total,0) - ifnull(count_selfie,0) as count_leave from ((a full outer join b on a.type = b.type ) full outer join c on b.type = c.type ) full outer join d on c.type = d.type 
"""

)
###
OUTLETS_NOT_VISITED_CSV = (

"""
(select * except (row_number) from
(select wallet_no ,"{4}" as user_type, whitelisting_number, name , region, area, territory, Distributor,date, row_number() over (partition by wallet_no ,date ) row_number from
(select   wallet_no,a.whitelisting_number, a.name , a.region, a.area, a.territory, a.distributor_code, b.Distributor  ,FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date  from `biponon.{4}` as a join `biponon.{4}_basic_info` as b on a.distributor_code = b.distributor_code where {0}
and  a.whitelisting_number  in (select whitelisting_number  from `biponon.{4}_basic_info` where distributor_code in ({3})  ) 
order by timestamp desc ))
where row_number = 1 and wallet_no  not in
(with a as
(select wallet_no ,date ,row_number from
(select wallet_no , date, row_number() over (partition by wallet_no ,date ) row_number from
(select   wallet_no, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date  from `biponon.{4}` where {0} 
and  whitelisting_number  in (select whitelisting_number  from `biponon.{4}_basic_info` where distributor_code in ({3})  ) 

order by timestamp desc ))
where row_number = 1),
b as
(
select wallet_number ,date , row_number from
(select wallet_number , date, row_number() over (partition by wallet_number ,date ) row_number from
(select   wallet_number, FORMAT_DATE("%d %b %Y",DATE(TIMESTAMP_MILLIS(CAST(timestamp as INT64)),"+06:00")) AS date  from `bkashbiponon.biponon.update_of_outlet` where timestamp >= {1} and timestamp <= {2}  and
region <>"TESTER" and distributor_code <> "000" and reporter  Not in ("+8801675345810","+8801682425474","+8801535704918","+8801981307544","+8801534575354","+8801534609614", "+8801534283301") and
 reporter  in (select whitelisting_number  from `biponon.{4}_basic_info` where distributor_code in ({3})  ) 

order by timestamp desc  ))
where row_number = 1
)
SELECT   b.wallet_number FROM  a join  b on b.wallet_number = a.wallet_no and a.date = b.date ))

"""

)
