FROM python:3.6
WORKDIR /app
RUN mkdir src
ADD requirements.txt .
RUN pip install -r requirements.txt
WORKDIR /app/src
COPY ./src .
EXPOSE 5000
ENTRYPOINT ["python", "manage.py", "runserver", "0.0.0.0:5000"]